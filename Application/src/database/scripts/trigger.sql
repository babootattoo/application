CREATE EXTENSION pgcrypto;

CREATE OR REPLACE FUNCTION crypt_password_employee() RETURNS TRIGGER AS $$
	BEGIN
		SELECT encode(digest(NEW.password, 'sha256'), 'hex') INTO NEW.password;
		RETURN NEW;
	END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER trig_cryptpwd_employee BEFORE INSERT ON employee
	FOR EACH ROW
	EXECUTE PROCEDURE crypt_password_employee();
	
CREATE OR REPLACE FUNCTION update_cardstate_workon() RETURNS TRIGGER AS $$
        DECLARE
        BEGIN
                UPDATE reparationform SET idcardstate = (SELECT id FROM cardstate WHERE description = 'In reparation') WHERE id = NEW.idreparationform;
                INSERT INTO commentaryreparationform VALUES (NEW.idreparationform, NEW.idemployee, now(), 'Working on this.');
                RETURN NEW;
        END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER trig_update_cardstate_workon BEFORE INSERT ON workon
        FOR EACH ROW
        EXECUTE PROCEDURE update_cardstate_workon();