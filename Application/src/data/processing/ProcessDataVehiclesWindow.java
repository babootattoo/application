package data.processing;

import core.server.model.BreakdownDAO;
import core.server.model.CardStateDAO;
import core.server.model.ViewModel;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

/**
 * All database methods related to the Car class
 * @author Pierre.TokerKovacic
 */
public class ProcessDataVehiclesWindow {
    JsonObject input;
    JsonObject output;

    JsonObjectBuilder builder;

    String ipClient;
    
    /**
     * Set all the attributes and initialize the JsonObjectBuilder, this builder
     * will build the outputJson
     *
     * @param jsonString input string of the client, should be a JsonObject
     * @param ip ip of the client
     */
    public ProcessDataVehiclesWindow(String jsonString, String ip) {
        JsonReader reader = Json.createReader(new StringReader(jsonString));
        input = reader.readObject();
        builder = Json.createObjectBuilder();
        ipClient = ip;
        ViewModel.println("(" + ipClient + ") Processing request (Action : " + input.getString("Action") + ",View : "+ input.getString("View") +")");
    }
    
    /**
     * Do a certain process depending on the Action JsonValue of the input
     * JsonObject
     *
     * @return output jsonObject which will be sent to the client
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     * @throws ParseException
     */
    public JsonObject getResult() throws SQLException, IOException, ClassNotFoundException, InterruptedException, ParseException, IllegalAccessException, InvocationTargetException,NoSuchMethodException {
        switch (input.getString("Action")) {
            case "getCountCardState":
                builder.add("Action", input.getString("Action"));
                handlingCountCardState();
                break;
            case "getPriorityPerCar":
                builder.add("Action", input.getString("Action"));
                handlingPriorityPerCar();
                break;
            case "getPriorityPerBike":
                builder.add("Action", input.getString("Action"));
                handlingPriorityPerBike();
                break;
            default:
                builder.add("Action", "None");
                builder.add("State", "Action not found");
                break;
        }
        output = builder.build();
        ViewModel.println("(" + ipClient + ") Getting response (Action : " + output.getString("Action") + " AND State : " + output.getString("State") + ")");
        return output;
    }
    
    /**
     * Method which handle the action "CountCardState" 
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    private void handlingCountCardState() throws SQLException, IOException, ClassNotFoundException, InterruptedException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        List<String> ListCountCardState = CardStateDAO.getCountCardState();
        if (ListCountCardState != null) {
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            builder.add("State", "Success");
            for(String line : ListCountCardState){
                arrayBuilder.add(line);
            }
            builder.add("List", arrayBuilder.build());
        } else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "No user found with this login and password");
        }
    }
    
    /**
     * Method which handle the action "PriorityPerCar" 
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    private void handlingPriorityPerCar() throws SQLException, IOException, ClassNotFoundException, InterruptedException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        List<String> ListPriorityPerCar = BreakdownDAO.getMaxPriorityLevelPerCar();
        if (ListPriorityPerCar != null) {
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            builder.add("State", "Success");
            for(String line : ListPriorityPerCar){
                arrayBuilder.add(line);
            }
            builder.add("List", arrayBuilder.build());
        }
    }
    
    /**
     * Method which handle the action "PriorityPerBike" 
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    private void handlingPriorityPerBike() throws SQLException, IOException, ClassNotFoundException, InterruptedException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        List<String> ListPriorityPerBike = BreakdownDAO.getMaxPriorityLevelPerBike();
        if (ListPriorityPerBike != null) {
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            builder.add("State", "Success");
            for(String line : ListPriorityPerBike){
                arrayBuilder.add(line);
            }
            builder.add("List", arrayBuilder.build());
        }
    }
    
}
