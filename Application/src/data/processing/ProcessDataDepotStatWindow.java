/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.processing;

import core.server.model.BreakdownDAO;
import core.server.model.ParkingDAO;
import core.server.model.PartDAO;
import core.server.model.ReparationFormDAO;
import core.server.model.SerializableJson;
import core.server.model.UserDAO;
import core.server.model.VehicleDAO;
import core.server.model.ViewModel;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

/**
 *
 * @author Quentin Goutte
 */
public class ProcessDataDepotStatWindow {
    JsonObject input;
    JsonObject output;

    JsonObjectBuilder builder;

    String ipClient;
    
    public ProcessDataDepotStatWindow (String jsonString, String ip){
        JsonReader reader = Json.createReader(new StringReader (jsonString));
        input = reader.readObject();
        builder = Json.createObjectBuilder();
        ipClient = ip;
        ViewModel.println("(" + ipClient + ") Processing request (Action : " + input.getString("Action") + ")");
    }
    
    public JsonObject getResult() throws SQLException, IOException, ClassNotFoundException, InterruptedException, ParseException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        switch (input.getString("Action")){
            case "getNumberEmployee":
                builder.add("Action", input.getString("Action"));
                handlingGetNumberEmployee();
                break;
            case "getNumberVehicle":
                builder.add("Action", input.getString("Action"));
                handlingGetNumberVehicle();
                break;
            case "getNumberVehicleInDepot":
                builder.add("Action", input.getString("Action"));
                handlingGetNumberVehicleInDepot();
                break;
            case "getNumberBreakdownThisYear":
                builder.add("Action", input.getString("Action"));
                handlingGetNumberBreakdownThisYear();
                break;
            case "getNumberBreakdownThisMonth":
                builder.add("Action", input.getString("Action"));
                handlingGetNumberBreakdownThisMonth();
                break;
            case "getAvgBreakdownByYear":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgBreakdownByYear();
                break;
            case "getAvgBreakdownByMonth":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgBreakdownByMonth();
                break;
            case "getNumberBuyingPiecesThisYear":
                builder.add("Action", input.getString("Action"));
                handlingNumberBuyingPiecesThisYear();
                break;
            case "getNumberBuyingPiecesThisMonth":
                builder.add("Action", input.getString("Action"));
                handlingNumberBuyingPiecesThisMonth();
                break;
            case "getAvgPiecesByYear":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgPiecesByYear();
                break;
            case "getAvgPiecesByMonth":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgPiecesByMonth();
                break;
            case "getAvgDuringBreakdown":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgDuringBreakdown();
                break;
            case "getPriceThisYear":
                builder.add("Action", input.getString("Action"));
                handlingPriceThisYear();
                break;
            case "getPriceThisMonth":
                builder.add("Action", input.getString("Action"));
                handlingPriceThisMonth();
                break;
            case "getAvgPriceByYear":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgPriceByYear();
                break;
            case "getAvgPriceByMonth":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgPriceByMonth();
                break;
            default:
                builder.add("Action", "None");
                builder.add("State", "Action not found");
                break;
        }
        output=builder.build();
        ViewModel.println("("+ipClient + ") Getting response (Action : " + output.getString("Action") + " AND State : " + output.getString("State") + ")");
        return output;
    }
    
    public void handlingGetNumberEmployee() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int number = UserDAO.getNumberUser();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("NbEmployee", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any users in depot");
        }
    }
    
    public void handlingGetNumberVehicle() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int number = VehicleDAO.getNumberVehicle();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("NbVehicle", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any vehicle");
        }
    }
    public void handlingGetNumberVehicleInDepot() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int number = ParkingDAO.getNumberPlacesOccupied();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("NbVehicleInDepot", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any vehicle");
        }
    }

    private void handlingGetNumberBreakdownThisYear() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int number = BreakdownDAO.getNumberBreakdownThisYear();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("NbBreakdownThisYear", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetNumberBreakdownThisMonth() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int number = BreakdownDAO.getNumberBreakdownThisMonth();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("NbBreakdownThisMonth", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetAvgBreakdownByMonth() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = BreakdownDAO.getAvgBreakdownByMonth();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetAvgBreakdownByMonth", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetAvgBreakdownByYear() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = BreakdownDAO.getAvgBreakdownByYear();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetAvgBreakdownByYear", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }

    private void handlingNumberBuyingPiecesThisYear()  throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int number = PartDAO.getNumberPiecesBuyingThisYear();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetNumberBuyingPiecesThisYear", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any pieces");
        }
    }

    private void handlingNumberBuyingPiecesThisMonth() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int number = PartDAO.getNumberPiecesBuyingThisMonth();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetNumberBuyingPiecesThisMonth", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any pieces");
        }
    }
    
    private void handlingGetAvgPiecesByMonth() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getAvgPiecesByMonth();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetAvgPiecesByMonth", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetAvgPiecesByYear() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getAvgPiecesByYear();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetAvgPiecesByYear", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetAvgDuringBreakdown() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = ReparationFormDAO.getAvgDuringBreakdown();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetAvgDuringBreakdown", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any During");
        }
    }
    
    private void handlingPriceThisYear()  throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getPriceThisYear();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetPriceThisYear", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any pieces");
        }
    }

    private void handlingPriceThisMonth() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getNumberPiecesBuyingThisMonth();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetPriceThisMonth", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any pieces");
        }
    }
    
    private void handlingGetAvgPriceByMonth() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getAvgPriceByMonth();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetAvgPriceByMonth", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any price");
        }
    }
    
    private void handlingGetAvgPriceByYear() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getAvgPriceByYear();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetAvgPriceByYear", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any price");
        }
    }
    
}
