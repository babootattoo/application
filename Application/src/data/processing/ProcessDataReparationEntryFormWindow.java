package data.processing;

import core.server.model.Breakdown;
import core.server.model.BreakdownDAO;
import core.server.model.CardStateDAO;
import core.server.model.Comment;
import core.server.model.CommentDAO;
import core.server.model.Part;
import core.server.model.PartDAO;
import core.server.model.ReparationForm;
import core.server.model.ReparationFormDAO;
import core.server.model.SerializableJson;
import core.server.model.VehicleDAO;
import core.server.model.ViewModel;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

/**
 * ProcessData for ReparationFormDetailWindow
 * @author Stephane.Schenkel
 */
public class ProcessDataReparationEntryFormWindow {
    
    JsonObject input;
    JsonObject output;

    JsonObjectBuilder builder;

    String ipClient;

    /**
     * Set all the attributes and initialize the JsonObjectBuilder, this builder
     * will build the outputJson
     *
     * @param jsonString input string of the client, should be a JsonObject
     * @param ip of the client
     */
    public ProcessDataReparationEntryFormWindow(String jsonString, String ip) {
        JsonReader reader = Json.createReader(new StringReader(jsonString));
        input = reader.readObject();
        builder = Json.createObjectBuilder();
        ipClient = ip;
        ViewModel.println("(" + ipClient + ") Processing request (Action : " + input.getString("Action") + ")");
    }

    /**
     * Do a certain process depending on the Action JsonValue of the input
     * JsonObject
     *
     * @return output jsonObject which will be sent to the client
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     * @throws ParseException
     */
    public JsonObject getResult() throws SQLException, IOException, ClassNotFoundException, InterruptedException, ParseException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        switch (input.getString("Action")) {
            case "getAllCardState":
                builder.add("Action", input.getString("Action"));
                handlingGetAllCardState();
                break;
            case "getCommentByFormId":
                builder.add("Action", input.getString("Action"));
                handlingGetCommentByFormId(input.getInt("FormId"));
                break;
            case "addComment":
                builder.add("Action", input.getString("Action"));
                handlingAddComment(input.getInt("UserId"), input.getInt("FormId"), input.getString("Message"));
                break;
            case "getListBreakdownCarModel":
                builder.add("Action", input.getString("Action"));
                handlingGetListBreakdownCarModel(input.getInt("IdCarModel"), input.getInt("IdCar"));
                break;
            case "getListBreakdownBike":
                builder.add("Action", input.getString("Action"));
                handlingGetListBreakdownBike(input.getInt("IdBike"));
                break;
            case "addBreakdown":
                builder.add("Action", input.getString("Action"));
                handlingAddBreakdown(input.getInt("IdVehicle"), input.getInt("IdBreakdown"), input.getString("Table"));
                break;
            case "deleteBreakdown":
                builder.add("Action", input.getString("Action"));
                handlingDeleteBreakdown(input.getInt("IdVehicle"), input.getInt("IdBreakdown"), input.getString("Table"));
                break;
            case "updateReparatorDetails":
                builder.add("Action", input.getString("Action"));
                handlingUpdateReparationForm(ReparationForm.deserialize(input.getJsonObject("ReparationForm")));
                break;
            default:
                builder.add("Action", "None");
                builder.add("State", "Action not found");
                break;
        }
        output = builder.build();
        ViewModel.println("(" + ipClient + ") Getting response (Action : " + output.getString("Action") + " AND State : " + output.getString("State") + ")");
        return output;
    }
    
    /**
     * Method to handle the getAllCardState action, build the JsonObject with the list of CardState
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    private void handlingGetAllCardState() throws ClassNotFoundException, SQLException, InterruptedException{
        List<String> list = CardStateDAO.getAllString();
        if(!list.isEmpty()){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            for(String line : list){
                arrayBuilder.add(line);
            }
            builder.add("State", "Success");
            builder.add("List", arrayBuilder.build());
        }else{
            builder.add("State", "Error");
            builder.add("ErrorMessage", "No card state found in the database");
        }
    }
    
    /**
     * Method to handle the getCommentByFormId action, build the JsonObject with the list of comment found
     * in the database for the specified reparationform id
     * @param id
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    private void handlingGetCommentByFormId(int id) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        List<Comment> list = CommentDAO.getCommentsByFormId(id);
        if(!list.isEmpty()){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            for(Comment comment : list){
                arrayBuilder.add(SerializableJson.serialize(comment));
            }
            builder.add("State", "Success");
            builder.add("CommentList", arrayBuilder.build());
        }else{
            builder.add("State", "Error");
            builder.add("ErrorMessage", "No comments found");
        }
    }
    
    /**
     * Method to handle the AddComment action. Add the specified message with the ifForm and idUser
     * to create a comment in the database
     * @param idUser
     * @param idForm
     * @param message
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    private void handlingAddComment(int idUser, int idForm, String message) throws SQLException, ClassNotFoundException, InterruptedException{
        int result = CommentDAO.addComment(idUser, idForm, message);
        if(result > 0){
            builder.add("State", "Success");
        }else{
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't add the message");
        }
    }
    
    
    /**
     * Method to handle the getListBreakdownCarModel action, build the JsonObject with the list of breakdown
     * that the specified carmodel can have.
     * @param idCarModel
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    private void handlingGetListBreakdownCarModel(int idCarModel, int idCar) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, SQLException, ClassNotFoundException, InterruptedException{
        List<Breakdown> list = BreakdownDAO.getAvailableBreakdownModel(idCarModel, idCar);
        if(!list.isEmpty()){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            for(Breakdown breakdown : list){
                arrayBuilder.add(SerializableJson.serialize(breakdown));
            }
            builder.add("State", "Success");
            builder.add("BreakdownList", arrayBuilder.build());
        }else{
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown for this car model.");
        }
    }
    
    /**
     * Method to handle the getListBreakdownBike action, build the JsonObject with the list of breakdown
     * that a bike can have.
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    private void handlingGetListBreakdownBike(int idBike) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, SQLException, ClassNotFoundException, InterruptedException{
        List<Breakdown> list = BreakdownDAO.getAvailableBreakdownBike(idBike);
        if(!list.isEmpty()){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            for(Breakdown breakdown : list){
                arrayBuilder.add(SerializableJson.serialize(breakdown));
            }
            builder.add("State", "Success");
            builder.add("BreakdownList", arrayBuilder.build());
        }else{
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown for bikes.");
        }
    }
    
    /**
     * Method to handle the addBreakdown action, add the breakdown for the specified vehicle.
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException 
     */
    private void handlingAddBreakdown(int idVehicle, int idBreakdown, String table) throws ClassNotFoundException, SQLException, InterruptedException, IllegalAccessException, NoSuchMethodException, InvocationTargetException{
        int result = VehicleDAO.addBreakdown(idVehicle, idBreakdown, table);
        if(result > 0){
            List<Breakdown> list = BreakdownDAO.getHaveListByVehicleId(idVehicle, table);
            if(!list.isEmpty()){
                JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
                for(Breakdown breakdown : list){
                    arrayBuilder.add(SerializableJson.serialize(breakdown));
                }
                builder.add("State", "Success");
                builder.add("BreakdownList", arrayBuilder.build());
            }else{
                builder.add("State", "Error");
                builder.add("ErrorMessage", "Couldn't retrieve the list of breakdown after adding it");
            }
        }else{
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't add the breakdown");
        }
    }
    
    /**
     * Method to handle the deleteBreakdown action, delete the breakdown for the specified vehicle.
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    private void handlingDeleteBreakdown(int idVehicle, int idBreakdown, String table) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException{
        int result = VehicleDAO.deleteBreakdown(idVehicle, idBreakdown, table);
        if(result > 0){
            List<Breakdown> list = BreakdownDAO.getHaveListByVehicleId(idVehicle, table);
            if(!list.isEmpty()){
                JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
                for(Breakdown breakdown : list){
                    arrayBuilder.add(SerializableJson.serialize(breakdown));
                }
                builder.add("State", "Success");
                builder.add("BreakdownList", arrayBuilder.build());
            }else{
                builder.add("State", "Error");
                builder.add("ErrorMessage", "Couldn't retrieve the list of breakdown after deleting breakdown");
            }
        }else{
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't delete the breakdown");
        }
    }
        
    /**
     * Method to handle the updateReparationForm action, update the specified ReparationForm
     * @param repForm
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    private void handlingUpdateReparationForm(ReparationForm repForm) throws ClassNotFoundException, SQLException, InterruptedException{
        int result = ReparationFormDAO.updateReparationForm(repForm);
        if(result > 0){
            builder.add("State", "Success");
        }else{
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't update the reparationform");
        }
    }
    
}
