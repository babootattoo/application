package data.processing;

import core.server.model.ReparationForm;
import core.server.model.ReparationFormDAO;
import core.server.model.SerializableJson;
import core.server.model.ViewModel;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

/**
 *
 * @author Brice.Boutamdja
 */
public class ProcessDataReparationFormMenuWindow {
    
    JsonObject input;
    JsonObject output;

    JsonObjectBuilder builder;

    String ipClient;

    /**
     * Set all the attributes and initialize the JsonObjectBuilder, this builder
     * will build the outputJson
     *
     * @param jsonString input string of the client, should be a JsonObject
     * @param ip ip of the client
     */
    public ProcessDataReparationFormMenuWindow(String jsonString, String ip) {
        JsonReader reader = Json.createReader(new StringReader(jsonString));
        input = reader.readObject();
        builder = Json.createObjectBuilder();
        ipClient = ip;
        ViewModel.println("(" + ipClient + ") Processing request (Action : " + input.getString("Action") + ")");
    }

    /**
     * Do a certain process depending on the Action JsonValue of the input
     * JsonObject
     *
     * @return output jsonObject which will be sent to the client
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     * @throws ParseException
     */
    public JsonObject getResult() throws SQLException, IOException, ClassNotFoundException, InterruptedException, ParseException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        switch (input.getString("Action")) {
            case "giveAllReparationForm":
                builder.add("Action", input.getString("Action"));
                handlingGiveAllReparationForm();
                break;
            case "giveAllReparationFormWorkOn":
                builder.add("Action", input.getString("Action"));
                handlingGiveAllReparationFormWorkOn(input.getInt("User_id"));
                break;
            case "workOnUpdate":
                builder.add("Action", input.getString("Action"));
                handlingWorkOnUpdate(input.getInt("ReparationForm_Id"), input.getInt("User_id"));
                break;
            default:
                builder.add("Action", "None");
                builder.add("State", "Action not found");
                break;
        }
        output = builder.build();
        ViewModel.println("(" + ipClient + ") Getting response (Action : " + output.getString("Action") + " AND State : " + output.getString("State") + ")");
        return output;
    }
    
    /**
     * Method to handle the getAllReparationForm action, build the JsonObject with the list of ReparationForm
     * 
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException
     * @throws ParseException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException 
     */
    private void handlingGiveAllReparationForm() throws ClassNotFoundException, SQLException, InterruptedException, ParseException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        List<ReparationForm> lRf = ReparationFormDAO.getReparationFormAll();
        if (!lRf.isEmpty()) {
            builder.add("State", "Success");
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            JsonObjectBuilder builderObjectList = Json.createObjectBuilder();
            int count = 0;
            for (ReparationForm rf : lRf) {
                rf.setPriorityLevel(ReparationFormDAO.getPriorityLevelInReparationForm(rf));
                builderObjectList.add("ReparationForm_" + count, SerializableJson.serialize(rf));
                arrayBuilder.add(builderObjectList);
                count++;
            }
            builder.add("ListReparationForm", arrayBuilder);
        } else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "List of reparation form empty");
        }
    }
    
    private void handlingGiveAllReparationFormWorkOn(int userId) throws ClassNotFoundException, SQLException, InterruptedException, ParseException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        List<ReparationForm> lRf = ReparationFormDAO.getReparationFormAllWorkOn(userId);
        if (!lRf.isEmpty()) {
            builder.add("State", "Success");
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            JsonObjectBuilder builderObjectList = Json.createObjectBuilder();
            int count = 0;
            for (ReparationForm rf : lRf) {
                rf.setPriorityLevel(ReparationFormDAO.getPriorityLevelInReparationForm(rf));
                builderObjectList.add("ReparationForm_" + count, SerializableJson.serialize(rf));
                arrayBuilder.add(builderObjectList);
                count++;
            }
            builder.add("ListReparationFormWorkOn", arrayBuilder);
        } else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "No reparationform for this user");
        }
    }
    
    private void handlingWorkOnUpdate(int idForm, int idUser) throws SQLException, ClassNotFoundException, InterruptedException{
        int result = ReparationFormDAO.updateWorkOn(idForm, idUser);
        if(result != 0){
            builder.add("State", "Success");
        }else{
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't update the database to work on this reparationform");
        }
    }
    
    
}
