package data.processing;

import core.server.model.Bike;
import core.server.model.BikeDAO;
import core.server.model.Breakdown;
import core.server.model.BreakdownDAO;
import core.server.model.Car;
import core.server.model.CarDAO;
import core.server.model.CarModel;
import core.server.model.DateFormatted;
import core.server.model.NFCCase;
import core.server.model.Part;
import core.server.model.PartDAO;
import core.server.model.RFIDChip;
import core.server.model.SerializableJson;
import core.server.model.ViewModel;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

/**
 *
 * @author Pierre.TokerKovacic
 */
public class ProcessDataVehicleDetails {
    JsonObject input;
    JsonObject output;

    JsonObjectBuilder builder;

    String ipClient;
    
    /**
     * Set all the attributes and initialize the JsonObjectBuilder, this builder
     * will build the outputJson
     *
     * @param jsonString input string of the client, should be a JsonObject
     * @param ip ip of the client
     */
    public ProcessDataVehicleDetails(String jsonString, String ip) {
        JsonReader reader = Json.createReader(new StringReader(jsonString));
        input = reader.readObject();
        builder = Json.createObjectBuilder();
        ipClient = ip;
        ViewModel.println("(" + ipClient + ") Processing request (Action : " + input.getString("Action") + ",View : "+ input.getString("View") +")");
    }
    
    /**
     * Do a certain process depending on the Action JsonValue of the input
     * JsonObject
     *
     * @return output jsonObject which will be sent to the client
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     * @throws ParseException
     */
    public JsonObject getResult() throws SQLException, IOException, ClassNotFoundException, InterruptedException, ParseException, IllegalAccessException, InvocationTargetException,NoSuchMethodException {
        switch (input.getString("Action")) {
            case "vehicleDetails":
                builder.add("Action", input.getString("Action"));
                handlingVehicleDetails(input.getInt("IdVehicle"), input.getString("Table"));
                break;
            default:
                builder.add("Action", "None");
                builder.add("State", "Action not found");
                break;
        }
        output = builder.build();
        ViewModel.println("(" + ipClient + ") Getting response (Action : " + output.getString("Action") + " AND State : " + output.getString("State") + ")");
        return output;
    }
    
    /**
     * Method which handle the action "Car details" 
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    private void handlingVehicleDetails(int idVehicle, String table) throws SQLException, IOException, ClassNotFoundException, InterruptedException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, ParseException {
        List<Breakdown> breakdowns = BreakdownDAO.getHaveListByVehicleId(idVehicle, table);
        List<Part> parts = PartDAO.getUsedListByVehicleId(idVehicle, table);
        switch (table){
            case "Car":
                Car carDetails = CarDAO.getCarById(idVehicle);
                Car car = new Car(carDetails.getId(),carDetails.getMatriculation(),carDetails.getPurchaseDate(),carDetails.getLastRevision(),carDetails.getNfcCase(),carDetails.getModel(),parts,breakdowns);
                builder.add("State", "Success");
                builder.add("Car",SerializableJson.serialize(car));
                break;
            case "Bike":
                Bike bikeDetails = BikeDAO.getBikeById(idVehicle);
                Bike bike = new Bike(bikeDetails.getId(),bikeDetails.getPurchaseDate(),bikeDetails.getLastRevision(),bikeDetails.getRfidChip(),parts,breakdowns);
                builder.add("State", "Success");
                builder.add("Bike",SerializableJson.serialize(bike));
                break;
            default :
                System.out.println("Error, any type of vehicle was founded");
        }
        
    }
}
