package data.processing;

import core.server.model.User;
import core.server.model.ViewModel;
import core.server.network.Server;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

/**
 * This class do a process depending on the input string of the Client The input
 * string should be a JsonObject containing a "Action" jsonvalue
 *
 * @author Stephane.Schenkel, Charles.Santerre, Brice.Boutamdja
 */
public class ProcessData {

    String json;
    JsonObject input;
    JsonObject output;

    JsonObjectBuilder builder;

    String ipClient;

    /**
     * Set all the attributes and initialize the JsonObjectBuilder, this builder
     * will build the outputJson
     *
     * @param jsonString input string of the client, should be a JsonObject
     * @param ip ip of the client
     */
    public ProcessData(String jsonString, String ip) {
        JsonReader reader = Json.createReader(new StringReader(jsonString));
        json = jsonString;
        input = reader.readObject();
        builder = Json.createObjectBuilder();
        ipClient = ip;
        output = null;
        ViewModel.println("(" + ipClient + ")" + ProcessData.class.getSimpleName() + " (Action : " + input.getString("Action") + ", View : "+ input.getString("View") +")");
    }

    /**
     * Create a processData for the specified view and get the result
     * @return
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     * @throws ParseException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException 
     */
    public JsonObject getResult() throws SQLException, IOException, ClassNotFoundException, InterruptedException, ParseException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        if(input.getString("Action").equals("Disconnection")){
            User user = new User(input.getInt("User_id"), input.getString("User_login"), ipClient);
            Server.cListUsers.disconnection(user);
        }else{
            boolean reconnection = false;
            if(!input.getString("Action").equals("Connection")){
                User user = new User(input.getInt("User_id"), input.getString("User_login"), ipClient);
                boolean askConnection = Server.cListUsers.checkList(user);
                if(askConnection){
                    reconnection = true;
                }
            }
            if(!reconnection){
                switch (input.getString("View")) {
                    case "ConnectWindow":
                        ProcessDataConnectWindow pdConnect = new ProcessDataConnectWindow(json, ipClient);
                        output = pdConnect.getResult();
                        break;
                    case "ReparatorMenuWindow":
                        ProcessDataReparatorMenuWindow processDataMenu = new ProcessDataReparatorMenuWindow(json, ipClient);
                        output = processDataMenu.getResult();
                        break;
                    case "ScanVehicleWindow":
                        ProcessDataScanVehicleWindow pdScan = new ProcessDataScanVehicleWindow(json, ipClient);
                        output = pdScan.getResult();
                        break;
                    case "ReparationFormDetailWindow":
                        ProcessDataReparationFormDetailWindow processDataReparationFormDetailWindow = new ProcessDataReparationFormDetailWindow(json, ipClient);
                        output = processDataReparationFormDetailWindow.getResult();
                        break;
                    case "ReparationFormMenuWindow":
                        ProcessDataReparationFormMenuWindow pdRepFormMenuWindow = new ProcessDataReparationFormMenuWindow(json, ipClient);
                        output = pdRepFormMenuWindow.getResult();
                        break;
                    case "DepotStatWindow":
                        ProcessDataDepotStatWindow processDataDepotStatWindow = new ProcessDataDepotStatWindow(json, ipClient);
                        output = processDataDepotStatWindow.getResult();
                        break;
                    case "TypeVehicleStatWindow":
                        ProcessDataVehicleStatWindow processDataVehicleStatWindow = new ProcessDataVehicleStatWindow(json, ipClient);
                        output = processDataVehicleStatWindow.getResult();
                        break;
                    case "ReparatorStatWindow":
                        ProcessDataReparatorStatWindow processDataReparatorStatWindow = new ProcessDataReparatorStatWindow(json, ipClient);
                        output = processDataReparatorStatWindow.getResult();
                        break;
                    case "VehiclesWindow":
                        ProcessDataVehiclesWindow pdRepFormVehicleWindow = new ProcessDataVehiclesWindow(json, ipClient);
                        output = pdRepFormVehicleWindow.getResult();
                        break;  
                    case "VehicleDetails":
                        ProcessDataVehicleDetails pdRepFormVehicleDetails = new ProcessDataVehicleDetails(json, ipClient);
                        output = pdRepFormVehicleDetails.getResult();
                        break;
                     case "ReparatorEntryWindow":
                        ProcessDataReparationEntryFormWindow pdEntry = new ProcessDataReparationEntryFormWindow(json, ipClient);
                        output = pdEntry.getResult();
                        break;
                     default:
                        builder.add("Action", "None");
                        builder.add("State", "Action not found");
                        output = builder.build();
                        break;
                }
            }else{
                builder.add("Action", "Reconnection_Asked");
                builder.add("State", "User session timeout");
                output = builder.build();
            }
        }
        return output;
    }
}
