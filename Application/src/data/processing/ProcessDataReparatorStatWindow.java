/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.processing;

import core.server.model.BreakdownDAO;
import core.server.model.SerializableJson;
import core.server.model.User;
import core.server.model.UserDAO;
import core.server.model.ViewModel;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

/**
 *
 * @author pds_user
 */
class ProcessDataReparatorStatWindow {
    JsonObject input;
    JsonObject output;

    JsonObjectBuilder builder;

    String ipClient;
    
    public ProcessDataReparatorStatWindow (String jsonString, String ip){
        JsonReader reader = Json.createReader(new StringReader (jsonString));
        input = reader.readObject();
        builder = Json.createObjectBuilder();
        ipClient = ip;
        ViewModel.println("(" + ipClient + ") Processing request (Action : " + input.getString("Action") + ")");
    }
    
    public JsonObject getResult() throws SQLException, IOException, ClassNotFoundException, InterruptedException, ParseException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        switch (input.getString("Action")){
            case "getAllReparator" :
                builder.add("Action", input.getString("Action"));
                handlingGetAllReparator();
                break;
            case "getNumberReparationToday":
                builder.add("Action", input.getString("Action"));
                handlingGetNumberReparationToday(input.getInt("id"));
                break;
            case "getNumberReparationThisMonth":
                builder.add("Action", input.getString("Action"));
                handlingGetNumberReparationThisMonth(input.getInt("id"));
                break;
            case "getNumberReparationThisYear":
                builder.add("Action", input.getString("Action"));
                handlingGetNumberReparationThisYear(input.getInt("id"));
                break;
            case "getNumberReparationTotal":
                builder.add("Action", input.getString("Action"));
                handlingGetNumberReparationTotal(input.getInt("id"));
                break;
            default:
                builder.add("Action", "None");
                builder.add("State", "Action not found");
                break;
        }
        output=builder.build();
        ViewModel.println("("+ipClient + ") Getting response (Action : " + output.getString("Action") + " AND State : " + output.getString("State") + ")");
        return output;
    }
    
    private void handlingGetAllReparator() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        List<User> list = UserDAO.getAllReparator();
        if(!list.isEmpty()){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            for(User line : list){
                arrayBuilder.add(SerializableJson.serialize(line));
            }
            builder.add("State", "Success");
            builder.add("AllReparator", arrayBuilder.build());
        }else{
            builder.add("State", "Error");
            builder.add("ErrorMessage", "No reparator found in the database");
        }
    }
    
    private void handlingGetNumberReparationTotal(int id) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int result = UserDAO.getNumberReparationTotal(id);
        if(result >=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(result);
            builder.add("State", "Success");
            builder.add("GetNumberReparationTotal", arrayBuilder.build());
        }else{
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Not found in the database");
        }
    }
    
    private void handlingGetNumberReparationToday(int id) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int result = UserDAO.getNumberReparationToday(id);
        if(result >=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(result);
            builder.add("State", "Success");
            builder.add("GetNumberReparationToday", arrayBuilder.build());
        }else{
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Not found in the database");
        }
    }
    
    private void handlingGetNumberReparationThisMonth(int id) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int result = UserDAO.getNumberReparationThisMonth(id);
        if(result >=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(result);
            builder.add("State", "Success");
            builder.add("GetNumberReparationThisMonth", arrayBuilder.build());
        }else{
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Not found in the database");
        }
    }
    
    private void handlingGetNumberReparationThisYear(int id) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int result = UserDAO.getNumberReparationThisYear(id);
        if(result >=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(result);
            builder.add("State", "Success");
            builder.add("GetNumberReparationThisYear", arrayBuilder.build());
        }else{
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Not found in the database");
        }
    }
}
