package data.processing;

import core.server.model.Parking;
import core.server.model.ParkingDAO;
import core.server.model.ReparationForm;
import core.server.model.ReparationFormDAO;
import core.server.model.SerializableJson;
import core.server.model.ViewModel;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

/**
 *
 * @author Brice.Boutamdja
 */
public class ProcessDataReparatorMenuWindow {

    JsonObject input;
    JsonObject output;

    JsonObjectBuilder builder;

    String ipClient;

    /**
     * Set all the attributes and initialize the JsonObjectBuilder, this builder
     * will build the outputJson
     *
     * @param jsonString input string of the client, should be a JsonObject
     * @param ip ip of the client
     */
    public ProcessDataReparatorMenuWindow(String jsonString, String ip) {
        JsonReader reader = Json.createReader(new StringReader(jsonString));
        input = reader.readObject();
        builder = Json.createObjectBuilder();
        ipClient = ip;
        ViewModel.println("(" + ipClient + ") Processing request (Action : " + input.getString("Action") + ")");
    }

    /**
     * Do a certain process depending on the Action JsonValue of the input
     * JsonObject
     *
     * @return output jsonObject which will be sent to the client
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     * @throws ParseException
     */
    public JsonObject getResult() throws SQLException, IOException, ClassNotFoundException, InterruptedException, ParseException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        switch (input.getString("Action")) {
            case "giveAllReparationFormAndAllWorkOn":
                builder.add("Action", input.getString("Action"));
                handlingGiveAllReparationFormAndAllWorkOn(input.getInt("User_id"));
                break;
            case "giveListAvailableParkingList":
                builder.add("Action", input.getString("Action"));
                handlingGiveListAvailableParkingList();
                break;
            default:
                builder.add("Action", "None");
                builder.add("State", "Action not found");
                break;
        }
        output = builder.build();
        ViewModel.println("(" + ipClient + ") Getting response (Action : " + output.getString("Action") + " AND State : " + output.getString("State") + ")");
        return output;
    }

    /**
     * Method which handle the action "GetListBrokenVehicle" It gets all Car and
     * Bike object which are in reparationForm Add an ErrorMessage to the
     * builder if no vehicle found
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     * @throws ParseException
     */
    private void handlingGiveAllReparationFormAndAllWorkOn(int userId) throws ClassNotFoundException, SQLException, InterruptedException, ParseException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        List<ReparationForm> lRf = ReparationFormDAO.getReparationFormAll();
        List<ReparationForm> lRfWO = ReparationFormDAO.getReparationFormAllWorkOn(userId);
        if (!lRf.isEmpty() || !lRfWO.isEmpty()) {
            builder.add("State", "Success");
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            JsonObjectBuilder builderObjectList = Json.createObjectBuilder();
            int count = 0;
            for (ReparationForm rf : lRf) {
                builderObjectList.add("ReparationForm_" + count, SerializableJson.serialize(rf));
                arrayBuilder.add(builderObjectList);
                count++;
            }
            builder.add("ListReparationForm", arrayBuilder);
            
            arrayBuilder = Json.createArrayBuilder();
            builderObjectList = Json.createObjectBuilder();
            count = 0;
            for (ReparationForm rf : lRfWO) {
                builderObjectList.add("ReparationForm_" + count, SerializableJson.serialize(rf));
                arrayBuilder.add(builderObjectList);
                count++;
            }
            builder.add("ListReparationFormWorkOn", arrayBuilder);
        } else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "List of reparation form empty");
        }
    }
    
    private void handlingGiveListAvailableParkingList() throws ClassNotFoundException, SQLException, InterruptedException, IllegalAccessException, NoSuchMethodException, IllegalArgumentException, ParseException, InvocationTargetException{
        ArrayList<Parking> listPark = ParkingDAO.getListAvailableParking();
        if (listPark != null) {
            builder.add("State", "Success");
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            JsonObjectBuilder builderObjectList = Json.createObjectBuilder();
            int i = 0;        
            for(Parking p : listPark){
                builderObjectList.add("Parking_" + i, SerializableJson.serialize(p));
                arrayBuilder.add(builderObjectList);
                i++;
            }
            builder.add("Parking_list", arrayBuilder);
        } else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "No parking place available");
        }
    }
}
