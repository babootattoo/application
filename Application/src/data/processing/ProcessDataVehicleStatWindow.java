/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.processing;

import core.server.model.BreakdownDAO;
import core.server.model.PartDAO;
import core.server.model.UserDAO;
import core.server.model.ViewModel;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

/**
 *
 * @author pds_user
 */
public class ProcessDataVehicleStatWindow {
    JsonObject input;
    JsonObject output;

    JsonObjectBuilder builder;

    String ipClient;
    
    public ProcessDataVehicleStatWindow (String jsonString, String ip){
        JsonReader reader = Json.createReader(new StringReader (jsonString));
        input = reader.readObject();
        builder = Json.createObjectBuilder();
        ipClient = ip;
        ViewModel.println("(" + ipClient + ") Processing request (Action : " + input.getString("Action") + ")");
    }
    
    public JsonObject getResult() throws SQLException, IOException, ClassNotFoundException, InterruptedException, ParseException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        switch (input.getString("Action")){
            case "getNbBikeTotalBreakdown":
                builder.add("Action", input.getString("Action"));
                handlingGetNbBikeTotalBreakdown();
                break;
            case "getNbCarTotalBreakdown":
                builder.add("Action", input.getString("Action"));
                handlingGetNbCarTotalBreakdown();
                break;
            case "getNbBikeBreakdownThisMonth":
                builder.add("Action", input.getString("Action"));
                handlingGetNbBikeBreakdownThisMonth();
                break;
            case "getNbCarBreakdownThisMonth":
                builder.add("Action", input.getString("Action"));
                handlingGetNbCarBreakdownThisMonth();
                break;
            case "getNbBikeBreakdownThisYear":
                builder.add("Action", input.getString("Action"));
                handlingGetNbBikeBreakdownThisYear();
                break;
            case "getNbCarBreakdownThisYear":
                builder.add("Action", input.getString("Action"));
                handlingGetNbCarBreakdownThisYear();
                break;
            case "getAvgBikeBreakdownThisMonth":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgBikeBreakdownThisMonth();
                break;
            case "getAvgCarBreakdownThisMonth":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgCarBreakdownThisMonth();
                break;
            case "getAvgBikeBreakdownThisYear":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgBikeBreakdownThisYear();
                break;
            case "getAvgCarBreakdownThisYear":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgCarBreakdownThisYear();
                break;  
            case "getListBreakdownCar":
                builder.add("Action", input.getString("Action"));
                handlingGetListBreakdownCar();
                break;
            case "getListBreakdownBike":
                builder.add("Action", input.getString("Action"));
                handlingGetListBreakdownBike();
                break;  
            case "getPriceTotalBreakdownCar":
                builder.add("Action", input.getString("Action"));
                handlingGetPriceTotalBreakdownCar();
                break;
            case "getPriceTotalBreakdownBike":
                builder.add("Action", input.getString("Action"));
                handlingGetPriceTotalBreakdownBike();
                break; 
            case "getPriceThisMonthBreakdownCar":
                builder.add("Action", input.getString("Action"));
                handlingGetPriceThisMonthBreakdownCar();
                break;
            case "getPriceThisMonthBreakdownBike":
                builder.add("Action", input.getString("Action"));
                handlingGetPriceThisMonthBreakdownBike();
                break;
            case "getPriceThisYearBreakdownCar":
                builder.add("Action", input.getString("Action"));
                handlingGetPriceThisYearBreakdownCar();
                break;
            case "getPriceThisYearBreakdownBike":
                builder.add("Action", input.getString("Action"));
                handlingGetPriceThisYearBreakdownBike();
                break;
            case "getAvgPriceMonthBreakdownCar":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgPriceMonthBreakdownCar();
                break;
            case "getAvgPriceMonthBreakdownBike":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgPriceMonthBreakdownBike();
                break;
            case "getAvgPriceYearBreakdownCar":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgPriceYearBreakdownCar();
                break;
            case "getAvgPriceYearBreakdownBike":
                builder.add("Action", input.getString("Action"));
                handlingGetAvgPriceYearBreakdownBike();
                break;
            default:
                builder.add("Action", "None");
                builder.add("State", "Action not found");
                break;
        }
        output=builder.build();
        ViewModel.println("("+ipClient + ") Getting response (Action : " + output.getString("Action") + " AND State : " + output.getString("State") + ")");
        return output;
    }
    
    public void handlingGetNbCarTotalBreakdown() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int number = BreakdownDAO.getNbCarTotalBreakdown();
        if (number!=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("NbCarTotalBreakdown", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    public void handlingGetNbBikeTotalBreakdown() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int number = BreakdownDAO.getNbBikeTotalBreakdown();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("NbBikeTotalBreakdown", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }

    private void handlingGetNbBikeBreakdownThisMonth() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int number = BreakdownDAO.getNbBikeBreakdownThisMonth();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("NbBikeBreakdownMonth", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }

    private void handlingGetNbCarBreakdownThisMonth() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int number = BreakdownDAO.getNbCarBreakdownThisMonth();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("NbCarBreakdownMonth", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetNbBikeBreakdownThisYear() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int number = BreakdownDAO.getNbBikeBreakdownThisYear();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("NbBikeBreakdownYear", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }

    private void handlingGetNbCarBreakdownThisYear() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        int number = BreakdownDAO.getNbCarBreakdownThisYear();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("NbCarBreakdownYear", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetAvgCarBreakdownThisYear() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = BreakdownDAO.getAvgCarBreakdownByYear();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("AvgCarBreakdownThisYear", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetAvgCarBreakdownThisMonth() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = BreakdownDAO.getAvgCarBreakdownByMonth();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("AvgCarBreakdownThisMonth", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetAvgBikeBreakdownThisYear() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = BreakdownDAO.getAvgBikeBreakdownByYear();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("AvgBikeBreakdownThisYear", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetAvgBikeBreakdownThisMonth() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = BreakdownDAO.getAvgBikeBreakdownByMonth();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("AvgBikeBreakdownThisMonth", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetListBreakdownBike() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        List<String[]> list = BreakdownDAO.getListBreakdownBike();
        if (!list.isEmpty()){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            for(String[] line : list){
                JsonArrayBuilder array = Json.createArrayBuilder();
                array.add(line[0]);
                array.add(line[1]);
                arrayBuilder.add(array); 
            }
            builder.add("State", "Success");
            builder.add("GetListBreakdownBike", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetListBreakdownCar() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        List<String[]> list = BreakdownDAO.getListBreakdownCar();
        if (!list.isEmpty()){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            for(String[] line : list){
                JsonArrayBuilder array = Json.createArrayBuilder();
                array.add(line[0]);
                array.add(line[1]);
                arrayBuilder.add(array); 
            }
            builder.add("State", "Success");
            builder.add("GetListBreakdownCar", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetPriceTotalBreakdownBike() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getPriceTotalBreakdownBike();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetPriceTotalBreakdownBike", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetPriceTotalBreakdownCar() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getPriceTotalBreakdownCar();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetPriceTotalBreakdownCar", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetPriceThisYearBreakdownCar() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getPriceThisYearBreakdownCar();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetPriceThisYearBreakdownCar", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetPriceThisYearBreakdownBike() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getPriceThisYearBreakdownBike();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetPriceThisYearBreakdownBike", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetPriceThisMonthBreakdownCar() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getPriceThisMonthBreakdownCar();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetPriceThisMonthBreakdownCar", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetPriceThisMonthBreakdownBike() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getPriceThisMonthBreakdownBike();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetPriceThisMonthBreakdownBike", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetAvgPriceYearBreakdownCar() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getAvgPriceYearBreakdownCar();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetAvgPriceYearBreakdownCar", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetAvgPriceYearBreakdownBike() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getAvgPriceYearBreakdownBike();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetAvgPriceYearBreakdownBike", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetAvgPriceMonthBreakdownBike() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getAvgPriceMonthBreakdownBike();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetAvgPriceMonthBreakdownBike", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    private void handlingGetAvgPriceMonthBreakdownCar() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, SQLException, InterruptedException {
        double number = PartDAO.getAvgPriceMonthBreakdownCar();
        if (number>=0){
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            arrayBuilder.add(number);
            builder.add("State", "Success");
            builder.add("GetAvgPriceMonthBreakdownCar", arrayBuilder.build());
        }
        else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Couldn't find any breakdown");
        }
    }
    
    
    
    
}
