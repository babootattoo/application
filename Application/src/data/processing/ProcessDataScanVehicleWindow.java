package data.processing;

import core.server.model.NFCCase;
import core.server.model.NFCCaseDAO;
import core.server.model.Parking;
import core.server.model.ParkingDAO;
import core.server.model.RFIDChip;
import core.server.model.RFIDChipDAO;
import core.server.model.ReparationForm;
import core.server.model.ReparationFormDAO;
import core.server.model.SerializableJson;
import core.server.model.ViewModel;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

/**
 *
 * @author Charles.Santerre
 */
public class ProcessDataScanVehicleWindow {

    JsonObject input;
    JsonObject output;

    JsonObjectBuilder builder;

    String ipClient;

    /**
     * Set all the attributes and initialize the JsonObjectBuilder, this builder
     * will build the outputJson
     *
     * @param jsonString input string of the client, should be a JsonObject
     * @param ip ip of the client
     */
    public ProcessDataScanVehicleWindow(String jsonString, String ip) {
        JsonReader reader = Json.createReader(new StringReader(jsonString));
        input = reader.readObject();
        builder = Json.createObjectBuilder();
        ipClient = ip;
        ViewModel.println("(" + ipClient + ") Processing request (Action : " + input.getString("Action") + ",View : " + input.getString("View") + ")");
    }

    /**
     * Do a certain process depending on the Action JsonValue of the input
     * JsonObject
     *
     * @return output jsonObject which will be sent to the client
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     * @throws ParseException
     */
    public JsonObject getResult() throws SQLException, IOException, ClassNotFoundException, InterruptedException, ParseException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        switch (input.getString("Action")) {
            case "createReparationForm":
                builder.add("Action", input.getString("Action"));
                handlingCreateReparationForm();
                break;
            case "giveLastReparationForm" :
                builder.add("Action", input.getString("Action"));
                handlingGiveLastReparationForm();
                break;
            default:
                builder.add("Action", "None");
                builder.add("State", "Action not found");
                break;
        }
        output = builder.build();
        ViewModel.println("(" + ipClient + ") Getting response (Action : " + output.getString("Action") + " AND State : " + output.getString("State") + ")");
        return output;
    }

    private void handlingCreateReparationForm() throws ClassNotFoundException, SQLException, InterruptedException, IllegalAccessException, NoSuchMethodException, IllegalArgumentException, ParseException, InvocationTargetException, ParseException, InvocationTargetException {
        String tag = input.getString("TagVehicle");
        NFCCase nfc = null;
        RFIDChip rfid = null;
        switch(tag.length()){
            case 8:
                nfc = NFCCaseDAO.getCaseByTag(tag);
                break;
            case 12:
                rfid = RFIDChipDAO.getChipByTag(tag);
                break;
        }
        if(nfc != null || rfid != null){
            int answer = ReparationFormDAO.createReparationForm(input.getString("TagVehicle"), input.getInt("idParking"));
            if (answer == 1) {
                ArrayList<Parking> listPark = ParkingDAO.getListAvailableParking();
                if (listPark != null) {
                    builder.add("State", "Success");
                    JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
                    JsonObjectBuilder builderObjectList = Json.createObjectBuilder();
                    int i = 0;
                    for (Parking p : listPark) {
                        builderObjectList.add("Parking_" + i, SerializableJson.serialize(p));
                        arrayBuilder.add(builderObjectList);
                        i++;
                    }
                    builder.add("Parking_list", arrayBuilder);
                }
            } else {
                builder.add("State", "Error");
                builder.add("ErrorMessage", "An error as occured");
            }
        }else{
                builder.add("State", "Error");
                builder.add("ErrorMessage", "No RFIDChip and NFCCase found with the specified tag");
        }
    }
    
    private void handlingGiveLastReparationForm() throws ClassNotFoundException, SQLException, InterruptedException, ParseException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        ReparationForm Rf = ReparationFormDAO.getLastReparationForm();
        if (Rf != null) {
            builder.add("State", "Success");
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            JsonObjectBuilder builderObject = Json.createObjectBuilder();
            builderObject.add("ReparationForm", SerializableJson.serialize(Rf));
            arrayBuilder.add(builderObject);

            builder.add("LastReparationForm", arrayBuilder);
        } else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "List of reparation form empty");
        }
    }
}
