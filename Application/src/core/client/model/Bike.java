package core.client.model;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 *
 * @author Stephane.Schenkel
 */
public class Bike{

    /**
     * Declaration of attribute
     *
     * @param id Int
     * @param purchaseDate DateFormatted
     * @param lastRevision DateFormatted
     */
    private int id;
    private DateFormatted purchaseDate;
    private DateFormatted lastRevision;
    private RFIDChip rfidChip;
    
    private List<Part> parts;
    private List<Breakdown> breakdowns;

    public void setRfidChip(RFIDChip rfidChip) {
        this.rfidChip = rfidChip;
    }

    public void setParts(List<Part> parts) {
        this.parts = parts;
    }

    public void setBreakdowns(List<Breakdown> breakdowns) {
        this.breakdowns = breakdowns;
    }

    public RFIDChip getRfidChip() {
        return rfidChip;
    }

    public List<Part> getParts() {
        return parts;
    }

    public List<Breakdown> getBreakdowns() {
        return breakdowns;
    }

    public Bike(int id, DateFormatted purchaseDate, DateFormatted lastRevision, RFIDChip rfidChip, List<Part> parts, List<Breakdown> breakdowns) {
        this.id = id;
        this.purchaseDate = purchaseDate;
        this.lastRevision = lastRevision;
        this.rfidChip = rfidChip;
        this.parts = parts;
        this.breakdowns = breakdowns;
    }

    public static Bike deserialize(JsonObject inputJson) throws ParseException {
        if (inputJson.getString("id").equals("-1")) {
            return null;
        } else {
            RFIDChip rfid = RFIDChip.deserialize(inputJson.getJsonObject("rfidChip"));
            
            JsonArray partList = inputJson.getJsonArray("parts_list");
            JsonArray breakdownList = inputJson.getJsonArray("breakdowns_list");
            List<Part> listPart = new ArrayList<Part>();
            List<Breakdown> listBreakdown = new ArrayList<Breakdown>();
            for(int i = 0; i < partList.size(); i++){
                listPart.add(Part.deserialize(partList.getJsonObject(i).getJsonObject("Part_"+i)));
            }
            for(int i = 0; i < breakdownList.size(); i++){
                listBreakdown.add(Breakdown.deserialize(breakdownList.getJsonObject(i).getJsonObject("Breakdown_"+i)));
            }
            return new Bike(Integer.parseInt(inputJson.getString("id")), new DateFormatted(inputJson.getString("purchaseDate"), false), new DateFormatted(inputJson.getString("lastRevision"), false), rfid, listPart, listBreakdown);
        }
    }

    public int getId() {
        return id;
    }

    public DateFormatted getPurchaseDate() {
        return purchaseDate;
    }

    public DateFormatted getLastRevision() {
        return lastRevision;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPurchaseDate(DateFormatted purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public void setLastRevision(DateFormatted lastRevision) {
        this.lastRevision = lastRevision;
    }
}
