package core.client.model;

import core.client.network.Communication;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author Stephane.Schenkel, Pierre.TokerKovacic
 */
public class CardStateDAO {
    
    /**
     * This method return JsonObject to get every cardState with JsonObjectBuilder
     * @param classView
     * @param userLogged
     * @return 
     */
    private static JsonObject getAllCardStateRequestJson(String classView, User userLogged){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAllCardState");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect json with the list of cardState
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getAllCardState(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getAllCardStateRequestJson(classView, userLogged));
        result = com.getData();
        com.close();
        return result;
    }
    
    
    private static JsonObject getCountCardStateRequestJson(String classView, User userLogged){
        JsonObject json;
        System.out.println("JSON getCount = " + classView);
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getCountCardState");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        System.out.println(json);
        return json;
    }
    
    /**
     * This method collect json with count of card state
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getCountCardState(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getCountCardStateRequestJson(classView, userLogged));
        result = com.getData();
        com.close();
        return result;
    }

    
}
