package core.client.model;

import core.client.network.Communication;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author Stephane.Schenkel
 */
public class BreakdownDAO {
    
    /**
     * This method return JsonObject for get listbreakdown depends on carmodel with JsonObjectBuilder
     * @param idCarModel
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestListBreakdownCarModel(int idCarModel, int idCar, User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getListBreakdownCarModel");
        builder.add("ConnectionState", "Try");
        builder.add("IdCarModel", idCarModel);
        builder.add("IdCar", idCar);
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect json with the list of breakdown of carModel
     * @param idCarModel
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getListBreakdownCarModel(int idCarModel, int idCar, User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestListBreakdownCarModel(idCarModel, idCar, userLogged, classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    /**
     * This method return JsonObject for get listbreakdown of every Bike with JsonObjectBuilder
     * @param idCarModel
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestListBreakdownBike(int idBike, User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getListBreakdownBike");
        builder.add("ConnectionState", "Try");
        builder.add("IdBike", idBike);
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect json with the list of breakdown of bike
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getListBreakdownBike(int idBike, User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestListBreakdownBike(idBike, userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    
    public static JsonObject getNbBreakdownThisYear(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonNbBreakdownThisYear(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonNbBreakdownThisYear(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getNumberBreakdownThisYear");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getNbBreakdownThisMonth(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonNbBreakdownThisMonth(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonNbBreakdownThisMonth(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getNumberBreakdownThisMonth");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    
    public static JsonObject getAvgBreakdownByMonth(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgBreakdownByMonth(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgBreakdownByMonth(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgBreakdownByMonth");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getAvgBreakdownByYear(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgBreakdownByYear(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgBreakdownByYear(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgBreakdownByYear");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getNbCarTotalBreakdown(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonNbCarTotalBreakdown(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonNbCarTotalBreakdown(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getNbCarTotalBreakdown");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getNbBikeTotalBreakdown(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonNbBikeTotalBreakdown(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonNbBikeTotalBreakdown(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getNbBikeTotalBreakdown");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getNbCarBreakdownThisMonth(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonNbCarBreakdownThisMonth(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonNbCarBreakdownThisMonth(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getNbCarBreakdownThisMonth");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getNbBikeBreakdownThisMonth(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonNbBikeBreakdownThisMonth(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonNbBikeBreakdownThisMonth(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getNbBikeBreakdownThisMonth");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getNbCarBreakdownThisYear(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonNbCarBreakdownThisYear(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonNbCarBreakdownThisYear(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getNbCarBreakdownThisYear");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getNbBikeBreakdownThisYear(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonNbBikeBreakdownThisYear(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonNbBikeBreakdownThisYear(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getNbBikeBreakdownThisYear");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getAvgCarBreakdownByMonth(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgCarBreakdownByMonth(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgCarBreakdownByMonth(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgCarBreakdownThisMonth");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getAvgCarBreakdownByYear(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgCarBreakdownByYear(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgCarBreakdownByYear(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgCarBreakdownThisYear");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getAvgBikeBreakdownByMonth(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgBikeBreakdownByMonth(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgBikeBreakdownByMonth(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgBikeBreakdownThisMonth");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getAvgBikeBreakdownByYear(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgBikeBreakdownByYear(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgBikeBreakdownByYear(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgBikeBreakdownThisYear");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getListBreakdownCarStat(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonListBreakdownCarStat(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonListBreakdownCarStat(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getListBreakdownCar");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getListBreakdownBikeStat(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonListBreakdownBikeStat(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonListBreakdownBikeStat(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getListBreakdownBike");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method return JsonObject for get max priority level for every Bike with JsonObjectBuilder
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestListPriorityPerBike(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getPriorityPerBike");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect json with the list of priority level per bike
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getListPriorityPerBike(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestListPriorityPerBike(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
     /**
     * This method return JsonObject for get max priority level for every Car with JsonObjectBuilder
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestListPriorityPerCar(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getPriorityPerCar");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect json with the list of priority level per Car
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getListPriorityPerCar(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestListPriorityPerCar(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
}
