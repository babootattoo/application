package core.client.model;

import core.client.network.Communication;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author Stéphane.Schenkel
 */
public class CommentDAO {
    
    /**
     * This method return JsonObject to get comments of reparationForm with JsonObjectBuilder
     * @param id
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonCommentByFormId(int id, User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getCommentByFormId");
        builder.add("ConnectionState", "Try");
        builder.add("FormId", id);
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
 
    }
    
    /**
     * This method collect json with the list of comments of id's reparationform in parameter
     * @param id
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getCommentByFormId(int id, User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonCommentByFormId(id, userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    /**
     * This method return JsonObject to add comment of reparationForm with JsonObjectBuilder
     * @param idUser
     * @param idForm
     * @param message
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonAddComment(int idUser, int idForm, String message, User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "addComment");
        builder.add("ConnectionState", "Try");
        builder.add("UserId", idUser);
        builder.add("FormId", idForm);
        builder.add("Message", message);
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
 
    }
    
    /**
     * This method collect json with the comment of id's reparationform in parameter
     * @param idUser
     * @param idForm
     * @param message
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject addComment(int idUser, int idForm, String message, User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAddComment(idUser, idForm, message, userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
}
