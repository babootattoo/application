package core.client.model;

import core.client.network.Communication;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author Stephane.Schenkel, Charles.Santere, Brice.Boutamdja, Pierre.TokerKovacic
 */
public class VehicleDAO {

    /**
     * This method return JsonObject for get vehicle with JsonObjectBuilder
     *
     * @return JsonObject
     */
    private static JsonObject getListVehicleRequestJson() {
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "giveListVehicleWatingForReparation");
        builder.add("ConnectionState", "Try");
        json = builder.build();
        return json;
    }

    /**
     * This method collect json to vehicle*
     *
     * @return JsonObject
     * @throws IOException
     */
    public static JsonObject getListVehicleWaitingForReparation() throws IOException {
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getListVehicleRequestJson());
        result = com.getData();
        com.close();
        return result;
    }

    /**
     * This method return JsonObject to delete part attach to a vehicle with JsonObjectBuilder
     * @param idVehicle
     * @param idPart
     * @param table
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestDeletePart(int idVehicle, int idPart, String table, User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "deletePart");
        builder.add("ConnectionState", "Try");
        builder.add("IdVehicle", idVehicle);
        builder.add("IdPart", idPart);
        builder.add("Table", table);
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method delete a part attach to a vehicle
     * @param idVehicle
     * @param idPart
     * @param table
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject deletePart(int idVehicle, int idPart, String table, User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestDeletePart(idVehicle, idPart, table, userLogged, classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    /**
     * This method return JsonObject to add part to a vehicle with JsonObjectBuilder
     * @param idVehicle
     * @param idPart
     * @param table
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestAddPart(int idVehicle, int idPart, String table, User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "addPart");
        builder.add("ConnectionState", "Try");
        builder.add("IdVehicle", idVehicle);
        builder.add("IdPart", idPart);
        builder.add("Table", table);
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method add a part to a vehicle
     * @param idVehicle
     * @param idPart
     * @param table
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject addPart(int idVehicle, int idPart, String table, User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestAddPart(idVehicle, idPart, table, userLogged, classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    /**
     * This method return JsonObject to add breakdown to a vehicle with JsonObjectBuilder
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestAddBreakdown(int idVehicle, int idBreakdown, String table, User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "addBreakdown");
        builder.add("ConnectionState", "Try");
        builder.add("IdVehicle", idVehicle);
        builder.add("IdBreakdown", idBreakdown);
        builder.add("Table", table);
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method add a breakdown to a vehicle
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject addBreakdown(int idVehicle, int idBreakdown, String table, User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestAddBreakdown(idVehicle, idBreakdown, table, userLogged, classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    /**
     * This method return JsonObject to delete breakdown to a vehicle with JsonObjectBuilder
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestDeleteBreakdown(int idVehicle, int idBreakdown, String table, User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "deleteBreakdown");
        builder.add("ConnectionState", "Try");
        builder.add("IdVehicle", idVehicle);
        builder.add("IdBreakdown", idBreakdown);
        builder.add("Table", table);
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method delete a breakdown attach to a vehicle
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject deleteBreakdown(int idVehicle, int idBreakdown, String table, User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestDeleteBreakdown(idVehicle, idBreakdown, table, userLogged, classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    /**
     * This method return JsonObject to update breakdown of a vehicle with JsonObjectBuilder
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestMarkAsResolvedBreakdown(int idVehicle, int idBreakdown, String table, User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "markAsResolvedBreakdown");
        builder.add("ConnectionState", "Try");
        builder.add("IdVehicle", idVehicle);
        builder.add("IdBreakdown", idBreakdown);
        builder.add("Table", table);
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method update a breakdown attached to a vehicle
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject markAsResolvedBreakdown(int idVehicle, int idBreakdown, String table, User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestMarkAsResolvedBreakdown(idVehicle, idBreakdown, table, userLogged, classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    /**
     * This method get details of car
     * @param idVehicle
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getDetailsByID(int idVehicle, String table, User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestDetailsByID(idVehicle, table, userLogged, classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    /**
     * This method return JsonObject to vechicle details with JsonObjectBuilder
     * @param idVehicle
     * @return 
     */
    private static JsonObject getJsonRequestDetailsByID(int idVehicle, String table, User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "vehicleDetails");
        builder.add("ConnectionState", "Try");
        builder.add("IdVehicle", idVehicle);
        builder.add("Table", table);
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
}
