package core.client.model;

import javax.json.JsonObject;

/**
 *
 * @author Stephane.Schenkel
 */
public class Parking{
    
    /**
     * Declaration of attribute
     * @param id Int
     * @param occupied Boolean
     */
    private int id;
    private String placeNumber;
    private boolean occupied;
    private boolean bikePlace;

    public Parking(int id,String placeNumber, boolean occupied,boolean bikePlace) {
        this.id = id;
        this.placeNumber = placeNumber;
        this.occupied = occupied;
        this.bikePlace = bikePlace;
    }

    public static Parking deserialize(JsonObject inputJson){
        if(inputJson.getString("id").equals("-1")){
            return null;
        }else{
            return new Parking(Integer.parseInt(inputJson.getString("id")), inputJson.getString("placeNumber"),Boolean.parseBoolean(inputJson.getString("occupied")),Boolean.parseBoolean(inputJson.getString("bikePlace")));
        }
    }

    @Override
    public String toString() {
        return "Parking{" + "id=" + id + ", placeNumber=" + placeNumber + ", occupied=" + occupied + ", bikePlace=" + bikePlace + '}';
    }


    public String getPlaceNumber() {
        return placeNumber;
    }

    public void setPlaceNumber(String placeNumber) {
        this.placeNumber = placeNumber;
    }

    public boolean getBikePlace() {
        return bikePlace;
    }

    public void setBikePlace(boolean bikePlace) {
        this.bikePlace = bikePlace;
    }
    
    
    
    public Parking(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public boolean getOccupied() {
        return occupied;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }  
}
