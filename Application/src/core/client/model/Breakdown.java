package core.client.model;

import java.text.ParseException;
import javax.json.JsonObject;

/**
 *
 * @author stéphane_schenkel
 */
public class Breakdown {
    
    private int id;
    private String category;
    private String description;
    private int averageTimeMinutes;
    private int priorityLevel;

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    public void setResolved_date(DateFormatted resolved_date) {
        this.resolved_date = resolved_date;
    }

    public boolean getResolved() {
        return resolved;
    }

    public DateFormatted getResolved_date() {
        return resolved_date;
    }
    private boolean resolved;
    private DateFormatted resolved_date;
    
    private DateFormatted occuredDate;

    public Breakdown(int id, String category, String description, int averageTimeMinutes, int priorityLevel, DateFormatted occuredDate, boolean resolved, DateFormatted resolved_date) {
        this.id = id;
        this.category = category;
        this.description = description;
        this.averageTimeMinutes = averageTimeMinutes;
        this.priorityLevel = priorityLevel;
        this.occuredDate = occuredDate;
        this.resolved = resolved;
        this.resolved_date = resolved_date;
    }

    public int getAverageTimeMinutes() {
        return averageTimeMinutes;
    }

    public void setAverageTimeMinutes(int averageTimeMinutes) {
        this.averageTimeMinutes = averageTimeMinutes;
    }

    public int getPriorityLevel() {
        return priorityLevel;
    }

    public void setPriorityLevel(int priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    public int getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public DateFormatted getOccuredDate() {
        return occuredDate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setOccuredDate(DateFormatted occuredDate) {
        this.occuredDate = occuredDate;
    }
    
    public static Breakdown deserialize(JsonObject inputJson) throws ParseException{
        if(inputJson.getString("id").equals("-1")){
            return null;
        }else{
            return new Breakdown(Integer.parseInt(inputJson.getString("id")), inputJson.getString("category"), inputJson.getString("description"), Integer.parseInt(inputJson.getString("averageTimeMinutes")), Integer.parseInt(inputJson.getString("priorityLevel")), new DateFormatted(inputJson.getString("occuredDate"), false), Boolean.parseBoolean(inputJson.getString("resolved")), new DateFormatted(inputJson.getString("resolved_date"), true));
        }
    }
    
}
