package core.client.model;

import core.client.network.Communication;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author Charles.Santerre, Stephane.Schenkel
 */
public class PartDAO {
    
    /**
     * This method return JsonObject to get list of Part depends on carModel with JsonObjectBuilder
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestUsablePartCarModel(int idCarModel, User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getUsablePartCarModel");
        builder.add("ConnectionState", "Try");
        builder.add("IdCarModel", idCarModel);
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect json with the list of usablePart
     * @param idCarModel
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getListUsablePartCarModel(int idCarModel, User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestUsablePartCarModel(idCarModel, userLogged, classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    /**
     * This method return JsonObject to get list of Part of every bike with JsonObjectBuilder
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestListUsablePartBike(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getListUsablePartBike");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect json with the list of usablePart of PartBike
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getListUsablePartBike(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestListUsablePartBike(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    public static JsonObject getNumberBuyingPiecesThisYear(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonNumberBuyingPiecesThisYear(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonNumberBuyingPiecesThisYear(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getNumberBuyingPiecesThisYear");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getNumberBuyingPiecesThisMonth(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonNumberBuyingPiecesThisMonth(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonNumberBuyingPiecesThisMonth(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getNumberBuyingPiecesThisMonth");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getAvgPiecesByMonth(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgPiecesByMonth(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgPiecesByMonth(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgPiecesByMonth");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getAvgPiecesByYear(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgPiecesByYear(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgPiecesByYear(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgPiecesByYear");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getPriceThisYear(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonPriceThisYear(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonPriceThisYear(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getPriceThisYear");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getPriceThisMonth(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonPriceThisMonth(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonPriceThisMonth(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getPriceThisMonth");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getAvgPriceByMonth(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgPriceByMonth(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgPriceByMonth(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgPriceByMonth");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getAvgPriceByYear(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgPriceByYear(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgPriceByYear(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgPriceByYear");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getPriceThisYearBreakdownCar(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonPriceThisYearBreakdownCar(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonPriceThisYearBreakdownCar(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getPriceThisYearBreakdownCar");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getPriceThisYearBreakdownBike(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonPriceThisYearBreakdownBike(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonPriceThisYearBreakdownBike(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getPriceThisYearBreakdownBike");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getPriceThisMonthBreakdownCar(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonPriceThisMonthBreakdownCar(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonPriceThisMonthBreakdownCar(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getPriceThisMonthBreakdownCar");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getPriceThisMonthBreakdownBike(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonPriceThisMonthBreakdownBike(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonPriceThisMonthBreakdownBike(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getPriceThisMonthBreakdownBike");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getPriceTotalBreakdownCar(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonPriceTotalBreakdownCar(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonPriceTotalBreakdownCar(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getPriceTotalBreakdownCar");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getPriceTotalBreakdownBike(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonPriceTotalBreakdownBike(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonPriceTotalBreakdownBike(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getPriceTotalBreakdownBike");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getAvgPriceMonthBreakdownCar(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgPriceMonthBreakdownCar(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgPriceMonthBreakdownCar(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgPriceMonthBreakdownCar");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getAvgPriceMonthBreakdownBike(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgPriceMonthBreakdownBike(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgPriceMonthBreakdownBike(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgPriceMonthBreakdownBike");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getAvgPriceYearBreakdownCar(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgPriceYearBreakdownCar(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgPriceYearBreakdownCar(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgPriceYearBreakdownCar");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getAvgPriceYearBreakdownBike(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgPriceYearBreakdownBike(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgPriceYearBreakdownBike(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgPriceYearBreakdownBike");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
}
