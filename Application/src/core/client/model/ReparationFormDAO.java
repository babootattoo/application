package core.client.model;

import core.client.network.Communication;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author brice.boutamdja
 */
public class ReparationFormDAO {
    
    private static JsonObject getLastReparationFormRequestJSon(String classView, User userLogged){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "giveLastReparationForm");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject getLastReparationForm(String classView, User userLogged) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getLastReparationFormRequestJSon(classView, userLogged));
        result = com.getData();
        com.close();
        return result;
    }
    
    
    /**
     * This method build JsonObject for recup all ReparationForm with JsonObjectBuilder
     * @param classView String
     * @param userLogged User
     * @return JsonObject
     */
    private static JsonObject getReparationFormRequestJSonAll(String classView, User userLogged){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "giveAllReparationForm");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect the ReparationForm
     * @param classView String
     * @param userLogged User
     * @return
     * @throws IOException 
     */
    public static JsonObject getReparationFormAll(String classView, User userLogged) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getReparationFormRequestJSonAll(classView, userLogged));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getReparationFormRequestJSonAllAndAllWorkOn(String classView, User userLogged){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "giveAllReparationFormAndAllWorkOn");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject giveAllReparationFormAndAllWorkOn(String classView, User userLogged) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getReparationFormRequestJSonAllAndAllWorkOn(classView, userLogged));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getRequestJSonAllReparationFormWorkOn(String classView, User userLogged){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "giveAllReparationFormWorkOn");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    public static JsonObject giveAllReparationFormWorkOn(String classView, User userLogged) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getRequestJSonAllReparationFormWorkOn(classView, userLogged));
        result = com.getData();
        com.close();
        return result;
    }
    
    /**
     * This method build JsonObject to create a reparationForm with JsonObjectBuilder
     * @param tag
     * @param classView
     * @param userLogged
     * @param idParking
     * @return 
     */
    private static JsonObject createReparationFormRequestJson(String tag, String classView, User userLogged, int idParking){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "createReparationForm");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("TagVehicle", tag);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        builder.add("idParking", idParking);
        json = builder.build();
        return json;
    }
    
    /**
     * This method create reparationForm
     * @param tag
     * @param classView
     * @param userLogged
     * @param idParking
     * @return
     * @throws IOException 
     */
    public static JsonObject createReparationForm(String tag, String classView, User userLogged, int idParking) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(createReparationFormRequestJson(tag, classView, userLogged, idParking));
        result = com.getData();
        com.close();
        return result;
    }

    /**
     * This method build JsonObject to update details's reparationForm with JsonObjectBuilder
     * @param repForm
     * @param userLogged
     * @param classView
     * @return
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException 
     */
    private static JsonObject getJsonRequestUpdateReparatorDetails(ReparationForm repForm, User userLogged, String classView) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException{
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "updateReparatorDetails");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        builder.add("ReparationForm", SerializableJson.serialize(repForm));
        json = builder.build();
        return json;
    }
    
    /**
     * This method update reparationForm
     * @param repForm
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException 
     */
    public static JsonObject updateReparatorDetails(ReparationForm repForm, User userLogged, String classView) throws IOException, IllegalAccessException, NoSuchMethodException, InvocationTargetException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestUpdateReparatorDetails(repForm, userLogged, classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonRequestWorkOnUpdate(String classView, User userLogged, int idForm) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException{
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "workOnUpdate");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        builder.add("ReparationForm_Id", idForm);
        json = builder.build();
        return json;
    }
    
    public static JsonObject workOnUpdate(String classView, User userLogged, int idForm) throws IOException, IllegalAccessException, NoSuchMethodException, InvocationTargetException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestWorkOnUpdate(classView, userLogged, idForm));
        result = com.getData();
        com.close();
        return result;
    }
    
    public static JsonObject getAvgDuringBreakdown(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonAvgDuringBreakdown(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    private static JsonObject getJsonAvgDuringBreakdown(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAvgDuringBreakdown");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
}
