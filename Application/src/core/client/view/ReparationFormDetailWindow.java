package core.client.view;

import core.client.controller.ControllerReparationFormDetailWindow;
import core.client.model.Bike;
import core.client.model.Breakdown;
import core.client.model.Car;
import core.client.model.Comment;
import core.client.model.Part;
import core.client.model.ReparationForm;
import core.client.model.User;
import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

/**
 * Window for the details of a reparationform
 * @author Stephane.Schenkel
 */
public class ReparationFormDetailWindow extends JFrame {
    
    private JTable breakdownTable;
    private JButton deleteBkButton;
    private JButton addBkButton;
    
    private JTable partTable;
    private JButton deletePartButton;
    private JButton addPartButton;
    
    private JTextField cardIdField;
    private JComboBox<String> cardStateBox;
    private JTextField entryDateField;
    private JTextField outDateField;
    private JTextField parkingField;
    
    private JTable commentTable;
    private JTextField commentField;
    private JButton addCommentButton;
    
    private JTextArea diagnosisText;
    
    private JLabel caseLabel;
    private JTextField caseField;
    private JTextField lastRevisionField;
    private JTextField matriculeField;
    private JTextField modelField;
    private JTextField purchaseDateField;
    
    private JPanel vehiclePanel;
    private JPanel formPanel;
    
    private TitledBorder vehicleTitle;
    
    private JLabel modelLabel = new JLabel();
    private JLabel immatriculationLabel = new JLabel();
    
    private JButton markAsResolvedButton;
    
    private JButton saveButton;
    private JButton resetButton;
    
    private DefaultTableModel commentModel = new DefaultTableModel(new Object[][] {}, new Object[]{"Employee", "Date", "Comment"});
    private DefaultTableModel partModel = new DefaultTableModel(new Object[][] {}, new Object[]{"Id", "Part", "Installation Date", "NumberUsed"});
    private DefaultTableModel breakdownModel = new DefaultTableModel(new Object[][] {}, new Object[]{"Id", "Breakdown", "Average Time", "Priority", "Occured Date", "Resolved", "Resolved Date"});
    
    private ControllerReparationFormDetailWindow controller;
    
    private ReparationForm repForm = null;
    
    public ReparationFormDetailWindow(ReparationForm repForm, User userLogged){
        controller = new ControllerReparationFormDetailWindow(userLogged, this.getClass().getSimpleName());
        this.repForm = repForm;
        
        initialize();
        initializeComboBoxes();
        initializeData();
        initializeComments();
        initializeTables();
        
        addCommentButton.addMouseListener(new AddCommentMouseListener());
        resetButton.addMouseListener(new ResetMouseListener());
        addPartButton.addMouseListener(new AddPartMouseListener());
        deletePartButton.addMouseListener(new DeletePartMouseListener());
        addBkButton.addMouseListener(new AddBreakdownMouseListener());
        deleteBkButton.addMouseListener(new DeleteBreakdownMouseListener());
        saveButton.addMouseListener(new SaveButtonMouseListener());
        markAsResolvedButton.addMouseListener(new MarkResolvedButtonMouseListener());
        
        setSize(1050,800);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setTitle("Reparation Form Details");
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                int reponse = JOptionPane.showConfirmDialog(ReparationFormDetailWindow.this,
                        "Do you want to exit application ?",
                        "confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (reponse==JOptionPane.YES_OPTION)
                    ReparationFormDetailWindow.this.dispose();
            }
        });
        this.setLocationRelativeTo(null);
        setVisible(true);
    }
    
    /**
     * Initialize all the JComponent
     */
    private void initialize(){
        formPanel = new JPanel();
        JScrollPane jScrollPane1 = new JScrollPane();
        diagnosisText = new JTextArea();
        JScrollPane jScrollPane3 = new JScrollPane();
        commentTable = new JTable();
        JLabel jLabel6 = new JLabel();
        cardIdField = new JTextField();
        JLabel jLabel1 = new JLabel();
        cardStateBox = new JComboBox<>();
        JLabel jLabel2 = new JLabel();
        entryDateField = new JTextField();
        JLabel jLabel3 = new JLabel();
        outDateField = new JTextField();
        JLabel jLabel4 = new JLabel();
        commentField = new JTextField();
        addCommentButton = new JButton();
        JLabel jLabel8 = new JLabel();
        parkingField = new JTextField();
        vehiclePanel = new JPanel();
        caseLabel = new JLabel();
        JLabel jLabel10 = new JLabel();
        JLabel jLabel11 = new JLabel();
        caseField = new JTextField();
        purchaseDateField = new JTextField();
        lastRevisionField = new JTextField();
        modelField = new JTextField();
        matriculeField = new JTextField();
        JPanel jPanel1 = new JPanel();
        JScrollPane jScrollPane5 = new JScrollPane();
        partTable = new JTable();
        addPartButton = new JButton();
        deletePartButton = new JButton();
        JPanel jPanel2 = new JPanel();
        JScrollPane jScrollPane6 = new JScrollPane();
        breakdownTable = new JTable();
        deleteBkButton = new JButton();
        addBkButton = new JButton();
        saveButton = new JButton("Save");
        resetButton = new JButton("Reset");
        markAsResolvedButton = new JButton("Mark as resolved");
        
        formPanel.setBorder(BorderFactory.createTitledBorder("Form details"));;

        diagnosisText.setColumns(20);
        diagnosisText.setRows(5);
        jScrollPane1.setViewportView(diagnosisText);

        commentTable.setModel(commentModel);
        jScrollPane3.setViewportView(commentTable);

        jLabel6.setText("Diagnosis :");

        jLabel1.setText("Form ID :");

        jLabel2.setText("Form State :");

        jLabel3.setText("Entry date :");

        jLabel4.setText("Out date :");

        commentField.setToolTipText("Comment");

        addCommentButton.setText("Add Comment");

        jLabel8.setText("Parking place :");

        javax.swing.GroupLayout formPanelLayout = new javax.swing.GroupLayout(formPanel);
        formPanel.setLayout(formPanelLayout);
        formPanelLayout.setHorizontalGroup(
            formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(formPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(formPanelLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cardStateBox, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(formPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cardIdField, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(formPanelLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(entryDateField, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(formPanelLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(outDateField, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(formPanelLayout.createSequentialGroup()
                        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(parkingField))))
                .addGap(18, 18, 18)
                .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(formPanelLayout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(formPanelLayout.createSequentialGroup()
                        .addComponent(commentField)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(addCommentButton)))
                .addContainerGap())
        );
        formPanelLayout.setVerticalGroup(
            formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(formPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(formPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(commentField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(addCommentButton))
                        .addContainerGap())
                    .addGroup(formPanelLayout.createSequentialGroup()
                        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, formPanelLayout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(9, 9, 9))
                            .addGroup(formPanelLayout.createSequentialGroup()
                                .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cardIdField, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1))
                                .addGap(20, 20, 20)))
                        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cardStateBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(18, 18, 18)
                        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(entryDateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(outDateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(formPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(parkingField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addGap(45, 45, 45))))
        );
        
        vehicleTitle = javax.swing.BorderFactory.createTitledBorder("Vehicle");
        vehiclePanel.setBorder(vehicleTitle);

        immatriculationLabel.setText("Immatriculation :");

        modelLabel.setText("Model :");

        caseLabel.setText("NFC Case :");
        caseLabel.setToolTipText("");

        jLabel10.setText("Purchase Date :");

        jLabel11.setText("Last Revision :");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Parts"));

        partTable.setModel(partModel);
        jScrollPane5.setViewportView(partTable);

        addPartButton.setText("Add Part");

        deletePartButton.setText("Delete Part");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(deletePartButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addPartButton)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addPartButton)
                    .addComponent(deletePartButton)))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Breakdowns"));

        breakdownTable.setModel(breakdownModel);
        jScrollPane6.setViewportView(breakdownTable);

        deleteBkButton.setText("Delete");

        addBkButton.setText("Add");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(deleteBkButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(markAsResolvedButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addBkButton)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(deleteBkButton)
                    .addComponent(markAsResolvedButton)
                    .addComponent(addBkButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout vehiclePanelLayout = new javax.swing.GroupLayout(vehiclePanel);
        vehiclePanel.setLayout(vehiclePanelLayout);
        vehiclePanelLayout.setHorizontalGroup(
            vehiclePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(vehiclePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(vehiclePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel11)
                    .addComponent(jLabel10)
                    .addComponent(caseLabel)
                    .addComponent(modelLabel)
                    .addComponent(immatriculationLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(vehiclePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(caseField)
                    .addComponent(purchaseDateField)
                    .addComponent(lastRevisionField)
                    .addComponent(modelField)
                    .addComponent(matriculeField, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(vehiclePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        vehiclePanelLayout.setVerticalGroup(
            vehiclePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(vehiclePanelLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(23, 23, 23))
            .addGroup(vehiclePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(vehiclePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(caseLabel)
                    .addComponent(caseField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(vehiclePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(purchaseDateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(vehiclePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(lastRevisionField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(vehiclePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(modelLabel)
                    .addComponent(modelField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(vehiclePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(matriculeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(immatriculationLabel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(resetButton, BorderLayout.LINE_START);
        panel.add(saveButton, BorderLayout.LINE_END);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(vehiclePanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(formPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(formPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(vehiclePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap()
                .addComponent(panel))
        );
    }
    
    /**
     * The comboBox will be populated with the list of urgency degree and cardstate found in the database
     */
    private void initializeComboBoxes(){
        try {
            List<String> listCardState = controller.getListCardState();
            for(String line : listCardState){
                cardStateBox.addItem(line);
            }
        } catch (ParseException ex) {
            Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Populate all the fields and set the window with the values of the reparationform
     */
    private void initializeData(){
        if(repForm != null){
            cardIdField.setText(String.valueOf(repForm.getId()));
            cardIdField.setEditable(false);
            cardStateBox.setSelectedItem(repForm.getDescriptionCardState());
            entryDateField.setText(repForm.getEntryDate().toString());
            entryDateField.setEditable(false);
            outDateField.setText(repForm.getOutDate().toString());
            outDateField.setEnabled(false);
            //urDegreeBox.setSelectedItem(repForm.getDescriptionUrgencyDegree()); delete UrgencyDegree
            parkingField.setText(repForm.getParkingPlace().getPlaceNumber());
            parkingField.setEditable(false);
            diagnosisText.setText(repForm.getDiagnosis());
            
            Car car = repForm.getCar();
            Bike bike = repForm.getBike();
            if(car != null){
                vehicleTitle.setTitle("Car details");
                caseLabel.setText("NFC Case : ");
                
                caseField.setText(car.getNfcCase().getTagId());
                purchaseDateField.setText(car.getPurchaseDate().toString());
                lastRevisionField.setText(car.getLastRevision().toString());
                modelField.setText(car.getModel().getModelFullName());
                matriculeField.setText(car.getMatriculation());
            }else if(bike != null){
                vehicleTitle.setTitle("Bike details");
                caseLabel.setText("RFID Chip : ");
                modelLabel.setVisible(false);
                immatriculationLabel.setVisible(false);
                modelField.setVisible(false);
                matriculeField.setVisible(false);
                
                caseField.setText(bike.getRfidChip().getTagId());
                purchaseDateField.setText(bike.getPurchaseDate().toString());
                lastRevisionField.setText(bike.getLastRevision().toString());
            }else{
                this.dispose();
            }
            
            caseField.setEditable(false);
            purchaseDateField.setEditable(false);
            lastRevisionField.setEditable(false);
            modelField.setEditable(false);
            matriculeField.setEditable(false);
        }else{
            this.dispose();
        }
    }
    
    /**
     * Get all the comments of the specified reparationform and populate the JTable of comments
     */
    private void initializeComments(){
        try {
            List<Comment> comments = controller.getComments(repForm.getId());
            if(!comments.isEmpty()){
                commentModel.setNumRows(0);
                for(Comment comment : comments){
                    commentModel.addRow(new Object[]{ comment.getFullName(), comment.getCreationDate().toString(), comment.getCommentary() });
                }
            }
        } catch (ParseException ex) {
            Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Populate the JTable of Part and Breakdown for the specified car or bike of the reparationform
     */
    private void initializeTables(){
        Car car = repForm.getCar();
        Bike bike = repForm.getBike();
        if(car != null){
            if(car.getParts() != null){
                partModel.setNumRows(0);
                for(Part part : car.getParts()){
                    partModel.addRow(new Object[]{ part.getId(), part.getDescription(), part.getInstallationDate(), part.getNumberUse()});
                }
            }
            if(car.getBreakdowns() != null){
                breakdownModel.setNumRows(0);
                for(Breakdown breakdown : car.getBreakdowns()){
                    breakdownModel.addRow(new Object[]{ breakdown.getId(), breakdown.getDescription(), breakdown.getAverageTimeMinutes(), breakdown.getPriorityLevel(), breakdown.getOccuredDate(), breakdown.getResolved(), breakdown.getResolved_date() });
                }
            }
        }else if(bike != null){
            if(bike.getParts() != null){
                partModel.setNumRows(0);
                for(Part part : bike.getParts()){
                    partModel.addRow(new Object[]{ part.getId(), part.getDescription(), part.getInstallationDate(), part.getNumberUse()});
                }
            }
            if(bike.getBreakdowns() != null){
                breakdownModel.setNumRows(0);
                for(Breakdown breakdown : bike.getBreakdowns()){
                    breakdownModel.addRow(new Object[]{ breakdown.getId(), breakdown.getDescription(), breakdown.getAverageTimeMinutes(), breakdown.getPriorityLevel(), breakdown.getOccuredDate(), breakdown.getResolved(), breakdown.getResolved_date() });
                }
            }
        }else{
            
        }
    }
    
    /**
     * Listener for addCommentButton
     */
    private class AddCommentMouseListener extends MouseAdapter{

        @Override
        public void mouseClicked(MouseEvent e){
            String message = commentField.getText();
            if(!message.equals("") && message != null){
                int reponse = JOptionPane.showConfirmDialog(ReparationFormDetailWindow.this,
                        "Do you really want to add this comment ?",
                        "confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (reponse==JOptionPane.YES_OPTION){
                    try {
                        String result = controller.addComment(repForm.getId(), message);
                        switch(result){
                            case "success":
                                ReparationFormDetailWindow.this.initializeComments();
                                break;
                            case "error":
                                JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, controller.getErrorMessage());
                                break;
                        }
                    } catch (ParseException ex) {
                        Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NoSuchAlgorithmException ex) {
                        Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }else{
                JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, "Please fill the comment field if you want to add one.");
            }
        }
        
    }
    
    /**
     * Listener for ResetButton
     */
    private class ResetMouseListener extends MouseAdapter{
        
        @Override
        public void mouseClicked(MouseEvent e){
            int reponse = JOptionPane.showConfirmDialog(ReparationFormDetailWindow.this,
                        "Do you really to reset all the fields ?",
                        "confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (reponse==JOptionPane.YES_OPTION){
                    ReparationFormDetailWindow.this.initializeData();
                }
        }
        
    }
    
    /**
     * Listener for AddPart
     */
    private class AddPartMouseListener extends MouseAdapter{
        
        @Override
        public void mouseClicked(MouseEvent args){
            Car car = repForm.getCar();
            Bike bike = repForm.getBike();
            try{
                if(car != null){
                    List<Part> listUsablePart = controller.getListUsablePartCar(car.getModel().getId());
                    List<String> listString = new ArrayList<String>();
                    for(Part part : listUsablePart){
                        listString.add(part.getId() + " - " + part.getDescription() + " - " + part.getStock() + " in stock");
                    }
                    if(!listUsablePart.isEmpty()){
                        Object selected = JOptionPane.showInputDialog(ReparationFormDetailWindow.this, "Choose a part", "Adding part window", JOptionPane.QUESTION_MESSAGE, null, listString.toArray(), listString.get(1));
                        if(selected != null && !selected.equals("")){
                            String idPart = selected.toString().split(" - ")[0];
                            String stock = selected.toString().split(" - ")[2].split(" in stock")[0];
                            if(Integer.parseInt(stock) != 0){
                                List<Part> result = controller.addPart(car.getId(), Integer.parseInt(idPart), "car");
                                if(!result.isEmpty()){
                                    repForm.getCar().setParts(result);
                                    ReparationFormDetailWindow.this.initializeTables();
                                }else{
                                    JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, controller.getErrorMessage());
                                }
                            }else
                            {
                                JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, "No stock for this part. Please contact the administrators."); 
                            }
                        }
                    }else{
                        JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, controller.getErrorMessage());
                    }
                }else if(bike != null){
                    List<Part> listUsablePart = controller.getListUsablePartBike();
                    List<String> listString = new ArrayList<String>();
                    for(Part part : listUsablePart){
                        listString.add(part.getId() + " - " + part.getDescription() + " - " + part.getStock() + " in stock");
                    }
                    if(!listUsablePart.isEmpty()){
                        Object selected = JOptionPane.showInputDialog(ReparationFormDetailWindow.this, "Choose a part", "Adding part window", JOptionPane.QUESTION_MESSAGE, null, listString.toArray(), listString.get(1));
                        if(selected != null && !selected.equals("")){
                            String idPart = selected.toString().split(" - ")[0];
                            String stock = selected.toString().split(" - ")[2].split(" in stock")[0];
                            if(Integer.parseInt(stock) != 0){
                                List<Part> result = controller.addPart(bike.getId(), Integer.parseInt(idPart), "bike");
                                if(!result.isEmpty()){
                                    repForm.getBike().setParts(result);
                                    ReparationFormDetailWindow.this.initializeTables();
                                }else{
                                    JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, controller.getErrorMessage());
                                }
                            }else
                            {
                                JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, "No stock for this part. Please contact the administrators."); 
                            }
                        }
                    }else{
                        JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, controller.getErrorMessage());
                    }
                }else{

                }
            } catch (ParseException ex) {
                Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    /**
     * Listener for DeletePart
     */
    private class DeletePartMouseListener extends MouseAdapter{
        
        @Override
        public void mouseClicked(MouseEvent args){
            int rowIndex =  partTable.getSelectedRow();
            if(rowIndex != -1){
                int idPart = Integer.parseInt(partModel.getValueAt(rowIndex, 0).toString());
                String description = partModel.getValueAt(rowIndex, 1).toString();
                int reponse = JOptionPane.showConfirmDialog(ReparationFormDetailWindow.this,
                        "Do you really want to delete this part ? (" + description + ")",
                        "confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (reponse==JOptionPane.YES_OPTION){
                    Car car = repForm.getCar();
                    Bike bike = repForm.getBike();
                    try{
                        if(car != null){
                            List<Part> result = controller.deletePart(car.getId(), idPart, "car");
                            if(!result.isEmpty()){
                                repForm.getCar().setParts(result);
                                ReparationFormDetailWindow.this.initializeTables();
                            }else{
                                repForm.getCar().getParts().clear();
                                partModel.setRowCount(0);
                            }
                        }else if(bike != null){
                            List<Part> result = controller.deletePart(bike.getId(), idPart, "bike");
                            if(!result.isEmpty()){
                                repForm.getBike().setParts(result);
                                ReparationFormDetailWindow.this.initializeTables();
                            }else{
                                repForm.getBike().getParts().clear();
                                partModel.setRowCount(0);
                            }
                        }
                    } catch (ParseException ex) {
                        Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NoSuchAlgorithmException ex) {
                        Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
    
    /**
     * Listener of AddBreakdown
     */
    private class AddBreakdownMouseListener extends MouseAdapter{
        
        @Override
        public void mouseClicked(MouseEvent args){
            Car car = repForm.getCar();
            Bike bike = repForm.getBike();
            try{
                if(car != null){
                        List<Breakdown> listHaveBreakdown = controller.getListHaveBreakdownCar(car.getModel().getId(), car.getId());
                        List<String> listString = new ArrayList<String>();
                        for(Breakdown breakdown : listHaveBreakdown){
                            listString.add(breakdown.getId() + " - " + breakdown.getDescription() + ", " + breakdown.getAverageTimeMinutes() + " min, " + " priority " + breakdown.getPriorityLevel());
                        }
                        if(!listHaveBreakdown.isEmpty()){
                            Object selected = JOptionPane.showInputDialog(ReparationFormDetailWindow.this, "Choose a breakdown", "Adding breakdown window", JOptionPane.QUESTION_MESSAGE, null, listString.toArray(), listString.get(1));
                            if(selected != null && !selected.equals("")){
                                String idBreakdown = selected.toString().split(" - ")[0];
                                List<Breakdown> result = controller.addBreakdown(car.getId(), Integer.parseInt(idBreakdown), "car");
                                if(!result.isEmpty()){
                                    repForm.getCar().setBreakdowns(result);
                                    ReparationFormDetailWindow.this.initializeTables();
                                }else{
                                    JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, controller.getErrorMessage());
                                }
                            }
                        }else{
                            JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, controller.getErrorMessage());
                        }
                }else if(bike != null){
                        List<Breakdown> listHaveBreakdown = controller.getListHaveBreakdownBike(bike.getId());
                        List<String> listString = new ArrayList<String>();
                        for(Breakdown breakdown : listHaveBreakdown){
                            listString.add(breakdown.getId() + " - " + breakdown.getDescription() + ", " + breakdown.getAverageTimeMinutes() + " min, " + " priority " + breakdown.getPriorityLevel());
                        }
                        if(!listHaveBreakdown.isEmpty()){
                            Object selected = JOptionPane.showInputDialog(ReparationFormDetailWindow.this, "Choose a breakdown", "Adding breakdown window", JOptionPane.QUESTION_MESSAGE, null, listString.toArray(), listString.get(1));
                            if(selected != null && !selected.equals("")){
                                String idBreakdown = selected.toString().split(" - ")[0];
                                List<Breakdown> result = controller.addBreakdown(bike.getId(), Integer.parseInt(idBreakdown), "bike");
                                if(!result.isEmpty()){
                                    repForm.getBike().setBreakdowns(result);
                                    ReparationFormDetailWindow.this.initializeTables();
                                }else{
                                    JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, controller.getErrorMessage());
                                }
                            }
                        }else{
                            JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, controller.getErrorMessage());
                        }
                }else{

                }
            } catch (ParseException ex) {
                Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    /**
     * Listener deleteBreakdown
     */
    private class DeleteBreakdownMouseListener extends MouseAdapter{
        
        @Override
        public void mouseClicked(MouseEvent arg){
            int rowIndex =  breakdownTable.getSelectedRow();
            if(rowIndex != -1){
                boolean resolved = Boolean.parseBoolean(breakdownModel.getValueAt(rowIndex, 5).toString());
                if(!resolved){
                    int idBreakdown = Integer.parseInt(breakdownModel.getValueAt(rowIndex, 0).toString());
                    String description = breakdownModel.getValueAt(rowIndex, 1).toString();
                    int reponse = JOptionPane.showConfirmDialog(ReparationFormDetailWindow.this,
                            "Do you really want to delete this breakdown ? (" + description + ")",
                            "confirmation",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE);
                    if (reponse==JOptionPane.YES_OPTION){
                        Car car = repForm.getCar();
                        Bike bike = repForm.getBike();
                        try{
                            if(car != null){
                                List<Breakdown> result = controller.deleteBreakdown(car.getId(), idBreakdown, "car");
                                if(!result.isEmpty()){
                                    repForm.getCar().setBreakdowns(result);
                                    ReparationFormDetailWindow.this.initializeTables();
                                }else{
                                    repForm.getCar().getBreakdowns().clear();
                                    breakdownModel.setRowCount(0);
                                }
                            }else if(bike != null){
                                List<Breakdown> result = controller.deleteBreakdown(bike.getId(), idBreakdown, "bike");
                                if(!result.isEmpty()){
                                    repForm.getBike().setBreakdowns(result);
                                    ReparationFormDetailWindow.this.initializeTables();
                                }else{
                                    repForm.getBike().getBreakdowns().clear();
                                    breakdownModel.setRowCount(0);
                                }
                            }
                        } catch (ParseException ex) {
                            Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (NoSuchAlgorithmException ex) {
                            Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                else{
                    JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, "You can't delete a breakdown marked as resolved.");            
                }
            }
        }
        
    }
    
    /**
     * Listener of MarkAsResolvedButton
     */
    private class MarkResolvedButtonMouseListener extends MouseAdapter{
        
        @Override
        public void mouseClicked(MouseEvent arg){
            int rowIndex =  breakdownTable.getSelectedRow();
            if(rowIndex != -1){
                boolean resolved = Boolean.parseBoolean(breakdownModel.getValueAt(rowIndex, 5).toString());
                if(!resolved){
                    int idBreakdown = Integer.parseInt(breakdownModel.getValueAt(rowIndex, 0).toString());
                    String description = breakdownModel.getValueAt(rowIndex, 1).toString();
                    int reponse = JOptionPane.showConfirmDialog(ReparationFormDetailWindow.this,
                            "Do you really want to mark this breakdown (" + description + ") as resolved ?",
                            "confirmation",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE);
                    if (reponse==JOptionPane.YES_OPTION){
                        Car car = repForm.getCar();
                        Bike bike = repForm.getBike();
                        try{
                            if(car != null){
                                List<Breakdown> result = controller.markAsResolvedBreakdown(car.getId(), idBreakdown, "car");
                                if(!result.isEmpty()){
                                    repForm.getCar().setBreakdowns(result);
                                    ReparationFormDetailWindow.this.initializeTables();
                                }
                            }else if(bike != null){
                                List<Breakdown> result = controller.markAsResolvedBreakdown(bike.getId(), idBreakdown, "bike");
                                if(!result.isEmpty()){
                                    repForm.getBike().setBreakdowns(result);
                                    ReparationFormDetailWindow.this.initializeTables();
                                }
                            }
                        } catch (ParseException ex) {
                            Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (NoSuchAlgorithmException ex) {
                            Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, "This breakdown is already marked as resolved.");            
                }
            }
        }
        
    }
    
    /**
     * Listener of SaveButton
     */
    private class SaveButtonMouseListener extends MouseAdapter{
        
        @Override
        public void mouseClicked(MouseEvent arg){
            String cardState = cardStateBox.getSelectedItem().toString();
            String diagnosis = diagnosisText.getText();
            if(!cardState.equals("")){
                int reponse = JOptionPane.showConfirmDialog(ReparationFormDetailWindow.this,
                        "Do you really want to save the fields you changed ?",
                        "confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (reponse==JOptionPane.YES_OPTION){
                    try {
                        repForm.setDiagnosis(diagnosis);
                        repForm.setDescriptionCardState(cardState);
                        
                        String result = controller.updateReparationFormDetails(repForm);
                        switch(result){
                            case "success":
                                JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, "Update successful.");
                                break;
                            case "error":
                                JOptionPane.showMessageDialog(ReparationFormDetailWindow.this, controller.getErrorMessage());
                                break;
                            default:
                                ReparationFormDetailWindow.this.dispose();
                                break;
                        }
                    } catch (NoSuchAlgorithmException ex) {
                        Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ParseException ex) {
                        Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NoSuchMethodException ex) {
                        Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InvocationTargetException ex) {
                        Logger.getLogger(ReparationFormDetailWindow.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        
    }
}
