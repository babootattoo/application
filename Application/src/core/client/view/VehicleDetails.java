package core.client.view;

import core.client.controller.ControllerVehicleDetails;
import core.client.controller.ControllerVehiclesWindow;
import core.client.model.Bike;
import core.client.model.Car;
import core.client.model.ReparationForm;
import core.client.model.User;
import java.awt.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.*;

public class VehicleDetails extends JFrame {

    private ControllerVehicleDetails controller = null;
    private JScrollPane jScrollPane2 = new JScrollPane();
    private JButton jbutton_back = new JButton("Back");
    private JLabel jlbl_title = new JLabel("Vehicule details");
    private JLabel jlbl_immatriculation = new JLabel(" Immatriculation :");
    private JLabel jlbl_purshase = new JLabel("Purshase date : ");
    private JLabel jlbl_revision = new JLabel("Last revision : ");
    private JLabel jlbl_builder = new JLabel("Builder : ");
    private JLabel jlbl_eletric = new JLabel("Is eletric : ");
    private JLabel jlbl_model = new JLabel("Car model : ");
    private JLabel jlbl_bikeRFID = new JLabel("Bike RFID : ");
    private JLabel jlbl_titleCardList = new JLabel("Reparation card list :");
    private JLabel jlbl_txtPurshase = new JLabel();
    private JLabel jlbl_txtImmatriculation = new JLabel();
    private JLabel jlbl_txtRevision = new JLabel();
    private JLabel jlbl_txtBikeRFID = new JLabel();
    private JLabel jlbl_txtModel = new JLabel();
    private JLabel jlbl_txtEletric = new JLabel();
    private JLabel jlbl_txtBuilder = new JLabel();
    private int id;
    private String typeVehicle;
    private Car car ;
    private User userLogged;
    
    public VehicleDetails(int id, String typeVehicle, User userLogged) {
        this.id = id;
        this.userLogged = userLogged;
        this.typeVehicle = typeVehicle;
        controller = new ControllerVehicleDetails(this.getClass().getSimpleName(), userLogged);
        initComponents();
    }
                        
    private void initComponents() {
        Object vehicle = null;
        setTitle("Vehicle details");
        try{
            vehicle = controller.getDetails(this.id, this.typeVehicle);
        }catch(IOException | NoSuchAlgorithmException | ParseException e){
            System.out.println(e);
        }
        Object[][] tableData = null;
        String [] cl =  {"Card ID", "Diagnosis", "Occured date", "Resolved"};
        DefaultTableModel tableModel = new DefaultTableModel(tableData,cl);
        if (vehicle instanceof Car){
            Car car = ((Car) vehicle);
            jlbl_bikeRFID.setText("");
            jlbl_txtPurshase.setText(car.getPurchaseDate().toString());
            jlbl_txtImmatriculation.setText(car.getMatriculation());
            jlbl_txtRevision.setText(car.getLastRevision().toString());
            jlbl_txtModel.setText(car.getModel().getModelFullName()); 
            jlbl_txtBuilder.setText(car.getModel().getBuilder());
            jlbl_txtRevision.setText(car.getLastRevision().toString());
            if (car.getModel().getIsElectric() == true){
                jlbl_txtEletric.setText("True");
            } else {
                jlbl_txtEletric.setText("False");
            }
            for (int i = 0; i <  car.getBreakdowns().size(); i++){
                Object[] rowData = new Object[]{
                    car.getBreakdowns().get(i).getId(),
                    car.getBreakdowns().get(i).getDescription(),
                    car.getBreakdowns().get(i).getOccuredDate().getDate(),
                    car.getBreakdowns().get(i).getResolved(),
                };
                tableModel.addRow(rowData);
            } 
        }else if (vehicle instanceof Bike){
            Bike bike = ((Bike) vehicle);
            jlbl_txtBikeRFID.setText(bike.getRfidChip().getTagId().toString());
            jlbl_txtPurshase.setText(bike.getPurchaseDate().toString());
            jlbl_txtRevision.setText(bike.getLastRevision().toString());
            for (int i = 0; i <  bike.getBreakdowns().size(); i++){
                Object[] rowData = new Object[]{
                    bike.getBreakdowns().get(i).getId(),
                    bike.getBreakdowns().get(i).getDescription(),
                    bike.getBreakdowns().get(i).getOccuredDate().getDate(),
                    bike.getBreakdowns().get(i).getResolved(),
                };
                tableModel.addRow(rowData);
            }
        }
        JTable jtable_cardList = new JTable(tableModel);
        ListSelectionModel selectionModel = jtable_cardList.getSelectionModel();
        jtable_cardList.setCellSelectionEnabled(false);
        selectionModel.addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent e){
                String typeVehicle = jtable_cardList.getColumnName(0);
                int id = Integer.parseInt(jtable_cardList.getValueAt(jtable_cardList.getSelectedRow(),0).toString());
                //ReparationForm repForm = new ReparationForm();
                //controller.openReparationFormDetailWindow(lRepForm, userLogged, idRepForm);
                //frame.setVisible(true);
            }
        });
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        jlbl_title.setFont(new Font("Dialog", 1, 18)); 
        jlbl_title.setHorizontalAlignment(SwingConstants.CENTER);

        jScrollPane2.setViewportView(jtable_cardList);

        jlbl_txtPurshase.setFont(new Font("Dialog", 0, 12)); 
        jlbl_txtImmatriculation.setFont(new Font("Dialog", 0, 12)); 
        jlbl_txtRevision.setFont(new Font("Dialog", 0, 12)); 
        jlbl_txtBikeRFID.setFont(new Font("Dialog", 0, 12)); 
        jlbl_txtModel.setFont(new Font("Dialog", 0, 12)); 
        jlbl_txtEletric.setFont(new Font("Dialog", 0, 12)); 
        jlbl_txtBuilder.setFont(new Font("Dialog", 0, 12)); 

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jlbl_title, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbutton_back, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
                .addGap(195, 195, 195))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addComponent(jlbl_bikeRFID)
                            .addComponent(jlbl_revision)
                            .addComponent(jlbl_immatriculation)
                            .addComponent(jlbl_purshase))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jlbl_txtPurshase)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jlbl_builder))
                                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jlbl_txtImmatriculation)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jlbl_model))
                                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jlbl_txtRevision)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jlbl_eletric)))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(jlbl_txtModel)
                                    .addComponent(jlbl_txtEletric)
                                    .addComponent(jlbl_txtBuilder))
                                .addGap(64, 64, 64))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jlbl_txtBikeRFID)
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jlbl_titleCardList)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jlbl_title)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jlbl_immatriculation)
                    .addComponent(jlbl_model)
                    .addComponent(jlbl_txtImmatriculation)
                    .addComponent(jlbl_txtModel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jlbl_purshase)
                    .addComponent(jlbl_builder)
                    .addComponent(jlbl_txtPurshase)
                    .addComponent(jlbl_txtBuilder))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jlbl_revision)
                    .addComponent(jlbl_eletric)
                    .addComponent(jlbl_txtRevision)
                    .addComponent(jlbl_txtEletric))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jlbl_bikeRFID)
                    .addComponent(jlbl_txtBikeRFID))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addComponent(jlbl_titleCardList)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jbutton_back)
                .addGap(6, 6, 6))
        );
        
        jbutton_back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    backButtonActionPerformed(evt);
                } catch (IOException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchMethodException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        pack();
    }               
    
    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) throws IOException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException, IllegalArgumentException, IllegalAccessException, NoSuchAlgorithmException{
        this.dispose();
    }            
}
