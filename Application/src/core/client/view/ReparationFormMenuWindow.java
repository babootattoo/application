package core.client.view;

import core.client.controller.ControllerReparationFormMenuWindow;
import core.client.model.Bike;
import core.client.model.Car;
import core.client.model.DateFormatted;
import core.client.model.Parking;
import core.client.model.ReparationForm;
import core.client.model.User;
import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 * View for reparation form menu window
 * @author brice.boutamdja
 */
public class ReparationFormMenuWindow extends JFrame{
    
    JPanel panel = new JPanel();
    
    final DefaultListModel vehicle = new DefaultListModel();
        
    private JList list = new JList(vehicle);
    
    private JTable table;
    
    private JTable tableWorkOn;
    
    private ControllerReparationFormMenuWindow controller = null;
    
    private User userLogged;
    
    private Modele modele;
    
    private Modele modeleWorkOn;
    
    private List<ReparationForm> lRepForm;
    
    private List<ReparationForm> lRepFormWorkOn;
   
    /**
     * 
     * @param lRepForm
     * @param userLogged
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException 
     */
    public ReparationFormMenuWindow(List<ReparationForm> lRepForm, List<ReparationForm> lRepFormWorkOn, User userLogged) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, IllegalArgumentException{

        this.userLogged = userLogged;
        this.lRepForm = lRepForm;
        this.lRepFormWorkOn = lRepFormWorkOn;
        controller = new ControllerReparationFormMenuWindow(this.getClass().getSimpleName(), userLogged);
        /**
         * Parameters of the window
         */
        Display();
    }
    
    /**
     * Initialize JTable
     * @param lRepForm list of reparation form
     * @param userLogged user
     */
    private void initialize(){
        JTabbedPane tabbedPane = new JTabbedPane();
        
        JPanel firstPanel = new JPanel();
        
        JScrollPane jScrollPane1 = new javax.swing.JScrollPane();
        
        modele = new Modele(lRepForm);
        table = new JTable(modele);
        table.addMouseListener(new PopClickListener());
        
        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(table.getModel());
        table.setRowSorter(sorter);
        
        List<RowSorter.SortKey> sortKeys = new ArrayList<>();
        sortKeys.add(new RowSorter.SortKey(1, SortOrder.DESCENDING));
        sorter.setSortKeys(sortKeys);
        sorter.sort();
        
        jScrollPane1.setViewportView(table);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(firstPanel);
        firstPanel.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE)
        );
        
        tabbedPane.addTab("Waiting list of reparation", firstPanel);
        
        JPanel secondPanel = new JPanel();
        
        JScrollPane jScrollPane2 = new javax.swing.JScrollPane();
        
        modeleWorkOn = new Modele(lRepFormWorkOn);
        tableWorkOn = new JTable(modeleWorkOn);
        tableWorkOn.addMouseListener(new PopClickListener());
        
        TableRowSorter<TableModel> sorterWorkOn = new TableRowSorter<TableModel>(tableWorkOn.getModel());
        tableWorkOn.setRowSorter(sorterWorkOn);
        
        List<RowSorter.SortKey> sortKeysWorkOn = new ArrayList<>();
        sortKeysWorkOn.add(new RowSorter.SortKey(1, SortOrder.DESCENDING));
        sorterWorkOn.setSortKeys(sortKeysWorkOn);
        sorterWorkOn.sort();
        
        jScrollPane2.setViewportView(tableWorkOn);

        javax.swing.GroupLayout layoutWorkOn = new javax.swing.GroupLayout(secondPanel);
        secondPanel.setLayout(layoutWorkOn);
        layoutWorkOn.setHorizontalGroup(
            layoutWorkOn.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)
        );
        layoutWorkOn.setVerticalGroup(
            layoutWorkOn.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE)
        );
        
        tabbedPane.addTab("My reparations", secondPanel);
        
        getContentPane().add(tabbedPane);
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    }
    
    /**
     * Modele customize for adapt table with reparation form
     */
    public class Modele extends AbstractTableModel{
        
        private List<ReparationForm> list;
        private String[] title = {"id", "priorityLevel", "descriptionCardState", "parkingPlace", "car", "bike", "entryDate","outDate","diagnosis"};

        
        public String getColumnName(int columnIndex){
            return title[columnIndex];
        }
        
        public Modele(List<ReparationForm> lRepForm){
            this.list = lRepForm;
        }
        
        public void setNewList(List<ReparationForm> lRepForm){
            this.list = lRepForm;
        }

        @Override
        public int getRowCount() {
            return list.size();
        }

        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex){
                case 0 :
                    return Integer.class;
                case 1 :
                    return Integer.class;
                case 2 :
                    return String.class;
                case 3 :
                    return Parking.class;
                case 4 :
                    return Car.class;
                case 5 :
                    return Bike.class;
                case 6 :
                    return DateFormatted.class;
                case 7 :
                    return DateFormatted.class;
                case 8 :
                    return String.class;
                default :
                    return Object.class;
            }
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            switch (columnIndex){
                case 0 :
                    return list.get(rowIndex).getId();
                case 1:
                    return list.get(rowIndex).getPriorityLevel();
                case 2 :
                    return list.get(rowIndex).getDescriptionCardState();
                case 3 :
                    return list.get(rowIndex).getParkingPlace().getPlaceNumber();
                case 4 :
                    if (list.get(rowIndex).getCar() == null)
                        return "";
                    else
                        return list.get(rowIndex).getCar().getMatriculation();
                case 5 :
                    if (list.get(rowIndex).getBike() == null)
                        return "";
                    else
                        return list.get(rowIndex).getBike().getId();
                case 6 :
                    return list.get(rowIndex).getEntryDate();
                case 7 :
                    return list.get(rowIndex).getOutDate();
                case 8 :
                    return list.get(rowIndex).getDiagnosis();
                default :
                    throw new IllegalArgumentException();
            }
        }

        @Override
        public int getColumnCount() {
            return title.length;
        }
    }
    
    /**
     * class for pop up on click right
     */
    class PopUp extends JPopupMenu {
        public PopUp(int row, JTable currentTable){
            if(table == currentTable){
                JMenuItem workMenu = new JMenuItem("Work on this");
                add(workMenu);
                workMenu.addMouseListener(new workOnThisReparationForm(row));
            }else if(tableWorkOn == currentTable){
                JMenuItem openMenu = new JMenuItem("Open this reparation form");
                add(openMenu);
                openMenu.addMouseListener(new openReparationForm(row));
            }
            JMenuItem refreshMenu = new JMenuItem("Refresh list");
            add(refreshMenu);
            refreshMenu.addMouseListener(new refreshListener(currentTable));
        }
    }
    
    /**
     * class for refresh table
     */
    class refreshListener extends MouseAdapter {

        JTable currentTable;
        
        public void mousePressed(MouseEvent e){
            try {
                if(currentTable == tableWorkOn){
                    refreshTableWorkOn();
                }else if(currentTable == table){
                    refreshTable();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public refreshListener(JTable currentTable){
            this.currentTable = currentTable;
        }
    }
    
    public void refreshTable() throws IOException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException{
        lRepForm = controller.getListReparationForm();

        table.removeAll();
        modele.setNewList(lRepForm);
        modele.fireTableDataChanged();
    }
    
    public void refreshTableWorkOn() throws IOException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException{
        lRepFormWorkOn = controller.getListReparationFormWorkOn();

        tableWorkOn.removeAll();
        modeleWorkOn.setNewList(lRepFormWorkOn);
        modeleWorkOn.fireTableDataChanged();
    }

    /**
     * class for open window reparation form detail window
     */
    class workOnThisReparationForm extends MouseAdapter {
        private int row;

        public void mousePressed(MouseEvent e){
            try {
                int reponse = JOptionPane.showConfirmDialog(ReparationFormMenuWindow.this,
                        "Do you want really want to work on this reparationform ?",
                        "confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (reponse==JOptionPane.YES_OPTION){
                    int idRepForm = Integer.valueOf(modele.getValueAt(row, 0).toString());
                    controller.workOnReparationForm(lRepForm, userLogged, idRepForm);
                    refreshTable();
                    refreshTableWorkOn();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        public workOnThisReparationForm(int row){
            this.row = row;
        }
    }
    
    

    /**
     * class for open window reparation form detail window
     */
    class openReparationForm extends MouseAdapter {
        private int row;

        public void mousePressed(MouseEvent e){
            try {
                int idRepForm = Integer.valueOf(modeleWorkOn.getValueAt(row, 0).toString());
                controller.openReparationForm(lRepFormWorkOn, userLogged, idRepForm);
            } catch (IOException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        public openReparationForm(int row){
            this.row = row;
        }
    }
    
    /**
     * class for pop click listener
     */
    class PopClickListener extends MouseAdapter {
        
        public void mouseReleased(MouseEvent e){
            if (e.isPopupTrigger()){
                JTable currentTable = (JTable)e.getSource();
                int rowTable = currentTable.getSelectedRow();
                if(rowTable > -1){
                    int lineSelected = currentTable.convertRowIndexToModel(rowTable);
                    int row = lineSelected;
                    PopUp menu = new PopUp(row, currentTable);
                    menu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        }
    }
    
    /**
     * Method wich contain all parameter of window 
     * @param lRepForm list of reparation form
     * @param userLogged User
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException 
     */
    private void Display() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, IllegalArgumentException {
        this.initialize();
        this.setSize(1200,300);
        this.setTitle("Reparation Form Menu");
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                int reponse = JOptionPane.showConfirmDialog(ReparationFormMenuWindow.this,
                        "Do you want to close this window ?",
                        "confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (reponse==JOptionPane.YES_OPTION)
                    ReparationFormMenuWindow.this.dispose();
            }
        });
        this.setLocationRelativeTo(null);
    }
    
    
}
