/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.client.view;

import core.client.controller.ControllerStatsMenuWindow;
import core.client.controller.ControllerVehicleStatWindow;
import core.client.model.User;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author pds_user
 */
/**
 *
 * @author pds_user
 */
public class TypeVehicleStatWindow extends javax.swing.JFrame {
    ControllerVehicleStatWindow controller = null;
    User userLogged=null; 
    /**
     * Creates new form TypeVehicleStatWindow
     */
    public TypeVehicleStatWindow(User userLogged) throws ParseException, NoSuchAlgorithmException, NoSuchAlgorithmException, IOException{
        this.userLogged=userLogged;
        controller = new ControllerVehicleStatWindow(this.getClass().getSimpleName(), userLogged);
        initComponents();
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setTitle("Type Vehicule Statistics Menu");
        this.setLocationRelativeTo(null);
        
        this.setVisible(true);
    }


                     
    private void initComponents() throws ParseException, NoSuchAlgorithmException, IOException {

        labelTitle = new javax.swing.JLabel();
        labelNbBreakdown = new javax.swing.JLabel();
        nbBreakdownTable = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        labelBreakdownCost = new javax.swing.JLabel();
        breakdownCostTable = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        backButton = new javax.swing.JButton();
        labelVehicle = new javax.swing.JLabel();
        vehicleComboBox = new javax.swing.JComboBox<>();
        labelBreakdownList = new javax.swing.JLabel();
        breakdownListTable = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        labelTitle.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        labelTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTitle.setText("Vehicle Type's statistics");

        labelNbBreakdown.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelNbBreakdown.setText("Number of Breakdown : ");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {controller.getNbCarBreakdownThisMonth(), controller.getAvgCarBreakdownByMonth(), controller.getNbCarBreakdownThisYear(), controller.getAvgCarBreakdownByYear(), controller.getNbCarTotalBreakdown()}
            },
            new String [] {
                "This month", "Average Month", "Year", "Average Year", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        nbBreakdownTable.setViewportView(jTable1);

        labelBreakdownCost.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelBreakdownCost.setText("Breakdown cost : ");

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {controller.getPriceThisMonthBreakdownCar(), controller.getAvgPriceMonthBreakdownCar(), controller.getPriceThisYearBreakdownCar(), controller.getAvgPriceYearBreakdownCar(), controller.getPriceTotalBreakdownCar()}
            },
            new String [] {
                "This month", "Average Month", "Year", "Average Year", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        breakdownCostTable.setViewportView(jTable2);

        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    backButtonActionPerformed(evt);
                } catch (IOException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchMethodException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        labelVehicle.setText("Vehicle Type : ");

        vehicleComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Car", "Bike"}));
        vehicleComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                String value = vehicleComboBox.getSelectedItem().toString();
                if (value.equalsIgnoreCase("Car")){
                    try {
                        jTable1.setModel(new javax.swing.table.DefaultTableModel(
                                new Object [][] {
                                    {controller.getNbCarBreakdownThisMonth(), controller.getAvgCarBreakdownByMonth(), controller.getNbCarBreakdownThisYear(), controller.getAvgCarBreakdownByYear(), controller.getNbCarTotalBreakdown()}
                                },
                                new String [] {
                                    "This month", "Average Month", "Year", "Average Year", "Total"
                                }
                        ) {
                            boolean[] canEdit = new boolean [] {
                                false, false, false, false, false
                            };
                            
                            public boolean isCellEditable(int rowIndex, int columnIndex) {
                                return canEdit [columnIndex];
                            }
                        });
                        
                        jTable2.setModel(new javax.swing.table.DefaultTableModel(
                            new Object [][] {
                                {controller.getPriceThisMonthBreakdownCar(), controller.getAvgPriceMonthBreakdownCar(), controller.getPriceThisYearBreakdownCar(), controller.getAvgPriceYearBreakdownCar(), controller.getPriceTotalBreakdownCar()}
                            },
                            new String [] {
                                "This month", "Average Month", "Year", "Average Year", "Total"
                            }
                        ) {
                            boolean[] canEdit = new boolean [] {
                                false, false, false, false, false
                            };

                            public boolean isCellEditable(int rowIndex, int columnIndex) {
                                return canEdit [columnIndex];
                            }
                        });
                        
                        
                        String[] title = {"Number","Name"};
                        DefaultTableModel tableModel = new DefaultTableModel(title,0);
        
                        for (String[] array : controller.getListBreakdownCar()){
                            List<String> list = new ArrayList<String>(); 
                            for (String str : array){
                                list.add(str);
                            }
                            Object[] data = {list.get(0), list.get(1)};
                            tableModel.addRow(data);
                        }
        
                        jTable3.setModel(tableModel);
                    } catch (ParseException ex) {
                        Logger.getLogger(TypeVehicleStatWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NoSuchAlgorithmException ex) {
                        Logger.getLogger(TypeVehicleStatWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(TypeVehicleStatWindow.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    nbBreakdownTable.setViewportView(jTable1);
                    breakdownCostTable.setViewportView(jTable2);
                    breakdownListTable.setViewportView(jTable3);
                }
                else if (value.equalsIgnoreCase("Bike")){
                    try {
                        jTable1.setModel(new javax.swing.table.DefaultTableModel(
                                new Object [][] {
                                    {controller.getNbBikeBreakdownThisMonth(), controller.getAvgBikeBreakdownByMonth(), controller.getNbBikeBreakdownThisYear(), controller.getAvgBikeBreakdownByYear(), controller.getNbBikeTotalBreakdown()}
                                },
                                new String [] {
                                    "This month", "Average Month", "Year", "Average Year", "Total"
                                }
                        ) {
                            boolean[] canEdit = new boolean [] {
                                false, false, false, false, false
                            };
                            
                            public boolean isCellEditable(int rowIndex, int columnIndex) {
                                return canEdit [columnIndex];
                            }
                        });
                        
                        jTable2.setModel(new javax.swing.table.DefaultTableModel(
                            new Object [][] {
                                {controller.getPriceThisMonthBreakdownBike(), controller.getAvgPriceMonthBreakdownBike(), controller.getPriceThisYearBreakdownBike(), controller.getAvgPriceYearBreakdownBike(), controller.getPriceTotalBreakdownBike()}
                            },
                            new String [] {
                                "This month", "Average Month", "Year", "Average Year", "Total"
                            }
                        ) {
                            boolean[] canEdit = new boolean [] {
                                false, false, false, false, false
                            };

                            public boolean isCellEditable(int rowIndex, int columnIndex) {
                                return canEdit [columnIndex];
                            }
                        });
                        
                        
                        String[] title = {"Number","Name"};
                        DefaultTableModel tableModel = new DefaultTableModel(title,0);
        
                        for (String[] array : controller.getListBreakdownBike()){
                            List<String> list = new ArrayList<String>(); 
                            for (String str : array){
                                list.add(str);
                            }
                            Object[] data = {list.get(0), list.get(1)};
                            tableModel.addRow(data);
                        }
                        
        
                        jTable3.setModel(tableModel);
                        
                        
                        
                    } catch (ParseException ex) {
                        Logger.getLogger(TypeVehicleStatWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NoSuchAlgorithmException ex) {
                        Logger.getLogger(TypeVehicleStatWindow.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(TypeVehicleStatWindow.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    nbBreakdownTable.setViewportView(jTable1);
                    breakdownCostTable.setViewportView(jTable2);
                    breakdownListTable.setViewportView(jTable3);
                }
            }});

        labelBreakdownList.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelBreakdownList.setText("Breakdown list : ");
        String[] title = {"Number","Name"};
        DefaultTableModel tableModel = new DefaultTableModel(title,0);
       
        
        for (String[] array : controller.getListBreakdownCar()){
            List<String> list = new ArrayList<String>(); 
            for (String str : array){
                list.add(str);
            }
            Object[] data = {list.get(0), list.get(1)};
            tableModel.addRow(data);
        }
        
         jTable3.setModel(tableModel);
        
        breakdownListTable.setViewportView(jTable3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(labelTitle, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(labelVehicle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(vehicleComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelBreakdownList)
                        .addGap(118, 118, 118)
                        .addComponent(breakdownListTable, javax.swing.GroupLayout.PREFERRED_SIZE, 527, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(labelBreakdownCost)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(breakdownCostTable, javax.swing.GroupLayout.PREFERRED_SIZE, 527, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(labelNbBreakdown)
                            .addGap(67, 67, 67)
                            .addComponent(nbBreakdownTable, javax.swing.GroupLayout.PREFERRED_SIZE, 527, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(backButton)
                .addGap(359, 359, 359))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(labelTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(vehicleComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelVehicle))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelNbBreakdown)
                    .addComponent(nbBreakdownTable, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(breakdownCostTable, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelBreakdownCost))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelBreakdownList)
                    .addComponent(breakdownListTable, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(backButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }           
    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) throws IOException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException, IllegalArgumentException, IllegalAccessException, NoSuchAlgorithmException{
        controller.openStatisticMenuWindow();
        this.dispose();
    } 

    // Variables declaration                   
    private javax.swing.JButton backButton;
    private javax.swing.JScrollPane breakdownCostTable;
    private javax.swing.JScrollPane breakdownListTable;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JLabel labelBreakdownCost;
    private javax.swing.JLabel labelBreakdownList;
    private javax.swing.JLabel labelNbBreakdown;
    private javax.swing.JLabel labelTitle;
    private javax.swing.JLabel labelVehicle;
    private javax.swing.JScrollPane nbBreakdownTable;
    private javax.swing.JComboBox<String> vehicleComboBox;
    // End of variables declaration                   
}
