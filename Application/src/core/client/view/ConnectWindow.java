package core.client.view;

import core.client.controller.ControllerConnectWindow;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * Main window
 * @author Stephane.Schenkel, Brice.Boutamdja, Charles.Santerre
 */
public class ConnectWindow extends JFrame {

    /**
     * Declaration of atribute
     * @param panel JPanel of principal window
     * @param cancelButton JButton with label "Cancel"
     * @param okButton JButton with label "OK"
     * @param viewPwdButton with label "View"
     * @param loginField JTextField with size 10
     * @param passwordField JPasswordField with size 10
     * @param controller Object ControllerConnectWindow
     */
    JPanel panel = new JPanel();

    JButton cancelButton = new JButton("Cancel");

    JButton okButton = new JButton("OK");

    JButton viewPwdButton = new JButton("View");

    JTextField loginField = new JTextField(10);

    JPasswordField passwordField = new JPasswordField(10);
    char defaultPwd;

    ControllerConnectWindow controller = new ControllerConnectWindow();

    /**
     * Create window
     */
    public ConnectWindow() {
        BoxLayout panelLayout = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(panelLayout);

        /**
         * Create Menu Bar
         */
        JMenuBar menu = new JMenuBar();
        setJMenuBar(menu);
        JMenu tools = new JMenu("Tools");
        menu.add(tools);
        JMenu help = new JMenu("Help");
        menu.add(help);

        /**
         * Implement Menu Item for tools Menu
         */
        JMenuItem toolsItem1 = new JMenuItem("Options");
        tools.add(toolsItem1);
        JMenuItem toolsItem2 = new JMenuItem("About");
        help.add(toolsItem2);

        /**
         * Create panels
         */
        JPanel titlePanel = new JPanel(new FlowLayout());
        JPanel loginPanel = new JPanel(new FlowLayout());
        JPanel passwordPanel = new JPanel(new FlowLayout());
        JPanel listPanel = new JPanel(new FlowLayout());
        JPanel buttonPanel = new JPanel(new FlowLayout());

        /**
         * Add JLabel name of prototype
         */
        titlePanel.add(new JLabel("Connect to access the application"));

        /**
         * Add JLabel and JTextField for Login
         */
        loginPanel.add(new JLabel("Login :"));
        loginPanel.add(loginField);

        /**
         * Add JLabel and JTextField for Password
         */
        passwordPanel.add(new JLabel("Password :"));
        passwordPanel.add(passwordField);
        passwordPanel.add(viewPwdButton);

        
        /**
         * Field Listener
         */
        
        passwordField.addKeyListener(new FieldAdapter());
        loginField.addKeyListener(new FieldAdapter());
        
        /**
         * Buttons Listener
         */
        okButton.addMouseListener(new connectionButtonClick());

        cancelButton.addMouseListener(new cancelButtonClick());

        viewPwdButton.addMouseListener(new showButtonClick());

        /**
         * Add buttons
         */
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        /**
         * Add panels to the window
         */
        panel.add(titlePanel);
        panel.add(loginPanel);
        panel.add(passwordPanel);
        panel.add(buttonPanel);
        this.add(panel);

        /**
         * Parameters of the window
         */
        this.setSize(350, 250);
        this.setTitle("Connection window");
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                int reponse = JOptionPane.showConfirmDialog(ConnectWindow.this,
                        "Do you want to exit application ?",
                        "confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (reponse==JOptionPane.YES_OPTION)
                    ConnectWindow.this.dispose();
            }
        });
        this.setLocationRelativeTo(null);
    }
    
    /**
     * This method collect string answer of connect controller for check if user is valide,
     * so log it and dispose a view of menu adapted at type user or show error message
     */
    public void connectionMethod(){
        try {
            String answer = controller.openMenuWindow(loginField.getText(), passwordField.getText(), ConnectWindow.class.getSimpleName());
            if (answer.equals("success")) {
                ConnectWindow.this.dispose();
            } else if (answer.equals("empty")) {
                JOptionPane.showMessageDialog(panel, "Please enter a login and a password", "Connection error", JOptionPane.ERROR_MESSAGE);
            } else if (answer.equals("wrong")) {
                JOptionPane.showMessageDialog(panel, controller.getErrorMessage(), "Connection error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (UnknownHostException e) {
            JOptionPane.showMessageDialog(panel, "Server is unreachable (Unknown Host)", "Connection error", JOptionPane.ERROR_MESSAGE);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(panel, e.getMessage(), "Connection error", JOptionPane.ERROR_MESSAGE);
        } catch (ParseException e){
            JOptionPane.showMessageDialog(panel, e.getMessage(), "Connection error", JOptionPane.ERROR_MESSAGE);
        } catch (NoSuchAlgorithmException e){
            JOptionPane.showMessageDialog(panel, e.getMessage(), "Software error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    class FieldAdapter extends KeyAdapter{
        @Override
        public void keyTyped(KeyEvent args){
            if(args.getKeyChar() == KeyEvent.VK_ENTER)
                connectionMethod();
        }
    }

    class connectionButtonClick extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent args) {
            connectionMethod();
        }
    }

    class cancelButtonClick extends MouseAdapter {
        @Override    
        public void mouseClicked(MouseEvent args) {
            loginField.setText("");
            passwordField.setText("");
        }
    }

    class showButtonClick extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            defaultPwd = passwordField.getEchoChar();
            passwordField.setEchoChar((char) 0);
        }
        @Override
        public void mouseReleased(MouseEvent e) {
            passwordField.setEchoChar(defaultPwd);
        }
    }
}
