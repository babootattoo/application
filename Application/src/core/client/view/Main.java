package core.client.view;

import core.client.network.Configuration;
import javax.swing.JFrame;

public class Main {
    /**
     * Declaration of attribute
     * @param config Object Configuration
     */
    final public static Configuration Config = new Configuration();
    
    /**
     * Display view connectWindow
     * @param args 
     */
    public static void main(String[] args){
        JFrame window = new ConnectWindow();
    }
}
