/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.client.view;

import core.client.controller.ControllerDepotStatWindow;
import core.client.controller.ControllerStatsMenuWindow;
import core.client.model.User;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author quentin_goutte
 */
public class DepotStatWindow extends javax.swing.JFrame {
    ControllerDepotStatWindow controller = null;
    User userLogged = null;
    /**
     * Creates new form DepotStatWindow
     */
    public DepotStatWindow(User userLogged) throws ParseException, NoSuchAlgorithmException, NoSuchAlgorithmException, IOException {
        this.userLogged=userLogged;
        controller = new ControllerDepotStatWindow(this.getClass().getSimpleName(), userLogged);
        initComponents();
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setTitle("Depot Statistics Menu");
        this.setLocationRelativeTo(null);
        
        this.setVisible(true);
    }


                        
    private void initComponents() throws ParseException, NoSuchAlgorithmException, IOException {

        labelTitle = new javax.swing.JLabel();
        labelNbEmployee = new javax.swing.JLabel();
        labelNbVehicle = new javax.swing.JLabel();
        labelNbVehicleDepot = new javax.swing.JLabel();
        labelNbReparation = new javax.swing.JLabel();
        nbReparationTable = new javax.swing.JScrollPane();
        repTable = new javax.swing.JTable();
        labelAverageDuration = new javax.swing.JLabel();
        labelPartPurchased = new javax.swing.JLabel();
        labelExpenses = new javax.swing.JLabel();
        backButton = new javax.swing.JButton();
        partPurchasedTable = new javax.swing.JScrollPane();
        partsTable = new javax.swing.JTable();
        expensesTable = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        labelTitle.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        labelTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTitle.setText("Depot's statistics");

        labelNbEmployee.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelNbEmployee.setText("Number of employees : ");

        labelNbVehicle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelNbVehicle.setText("Number of vehicles : ");

        labelNbVehicleDepot.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelNbVehicleDepot.setText("Number of vehicles in depot : ");

        labelNbReparation.setText("Number of breakdown : ");

        repTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {controller.getNumberBreakdownThisMonth(), controller.getAvgBreakdownByMonth(), controller.getNumberBreakdownThisYear(), controller.getAvgBreakdownByYear()}
            },
            new String [] {
                "This month", "Average Month", "Year", "Average Year"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        nbReparationTable.setViewportView(repTable);

        labelAverageDuration.setText("Average duration of reparation in days: ");

        labelPartPurchased.setText("Parts purchased : ");

        labelExpenses.setText("Expenses : ");

        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    backButtonActionPerformed(evt);
                } catch (IOException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchMethodException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        partsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {controller.getNumberBuyingPiecesThisMonth(), controller.getAvgPiecesByMonth(), controller.getNumberBuyingPiecesThisYear(), controller.getAvgPiecesByYear()}
            },
            new String [] {
                "This month", "Average Month", "Year", "Average Year"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        partPurchasedTable.setViewportView(partsTable);

        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {controller.getPriceThisMonth(), controller.getAvgPriceByMonth(), controller.getPriceThisYear(), controller.getAvgPriceByYear()}
            },
            new String [] {
                "This month", "Average Month", "Year", "Average Year"
            }
            
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        expensesTable.setViewportView(jTable5);
        if (controller.getNumberEmployee()>0){
            Integer value =controller.getNumberEmployee();
            jLabel1.setText(value.toString());
        }
        else {
            jLabel1.setText("N/A");
        }
        
       
        if (controller.getNumberVehicle()>0){
            Integer value =controller.getNumberVehicle();
            jLabel2.setText(value.toString());
        }
        else {
            jLabel2.setText("N/A");
        }

        if (controller.getNumberVehicle()>0){
            Integer value =controller.getNumberVehicleInDepot();
            jLabel3.setText(value.toString());
        }
        else {
            jLabel3.setText("N/A");
        }
        if (controller.getAvgDuringBreakdown()>0){
            Double value =controller.getAvgDuringBreakdown();
            jLabel4.setText(value.toString());
        }
        else {
            jLabel4.setText("N/A");
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(labelAverageDuration)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel4))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(labelNbVehicleDepot)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel3))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(labelNbVehicle)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(labelNbEmployee)
                                    .addGap(80, 80, 80)
                                    .addComponent(jLabel1))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelNbReparation)
                            .addComponent(labelPartPurchased)
                            .addComponent(labelExpenses))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(expensesTable, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(partPurchasedTable, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(nbReparationTable, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(277, 277, 277)
                .addComponent(backButton)
                .addContainerGap(276, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(labelTitle)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNbEmployee)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNbVehicle)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNbVehicleDepot)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelAverageDuration)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelNbReparation)
                    .addComponent(nbReparationTable, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(labelPartPurchased))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(partPurchasedTable, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelExpenses)
                    .addComponent(expensesTable, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(backButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }   
    
    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) throws IOException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException, IllegalArgumentException, IllegalAccessException, NoSuchAlgorithmException{
        controller.openStatisticMenuWindow();
        this.dispose();
    }    

    /**
     * @param args the command line arguments
     */
    /*public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DepotStatWindow().setVisible(true);
            }
        });
    }*/

    // Variables declaration                     
    private javax.swing.JButton backButton;
    private javax.swing.JScrollPane expensesTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTable jTable5;
    private javax.swing.JLabel labelAverageDuration;
    private javax.swing.JLabel labelExpenses;
    private javax.swing.JLabel labelNbEmployee;
    private javax.swing.JLabel labelNbReparation;
    private javax.swing.JLabel labelNbVehicle;
    private javax.swing.JLabel labelNbVehicleDepot;
    private javax.swing.JLabel labelPartPurchased;
    private javax.swing.JLabel labelTitle;
    private javax.swing.JScrollPane nbReparationTable;
    private javax.swing.JScrollPane partPurchasedTable;
    private javax.swing.JTable partsTable;
    private javax.swing.JTable repTable;
    // End of variables declaration                   
}
