package core.client.view;

import core.client.controller.ControllerReparatorMenuWindow;
import core.client.controller.ControllerVehiclesWindow;
import core.client.model.User;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Charles.Santerre, Brice.Boutamdja
 */
public class ReparatorMenuWindow extends JFrame {

    /**
     * Declaration of atribute
     * @param userLogged Object to User class
     * @param panel JPanel of principal window
     * @param openListVehicle JButton with label "open list"
     * @param openReparationForm JButton with label "open reparation form (proto)"
     * @param disconnectButton JButton with label "disconnect"
     * @param controller Object to ControllerReparatorMenuWindow class
     */

    User userLogged;

    JPanel panel = new JPanel();
    
    JButton openReparationForm = new JButton("open reparation form");

    JButton scanVehicle = new JButton("scan vehicle");

    JButton disconnectButton = new JButton("disconnect");
    
    JButton vehicleWindow = new JButton("Vehicle Details");
    
    ControllerReparatorMenuWindow controller = null;


    public ReparatorMenuWindow(User userLogged) {
        this.userLogged = userLogged;
        controller = new ControllerReparatorMenuWindow(this.getClass().getSimpleName(), userLogged);
        BorderLayout panelLayout = new BorderLayout();
        panel.setLayout(panelLayout);

        /**
         * Create Menu Bar
        */
        JMenuBar menu= new JMenuBar();
        setJMenuBar(menu);
        JMenu tools = new JMenu("Tools");
        menu.add(tools);
        JMenu help = new JMenu("Help");
        menu.add(help);
        
        /**
         * Implement Menu Item for tools Menu
         */
        JMenuItem toolsItem1 = new JMenuItem("Options");
        tools.add(toolsItem1);
        JMenuItem toolsItem2 = new JMenuItem("About");
        help.add(toolsItem2);
        /**
         * Create panels
         */
        JPanel titlePanel = new JPanel(new FlowLayout());
        JPanel listPanel = new JPanel(new FlowLayout());
        JPanel buttonPanel = new JPanel();
        
        BoxLayout panelButtonLayout = new BoxLayout(buttonPanel, BoxLayout.Y_AXIS);
        buttonPanel.setLayout(panelButtonLayout);

        /**
         * Add JLabel name of prototype
         */
        
        titlePanel.add(new JLabel(userLogged.getFirstName() + " " + userLogged.getLastName() + " - " + userLogged.getTypeUser()));
        titlePanel.add(new JLabel("Menu"));

        /**
         * Buttons Listener
         */
        openReparationForm.addMouseListener(new openReparationFormClick());
        scanVehicle.addMouseListener(new scanVehicleClick());
        disconnectButton.addMouseListener(new disconnectClick());
        vehicleWindow.addMouseListener(new vehicleWindowClick());

        /**
         * Buttons Center alignment
         */
        openReparationForm.setAlignmentX(JButton.CENTER_ALIGNMENT);
        scanVehicle.setAlignmentX(JButton.CENTER_ALIGNMENT);
        vehicleWindow.setAlignmentX(JButton.CENTER_ALIGNMENT);
        disconnectButton.setAlignmentX(JButton.CENTER_ALIGNMENT);
        
        /**
         * Add buttons
         */
        buttonPanel.add(openReparationForm);
        buttonPanel.add(scanVehicle);
        buttonPanel.add(vehicleWindow);
        buttonPanel.add(disconnectButton);

        /**
         * Add panels to the window
         */
        panel.add(titlePanel, BorderLayout.NORTH);
        panel.add(buttonPanel, BorderLayout.CENTER);
        this.add(panel);

        /**
         * Parameters of the window
         */
        this.setSize(350, 250);
        this.setTitle("Client Prototype");
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                int reponse = JOptionPane.showConfirmDialog(ReparatorMenuWindow.this,
                        "Do you want to exit application ?",
                        "confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (reponse==JOptionPane.YES_OPTION)
                    ReparatorMenuWindow.this.dispose();
            }
        });

        this.setLocationRelativeTo(null);
    }
    
    class openReparationFormClick extends MouseAdapter{
        public void mouseClicked(MouseEvent args){
            try {
                String answer = controller.openReparationForm();
                switch(answer){
                    case "close":
                        ReparatorMenuWindow.this.dispose();
                        break;
                    case "error":
                        JOptionPane.showMessageDialog(ReparatorMenuWindow.this, controller.getErrorMessage());
                        break;
                    default:
                        break;
                }
            } catch (IOException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    class scanVehicleClick extends MouseAdapter{
        public void mouseClicked(MouseEvent args){
            try {
                String answer = controller.openScanVehicleWindow();
                switch(answer){
                    case "close":
                        ReparatorMenuWindow.this.dispose();
                        break;
                    default:
                        break;
                }
            } catch (IOException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    class vehicleWindowClick extends MouseAdapter{
        public void mouseClicked(MouseEvent args){
            try {
                String answer = controller.openVehiclesWindow();
                switch(answer){
                    case "close":
                        ReparatorMenuWindow.this.dispose();
                        break;
                    default:
                        break;
                }
            } catch (IOException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    class disconnectClick extends MouseAdapter{
        public void mouseClicked(MouseEvent args){
            try {
                controller.disconnectUser();
                ReparatorMenuWindow.this.dispose();
            } catch (IOException ex) {
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
