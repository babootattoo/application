/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.client.view;

import core.client.controller.ControllerReparatorStatWindow;
import core.client.controller.ControllerStatsMenuWindow;
import core.client.model.User;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;

/**
 *
 * @author quentin_goutte
 */
public class ReparatorStatWindow extends javax.swing.JFrame {
    ControllerReparatorStatWindow controller = null;
    User userLogged = null;
    /**
     * Creates new form ReparatorStatWindow
     */
    public ReparatorStatWindow(User userLogged) throws ParseException, NoSuchAlgorithmException, IOException{
        this.userLogged=userLogged;
        controller = new ControllerReparatorStatWindow(this.getClass().getSimpleName(), userLogged);
        initComponents();
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setTitle("Reparator Statistics Menu");
        this.setLocationRelativeTo(null);
        
        this.setVisible(true);
    }

                       
    private void initComponents() throws ParseException, NoSuchAlgorithmException, IOException {

        labelTitle = new javax.swing.JLabel();
        labelReparator = new javax.swing.JLabel();
        reparatorComboBox = new javax.swing.JComboBox<>();
        labelNbReparation = new javax.swing.JLabel();
        nbReparationTable = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        backButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        labelTitle.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        labelTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTitle.setText("Reparator's statistics");
        List<User> list = controller.getAllReparator();
        for (User user : list){
            reparatorComboBox.addItem(new Item(user.getId(),user.getFirstName()+" "+user.getLastName()));
        }
        Item value = (Item) reparatorComboBox.getSelectedItem();
                int result = value.getId();
        
        reparatorComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Item value = (Item) reparatorComboBox.getSelectedItem();
                int result = value.getId();
                
                try {
                    jTable1.setModel(new javax.swing.table.DefaultTableModel(
                            new Object [][] {
                                {controller.getNumberReparationToday(result), controller.getNumberReparationThisMonth(result), controller.getNumberReparationThisYear(result), controller.getNumberReparationTotal(result)}
                            },
                            new String [] {
                                "Today", "This month", "Year", "Total"
                            }
                    ) {
                        boolean[] canEdit = new boolean [] {
                            false, false, false, false
                        };
                        
                        public boolean isCellEditable(int rowIndex, int columnIndex) {
                            return canEdit [columnIndex];
                        }
                    });
                } catch (ParseException ex) {
                    Logger.getLogger(ReparatorStatWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(ReparatorStatWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ReparatorStatWindow.class.getName()).log(Level.SEVERE, null, ex);
                }
                nbReparationTable.setViewportView(jTable1);
                }});
        
        
        labelReparator.setText("Reparator : ");

        //reparatorComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        labelNbReparation.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelNbReparation.setText("Number of Reparation : ");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {controller.getNumberReparationToday(result), controller.getNumberReparationThisMonth(result), controller.getNumberReparationThisYear(result), controller.getNumberReparationTotal(result)}
            },
            new String [] {
                "Today", "This month", "Year", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        nbReparationTable.setViewportView(jTable1);

        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    backButtonActionPerformed(evt);
                } catch (IOException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchMethodException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(StatisticMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(labelTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(labelReparator)
                        .addGap(11, 11, 11)
                        .addComponent(reparatorComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelNbReparation)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nbReparationTable, javax.swing.GroupLayout.DEFAULT_SIZE, 604, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(backButton)
                .addGap(370, 370, 370))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(labelTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelReparator)
                    .addComponent(reparatorComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelNbReparation)
                    .addComponent(nbReparationTable, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(backButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }   
    
    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) throws IOException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException, IllegalArgumentException, IllegalAccessException, NoSuchAlgorithmException{
        controller.openStatisticMenuWindow();
        this.dispose();
    }    
    
    

    // Variables declaration               
    private javax.swing.JButton backButton;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel labelNbReparation;
    private javax.swing.JLabel labelReparator;
    private javax.swing.JLabel labelTitle;
    private javax.swing.JScrollPane nbReparationTable;
    private javax.swing.JComboBox reparatorComboBox;
    // End of variables declaration        
    
    public class Item {
        private int id;
        private String description;
        
        public Item (int id, String description){
            this.id=id;
            this.description=description;
        }
        
        public int getId(){
            return this.id;
        }
        
        public String getDescription(){
            return this.description;
        }
        
        @Override
        public  String toString(){
            return description;
        }
    }
}

