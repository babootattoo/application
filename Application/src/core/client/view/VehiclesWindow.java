package core.client.view;

import core.client.controller.ControllerVehiclesWindow;
import core.client.model.Bike;
import core.client.model.Car;
import core.client.model.User;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.UnsupportedEncodingException;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

public class VehiclesWindow extends JFrame {

    private ControllerVehiclesWindow controller = null;              
    private JLabel jlbl_allVehicles;
    private JLabel jlblDisplayCountCard;
    private JLabel jlbl_listBike;
    private JLabel jlbl_listCar;
    private JLabel jlbl_totalVehicle;
    private JScrollPane jScrollPane1, jScrollPane2;
    private List<String> bike, car;
    User userLogged;
    
    public VehiclesWindow(User userLogged) throws IOException, UnsupportedEncodingException, ParseException{
        this.userLogged = userLogged;
        controller = new ControllerVehiclesWindow(this.getClass().getSimpleName(), userLogged);
        try {
            initialize();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(VehiclesWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
                  
    private void initialize() throws IOException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException {

        jlbl_allVehicles = new JLabel();
        jScrollPane1 = new JScrollPane();
        jlbl_listBike = new JLabel();
        jScrollPane2 = new JScrollPane();
        jlbl_listCar = new JLabel();
        jlbl_totalVehicle = new JLabel();
        jlblDisplayCountCard = new JLabel();
        
        jlblDisplayCountCard.setFont(new Font("Dialog", 0, 12));
        setTitle("Vehicles window");
        List<String> allCardState = controller.getCountCardState();
        jlblDisplayCountCard.setText("<html>");
        for (int i = 0; i < allCardState.size(); i += 2){
            jlblDisplayCountCard.setText(jlblDisplayCountCard.getText() + allCardState.get(i) + " : "+ allCardState.get(i+1) + "<br/><br/>");
        }
        jlblDisplayCountCard.setText(jlblDisplayCountCard.getText() + "</html>"); 
        
        bike = controller.getListPriorityPerBike();
        Object[][] tableData = null;
        String [] cl =  {"Bike ID","Bike RFID chip", "Priority max level"};
        DefaultTableModel tableModelBike = new DefaultTableModel(tableData,cl);
        for (int i = 0; i <= bike.size()-1; i++){
            String[] elements = bike.get(i).split("_");
            Object[] rowData = new Object[]{
                Integer.parseInt(elements[0]),
                Integer.parseInt(elements[1]),
                Integer.parseInt(elements[2])
            };
            tableModelBike.addRow(rowData);
        }
        JTable table_bike = new JTable(tableModelBike);
        ListSelectionModel selectionModel = table_bike.getSelectionModel();
        table_bike.setCellSelectionEnabled(false);
        selectionModel.addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent e){
                String typeVehicle = table_bike.getColumnName(0);
                int id = Integer.parseInt(table_bike.getValueAt(table_bike.getSelectedRow(),0).toString());
                VehicleDetails frame = new VehicleDetails(id, typeVehicle, userLogged);
                frame.setVisible(true);
            }
        });
        car = controller.getListPriorityPerCar();
        tableData = null;
        String [] col_car =  {"Car ID","Matriculation","Priority max level"};
        DefaultTableModel tableModelCar = new DefaultTableModel(tableData,col_car);
        for (int i = 0; i < car.size(); i+= 3){
            Object[] rowData = new Object[]{
                car.get(i),
                car.get(i + 1),
                car.get(i + 2)
            };
            tableModelCar.addRow(rowData);
        }
        JTable table_car = new JTable(tableModelCar);
        selectionModel = table_car.getSelectionModel();
        table_car.setCellSelectionEnabled(false);
        selectionModel.addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent e){
                String typeVehicle = table_car.getColumnName(0);
                int id = Integer.parseInt(table_car.getValueAt(table_car.getSelectedRow(),0).toString());
                VehicleDetails frame = new VehicleDetails(id, typeVehicle, userLogged);
                frame.setVisible(true);
            }
        });

        jlbl_allVehicles.setFont(new Font("Dialog", 1, 18));
        jlbl_allVehicles.setHorizontalAlignment(SwingConstants.CENTER);
        jlbl_allVehicles.setText("All Vehicles");

        jScrollPane1.setViewportView(table_bike);

        jlbl_listCar.setText("List of car :");
        
        jScrollPane2.setViewportView(table_car);

        jlbl_listBike.setText("List of bike :");

        jlbl_totalVehicle.setText("Total vehicles today sorted by status :");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jlbl_allVehicles, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(jlbl_listBike)
                                    .addComponent(jlbl_totalVehicle)
                                    .addComponent(jlbl_listCar))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jlblDisplayCountCard)))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGap(36, 36, 36))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jlbl_allVehicles)
                .addGap(18, 18, 18)
                .addComponent(jlbl_listBike)
                .addGap(4, 4, 4)
                .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jlbl_listCar)
                .addGap(4, 4, 4)
                .addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(jlbl_totalVehicle)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblDisplayCountCard))
                .addContainerGap(49, Short.MAX_VALUE))
        );
        pack();
        setVisible(true);
    }
    
    public class ModeleBike extends AbstractTableModel{
        
        private List<Bike> bk;
        private String[] title = {"Bike RFID", "Status", "Urgency degree"};
        
        public String getColumnName(int columnIndex){
            return title[columnIndex];
        }
        
        public ModeleBike(List<Bike> bk){
            this.bk = bk;
        }
        
        public void setNewList(List<Bike> bk){
            this.bk = bk;
        }

        @Override
        public int getRowCount() {
            return bk.size();
        }

        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex){
                case 0 :
                    return Integer.class;
                case 1 :
                    return String.class;
                case 2 :
                    return String.class;
                default :
                    return Object.class;
            }
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            switch (columnIndex){
                case 0 :
                    return bk.get(rowIndex).getRfidChip();
                case 1 :
                    return bk.get(rowIndex).getBreakdowns();
                case 2 :
                    
                default :
                    throw new IllegalArgumentException();
            }
        }

        @Override
        public int getColumnCount() {
            return title.length;
        }
    }
 
    public class ModeleCar extends AbstractTableModel{
        
        private List<Car> car;
        private String[] title = {"Bike RFID", "Status", "Urgency degree"};

        
        public String getColumnName(int columnIndex){
            return title[columnIndex];
        }
        
        public ModeleCar(List<Car> car){
            this.car = car;
        }
        
        public void setNewList(List<Car> car){
            this.car = car;
        }

        @Override
        public int getRowCount() {
            return car.size();
        }

        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex){
                case 0 :
                    return Integer.class;
                case 1 :
                    return String.class;
                case 2 :
                    return String.class;
                default :
                    return Object.class;
            }
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            switch (columnIndex){
                case 0 :
                    return car.get(rowIndex).getMatriculation();
                case 1 :
                    //return car.get(rowIndex).
                case 2 :
                    
                default :
                    throw new IllegalArgumentException();
            }
        }

        @Override
        public int getColumnCount() {
            return title.length;
        }
    }

}
