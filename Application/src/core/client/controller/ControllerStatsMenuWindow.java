/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.client.controller;

import core.client.model.User;
import core.client.view.AdministratorMenuWindow;
import core.client.view.DepotStatWindow;
import core.client.view.ReparatorStatWindow;
import core.client.view.StatisticMenuWindow;
import core.client.view.TypeVehicleStatWindow;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

/**
 *
 * @author pds_user
 */
public class ControllerStatsMenuWindow extends Controller{
    private String classView = null; 
    private User userLogged = null;
    
    public ControllerStatsMenuWindow (String classView, User userLogged){
        this.classView = classView;
        this.userLogged = userLogged;
        System.out.println(userLogged.getLogin());
    }
    
    public String openDepotStatWindow()throws IOException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException, IllegalArgumentException, IllegalAccessException, NoSuchAlgorithmException{
        String answer = "";
        new DepotStatWindow(this.userLogged);
        return answer;
    }
    
    public String openReparatorStatWindow()throws IOException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException, IllegalArgumentException, IllegalAccessException, NoSuchAlgorithmException{
        String answer = "";
        new ReparatorStatWindow(this.userLogged);
        return answer;
    }
    
    public String openTypeVehicleStatWindow()throws IOException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException, IllegalArgumentException, IllegalAccessException, NoSuchAlgorithmException{
        String answer = "";
        new TypeVehicleStatWindow(this.userLogged);
        return answer;
    }
    public String openStatisticMenuWindow()throws IOException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException, IllegalArgumentException, IllegalAccessException, NoSuchAlgorithmException{
        String answer = "";
        new StatisticMenuWindow(this.userLogged);
        return answer;
    }
    public String openAdministratorMenuWindow()throws IOException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException, IllegalArgumentException, IllegalAccessException, NoSuchAlgorithmException{
        String answer = "";
        new AdministratorMenuWindow(this.userLogged);
        return answer;
    }
}
