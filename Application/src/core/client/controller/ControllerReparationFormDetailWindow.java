package core.client.controller;

import core.client.model.BreakdownDAO;
import core.client.model.Breakdown;
import core.client.model.CardStateDAO;
import core.client.model.Comment;
import core.client.model.CommentDAO;
import core.client.model.Part;
import core.client.model.PartDAO;
import core.client.model.ReparationForm;
import core.client.model.ReparationFormDAO;
import core.client.model.User;
import core.client.model.VehicleDAO;
import core.client.view.ConnectWindow;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 * Controller of ReparationFormDetailWindow
 * @author Stephane.Schenkel
 */
public class ControllerReparationFormDetailWindow extends Controller {
    
    private String classView = null; 
    private User userLogged = null;
    
    public ControllerReparationFormDetailWindow(User userLogged, String classView){
        this.userLogged = userLogged;
        this.classView = classView;
    }
    
    /**
     * Get the list of all card state from the server and return it
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<String> getListCardState() throws ParseException, NoSuchAlgorithmException, IOException{
        List<String> result = new ArrayList<String>();
        JsonObject inputJson = verifJson(CardStateDAO.getAllCardState(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("List");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(jsonArray.getString(i));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        return result;
    }
    
    /**
     * Get the list of all comments of the specified reparationform from the server and return it
     * @param idForm
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<Comment> getComments(int idForm) throws ParseException, NoSuchAlgorithmException, IOException{
        List<Comment> result = new ArrayList<Comment>();
        JsonObject inputJson = verifJson(CommentDAO.getCommentByFormId(idForm, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("CommentList");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(Comment.deserialize(jsonArray.getJsonObject(i)));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        return result;
    }
    
    /**
     * Send the message and the idForm to the server to add a comment in the database
     * @param idForm
     * @param message
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public String addComment(int idForm, String message) throws ParseException, NoSuchAlgorithmException, IOException{
        String result = "";
        
        JsonObject inputJson = verifJson(CommentDAO.addComment(userLogged.getId(), idForm, message, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                result = "success";
                break;
            case "Error":
                result = "error";
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
    /**
     * Get the list of available part for the specified carmodel from the server
     * @param idCarModel
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<Part> getListUsablePartCar(int idCarModel) throws ParseException, NoSuchAlgorithmException, IOException{
        List<Part> result = new ArrayList<Part>();
        
        JsonObject inputJson = verifJson(PartDAO.getListUsablePartCarModel(idCarModel, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("PartList");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(Part.deserialize(jsonArray.getJsonObject(i)));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
    /**
     * Get the list of available part for bikes from the server
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<Part> getListUsablePartBike() throws ParseException, NoSuchAlgorithmException, IOException{
        List<Part> result = new ArrayList<Part>();
        
        JsonObject inputJson = verifJson(PartDAO.getListUsablePartBike(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("PartList");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(Part.deserialize(jsonArray.getJsonObject(i)));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
    /**
     * Send the idPart and idVehicle to the server to add a part to the specified vehicle in the database
     * @param idVehicle
     * @param idPart
     * @param table
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<Part> addPart(int idVehicle, int idPart, String table) throws ParseException, NoSuchAlgorithmException, IOException{
        List<Part> result = new ArrayList<Part>();
        
        JsonObject inputJson = verifJson(VehicleDAO.addPart(idVehicle, idPart, table, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("PartList");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(Part.deserialize(jsonArray.getJsonObject(i)));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
    /**
     * Send the idPart and idVehicle to the server to delete a part to the specified vehicle in the database
     * @param idVehicle
     * @param idPart
     * @param table
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<Part> deletePart(int idVehicle, int idPart, String table) throws ParseException, NoSuchAlgorithmException, IOException{
        List<Part> result = new ArrayList<Part>();
        
        JsonObject inputJson = verifJson(VehicleDAO.deletePart(idVehicle, idPart, table, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("PartList");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(Part.deserialize(jsonArray.getJsonObject(i)));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
    /**
     * Send the idPart and idVehicle to the server to add a part to the specified vehicle in the database
     * @param idCarModel
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<Breakdown> getListHaveBreakdownCar(int idCarModel, int idCar) throws ParseException, NoSuchAlgorithmException, IOException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        
        JsonObject inputJson = verifJson(BreakdownDAO.getListBreakdownCarModel(idCarModel, idCar, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("BreakdownList");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(Breakdown.deserialize(jsonArray.getJsonObject(i)));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
    /**
     * Get the list of breakdown that a bike can have from the server
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<Breakdown> getListHaveBreakdownBike(int idBike) throws ParseException, NoSuchAlgorithmException, IOException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        
        JsonObject inputJson = verifJson(BreakdownDAO.getListBreakdownBike(idBike, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("BreakdownList");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(Breakdown.deserialize(jsonArray.getJsonObject(i)));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
    /**
     * Send the idVehicle and the idBreakdown to the server to add the breakdown to the vehicle in the database
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<Breakdown> addBreakdown(int idVehicle, int idBreakdown, String table) throws ParseException, NoSuchAlgorithmException, IOException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        
        JsonObject inputJson = verifJson(VehicleDAO.addBreakdown(idVehicle, idBreakdown, table, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("BreakdownList");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(Breakdown.deserialize(jsonArray.getJsonObject(i)));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
    /**
     * Send the idVehicle and the idBreakdown to the server to delete the breakdown from the vehicle in the database
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<Breakdown> deleteBreakdown(int idVehicle, int idBreakdown, String table) throws ParseException, NoSuchAlgorithmException, IOException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        
        JsonObject inputJson = verifJson(VehicleDAO.deleteBreakdown(idVehicle, idBreakdown, table, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("BreakdownList");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(Breakdown.deserialize(jsonArray.getJsonObject(i)));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
    /**
     * Send the idVehicle and the idBreakdown to the server to update the breakdown of the vehicle in the database
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<Breakdown> markAsResolvedBreakdown(int idVehicle, int idBreakdown, String table) throws ParseException, NoSuchAlgorithmException, IOException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        
        JsonObject inputJson = verifJson(VehicleDAO.markAsResolvedBreakdown(idVehicle, idBreakdown, table, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("BreakdownList");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(Breakdown.deserialize(jsonArray.getJsonObject(i)));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
    /**
     * Send the ReparationForm to the server to update its attributes in the database
     * @param repForm
     * @return
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws UnsupportedEncodingException
     * @throws ParseException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException 
     */
    public String updateReparationFormDetails(ReparationForm repForm) throws NoSuchAlgorithmException, IOException, UnsupportedEncodingException, ParseException, IllegalAccessException, NoSuchMethodException, InvocationTargetException{
        String result = "";
        
        JsonObject inputJson = verifJson(ReparationFormDAO.updateReparatorDetails(repForm, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                result = "success";
                break;
            case "Error":
                result = "error";
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
}
