package core.client.controller;

import core.client.model.User;
import core.client.model.UserDAO;
import core.client.view.ConnectWindow;
import java.awt.Color;
import java.awt.FlowLayout;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author pds_user
 */
public class Controller {
    
    /**
     * Declaration of attribute
     * @param errorMessage String
     */
    String errorMessage = "";
    
    /**
     * Return error message situation of adapted
     * @return String message
     */
    public String getErrorMessage(){
        return this.errorMessage;
    }
    
    public JsonObject verifJson(JsonObject json) throws UnsupportedEncodingException, NoSuchAlgorithmException, IOException, ParseException{
        JsonObject jsonReturn = null;
        if(json.getString("Action").equals("Reconnection_Asked")){
            JPanel fieldsPan = new JPanel();
            fieldsPan.setLayout(new BoxLayout(fieldsPan, BoxLayout.Y_AXIS));
            JPanel loginPan = new JPanel();
            loginPan.setLayout(new FlowLayout());
            JPanel pwdPan = new JPanel();
            pwdPan.setLayout(new FlowLayout());
            JTextField loginField = new JTextField(10);
            JPasswordField pwdField = new JPasswordField(10);
            
            JLabel message = new JLabel();
            message.setText("You have been disconnected (30min timeout). Please reconnect.");
            JLabel message2 = new JLabel();
            message.setText("If you click on cancel, you will lose your current work.");
            
            JLabel errorText = new JLabel();
            errorText.setForeground(Color.RED);
            
            loginPan.add(new JLabel("Login :"));
            loginPan.add(loginField);
            pwdPan.add(new JLabel("Password : "));
            pwdPan.add(pwdField);
            
            fieldsPan.add(message);
            fieldsPan.add(message2);
            fieldsPan.add(loginPan);
            fieldsPan.add(pwdPan);
            fieldsPan.add(errorText);
            
            boolean confirmed = false;
            int result;
            do{
                result = JOptionPane.showConfirmDialog(null, fieldsPan, "Reconnection Window", JOptionPane.OK_CANCEL_OPTION);
                errorText.setText("");
                if(result == JOptionPane.OK_OPTION){
                    String loginInput = loginField.getText();
                    String pwdInput = pwdField.getText();
                    if(loginInput.equals("") || pwdInput.equals("")){
                        errorText.setText("Please fill all the fields.");
                    }else{
                        String cryptedPassword = User.cryptPassword(pwdInput);
                        JsonObject inputJson = UserDAO.tryLogin(loginInput, cryptedPassword, "ConnectWindow");
                        switch(inputJson.getString("State")){
                            case "Success":
                                confirmed = true;
                                JsonObjectBuilder builder = Json.createObjectBuilder();
                                builder.add("State", "Reconnection_Success");
                                builder.add("User", inputJson.getJsonObject("User"));
                                jsonReturn = builder.build();
                                break;
                            case "Error":
                                errorText.setText("Incorrect login and password.");
                                break;
                        }
                    }
                }
            }while(result != JOptionPane.CANCEL_OPTION && !confirmed);
            if(result == JOptionPane.CANCEL_OPTION || result == JOptionPane.CLOSED_OPTION){
                JsonObjectBuilder cancelJson = Json.createObjectBuilder();;
                cancelJson.add("State", "Cancelled_Reconnection");
                jsonReturn = cancelJson.build();
            }
        }else{
            jsonReturn = json;
        }
        return jsonReturn;
    }
    
    public void disconnectUser(User userLogged, String classView) throws IOException{
        UserDAO.disconnectUser(userLogged, classView);
        new ConnectWindow();
    }
}
