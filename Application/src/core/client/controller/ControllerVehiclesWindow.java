package core.client.controller;

import core.client.model.Bike;
import core.client.model.BreakdownDAO;
import core.client.model.Car;
import core.client.model.CardStateDAO;
import core.client.model.User;
import core.client.model.VehicleDAO;
import core.client.view.ConnectWindow;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;

public class ControllerVehiclesWindow extends Controller{
   
    private String classView; 
    private User userLogged;
    
    public ControllerVehiclesWindow(String classView, User userLogged){
        this.classView = classView;
        this.userLogged = userLogged;
    }
    
    /**
     * This method receive and send list of bike
     * @return List of list priority per bike
     * @throws IOException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws ParseException 
     */
    public List<String> getListPriorityPerBike() throws IOException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException{
        List<String> allPriorityPerBike = new ArrayList<String>();
        JsonObject inputJson = verifJson(BreakdownDAO.getListPriorityPerBike(userLogged, classView));
        String stateCom = inputJson.getString("State");
        System.out.println(stateCom);
        switch (stateCom){
            case "Success" :
                JsonArray jsonArray = inputJson.getJsonArray("List");
                for(int i = 0; i < jsonArray.size(); i++){
                    allPriorityPerBike.add(jsonArray.getString(i));
                }
                break;
            case "Error" :
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                new ConnectWindow();
                break;
        }
        return allPriorityPerBike;
    }
    
     /**
     * This method receive and send list of priority per car
     * @return List of reparation form
     * @throws IOException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws ParseException 
     */
    public List<String> getListPriorityPerCar() throws IOException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException{
        List<String> allPriorityPerCar = new ArrayList<String>();
        JsonObject inputJson = verifJson(BreakdownDAO.getListPriorityPerCar(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success" :
                JsonArray jsonArray = inputJson.getJsonArray("List");
                for(int i = 0; i < jsonArray.size(); i++){
                    allPriorityPerCar.add(jsonArray.getString(i));
                }
                break;
            case "Error" :
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                new ConnectWindow();
                break;
        }
        return allPriorityPerCar;
    }
    
    
    public List<String> getCountCardState() throws IOException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException{
        List<String> allCardState = new ArrayList<String>();
        JsonObject inputJson = verifJson(CardStateDAO.getCountCardState(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success" :
                JsonArray jsonArray = inputJson.getJsonArray("List");
                for(int i = 0; i < jsonArray.size(); i++){
                    allCardState.add(jsonArray.getString(i));
                }
                break;
            case "Error" :
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                new ConnectWindow();
                break;
        }
        return allCardState;
    }
    

    
}
