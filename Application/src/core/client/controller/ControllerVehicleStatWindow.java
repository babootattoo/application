/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.client.controller;

import core.client.model.BreakdownDAO;
import core.client.model.PartDAO;
import core.client.model.User;
import core.client.view.StatisticMenuWindow;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 *
 * @author pds_user
 */
public class ControllerVehicleStatWindow extends Controller {
    private String classView = null; 
    private User userLogged = null;
    
    public ControllerVehicleStatWindow(String classView, User userLogged){
        this.userLogged = userLogged;
        this.classView = classView;
    }

    public String openStatisticMenuWindow()throws IOException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException, IllegalArgumentException, IllegalAccessException, NoSuchAlgorithmException{
        String answer = "";
        new StatisticMenuWindow(userLogged);
        return answer;
    }
    
    public int getNbBikeTotalBreakdown()throws ParseException, NoSuchAlgorithmException, IOException{
        int result=0;
        JsonObject inputJson = verifJson(BreakdownDAO.getNbBikeTotalBreakdown(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("NbBikeTotalBreakdown");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNbCarTotalBreakdown()throws ParseException, NoSuchAlgorithmException, IOException{
        int result=0;
        JsonObject inputJson = verifJson(BreakdownDAO.getNbCarTotalBreakdown(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("NbCarTotalBreakdown");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNbBikeBreakdownThisMonth()throws ParseException, NoSuchAlgorithmException, IOException{
        int result=0;
        JsonObject inputJson = verifJson(BreakdownDAO.getNbBikeBreakdownThisMonth(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("NbBikeBreakdownMonth");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNbCarBreakdownThisMonth()throws ParseException, NoSuchAlgorithmException, IOException{
        int result=0;
        JsonObject inputJson = verifJson(BreakdownDAO.getNbCarBreakdownThisMonth(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("NbCarBreakdownMonth");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNbBikeBreakdownThisYear()throws ParseException, NoSuchAlgorithmException, IOException{
        int result=0;
        JsonObject inputJson = verifJson(BreakdownDAO.getNbBikeBreakdownThisYear(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("NbBikeBreakdownYear");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNbCarBreakdownThisYear()throws ParseException, NoSuchAlgorithmException, IOException{
        int result=0;
        JsonObject inputJson = verifJson(BreakdownDAO.getNbCarBreakdownThisYear(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("NbCarBreakdownYear");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getAvgCarBreakdownByMonth()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(BreakdownDAO.getAvgCarBreakdownByMonth(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("AvgCarBreakdownThisMonth");
                result = jsonArray.getJsonNumber(0).doubleValue();
                
                
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getAvgCarBreakdownByYear()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(BreakdownDAO.getAvgCarBreakdownByYear(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("AvgCarBreakdownThisYear");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getAvgBikeBreakdownByMonth()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(BreakdownDAO.getAvgBikeBreakdownByMonth(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("AvgBikeBreakdownThisMonth");
                result = jsonArray.getJsonNumber(0).doubleValue();
                
                
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getAvgBikeBreakdownByYear()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(BreakdownDAO.getAvgBikeBreakdownByYear(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("AvgBikeBreakdownThisYear");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public List<String[]> getListBreakdownCar()throws ParseException, NoSuchAlgorithmException, IOException{
        List<String[]> result = new ArrayList<String[]>();
        JsonObject inputJson = verifJson(BreakdownDAO.getListBreakdownCarStat(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetListBreakdownCar");
                int i =0;
                while(i<jsonArray.size()){
                    JsonArray recup = jsonArray.getJsonArray(i);
                    String[] arrayRecup = {recup.getString(0),recup.getString(1)};
                    result.add(arrayRecup);
                    i++;
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public List<String[]> getListBreakdownBike()throws ParseException, NoSuchAlgorithmException, IOException{
        List<String[]> result = new ArrayList<String[]>();
        JsonObject inputJson = verifJson(BreakdownDAO.getListBreakdownBikeStat(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetListBreakdownBike");
                int i =0;
                while(i<jsonArray.size()){
                    JsonArray recup = jsonArray.getJsonArray(i);
                    String[] arrayRecup = {recup.getString(0),recup.getString(1)};
                    result.add(arrayRecup);
                    i++;
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getPriceTotalBreakdownCar()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getPriceTotalBreakdownCar(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetPriceTotalBreakdownCar");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    public double getPriceTotalBreakdownBike()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getPriceTotalBreakdownBike(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetPriceTotalBreakdownBike");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getPriceThisMonthBreakdownCar()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getPriceThisMonthBreakdownCar(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetPriceThisMonthBreakdownCar");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    public double getPriceThisMonthBreakdownBike()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getPriceThisMonthBreakdownBike(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetPriceThisMonthBreakdownBike");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    public double getPriceThisYearBreakdownCar()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getPriceThisYearBreakdownCar(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetPriceThisYearBreakdownCar");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    public double getPriceThisYearBreakdownBike()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getPriceThisYearBreakdownBike(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetPriceThisYearBreakdownBike");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getAvgPriceYearBreakdownBike()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getAvgPriceYearBreakdownBike(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetAvgPriceYearBreakdownBike");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getAvgPriceYearBreakdownCar()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getAvgPriceYearBreakdownCar(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetAvgPriceYearBreakdownCar");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getAvgPriceMonthBreakdownBike()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getAvgPriceMonthBreakdownBike(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetAvgPriceMonthBreakdownBike");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getAvgPriceMonthBreakdownCar()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getAvgPriceMonthBreakdownCar(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetAvgPriceMonthBreakdownCar");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    
}
