/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.client.controller;

import core.client.model.User;
import core.client.view.DepotStatWindow;
import core.client.view.StatisticMenuWindow;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

/**
 *
 * @author pds_user
 */
public class ControllerAdministratorMenuWindow extends Controller {
    private String classView = null; 
    private User userLogged = null;
    
    public ControllerAdministratorMenuWindow (String classView, User userLogged){
        this.classView = classView;
        this.userLogged = userLogged;
        System.out.println(userLogged.getLogin());
    }
    
    public String openStatMenuWindow()throws IOException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException, IllegalArgumentException, IllegalAccessException, NoSuchAlgorithmException{
        String answer = "";
        new StatisticMenuWindow(this.userLogged);
        return answer;
    }
    
    public void disconnectUser() throws IOException{
        super.disconnectUser(userLogged, classView);
    }
}
