/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.client.controller;

import core.client.controller.Controller;
import core.client.model.BreakdownDAO;
import core.client.model.User;
import core.client.model.UserDAO;
import core.client.view.StatisticMenuWindow;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 *
 * @author pds_user
 */
public class ControllerReparatorStatWindow extends Controller {
    private String classView = null; 
    private User userLogged = null;
    
    public ControllerReparatorStatWindow(String classView, User userLogged){
        this.userLogged = userLogged;
        this.classView = classView;
    }

    public String openStatisticMenuWindow()throws IOException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException, IllegalArgumentException, IllegalAccessException, NoSuchAlgorithmException{
        String answer = "";
        new StatisticMenuWindow(userLogged);
        return answer;
    }
    
    public List<User> getAllReparator()throws ParseException, NoSuchAlgorithmException, IOException{
        List <User> result= new ArrayList<User>();
        JsonObject inputJson = verifJson(UserDAO.getAllReparator(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("AllReparator");
                for(int i = 0; i < jsonArray.size(); i++){
                    JsonObject jsonRepForm = jsonArray.getJsonObject(i);
                    User user = User.deserialize(jsonRepForm);
                    result.add(user);
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNumberReparationToday(int id)throws ParseException, NoSuchAlgorithmException, IOException{
        int result= 0;
        JsonObject inputJson = verifJson(UserDAO.getNumberReparationToday(userLogged, classView,id));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetNumberReparationToday");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNumberReparationTotal(int id)throws ParseException, NoSuchAlgorithmException, IOException{
        int result= 0;
        JsonObject inputJson = verifJson(UserDAO.getNumberReparationTotal(userLogged, classView,id));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetNumberReparationTotal");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNumberReparationThisYear(int id)throws ParseException, NoSuchAlgorithmException, IOException{
        int result= 0;
        JsonObject inputJson = verifJson(UserDAO.getNumberReparationThisYear(userLogged, classView,id));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetNumberReparationThisYear");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNumberReparationThisMonth(int id)throws ParseException, NoSuchAlgorithmException, IOException{
        int result= 0;
        JsonObject inputJson = verifJson(UserDAO.getNumberReparationThisMonth(userLogged, classView,id));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetNumberReparationThisMonth");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
}
