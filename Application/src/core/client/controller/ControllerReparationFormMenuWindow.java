package core.client.controller;

import core.client.model.ReparationFormDAO;
import core.client.model.ReparationForm;
import core.client.model.User;
import core.client.view.ConnectWindow;
import core.client.view.ReparationFormDetailWindow;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 * class controller reparation form menu window
 * @author brice.boutamdja
 */
public class ControllerReparationFormMenuWindow extends Controller{
    
    private String classView;
    private User userLogged;
    
    /**
     * Controller
     * @param classView String
     * @param userLogged User
     */
    public ControllerReparationFormMenuWindow(String classView,User userLogged){
        this.classView = classView;
        this.userLogged = userLogged;
    }
    
    /**
     * This method receive and send list of reparation form
     * @return List of reparation form
     * @throws IOException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws ParseException 
     */
    public List<ReparationForm> getListReparationForm() throws IOException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException{
        List<ReparationForm> lRepForm= new ArrayList<ReparationForm>();
        JsonObject inputJson = verifJson(ReparationFormDAO.getReparationFormAll(classView, userLogged));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success" :
                JsonArray JArray = inputJson.getJsonArray("ListReparationForm");
                for(int i = 0; i < JArray.size(); i++){
                    JsonObject jsonRepForm = JArray.getJsonObject(i);
                    ReparationForm rf = ReparationForm.deserialize(jsonRepForm.getJsonObject("ReparationForm_"+i));
                    lRepForm.add(rf);
                }
                break;
            case "Error" :
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        return lRepForm;
    }
    
    public List<ReparationForm> getListReparationFormWorkOn() throws IOException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException{
        List<ReparationForm> lRepForm= new ArrayList<ReparationForm>();
        JsonObject inputJson = verifJson(ReparationFormDAO.giveAllReparationFormWorkOn(classView, userLogged));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success" :
                JsonArray JArray = inputJson.getJsonArray("ListReparationFormWorkOn");
                for(int i = 0; i < JArray.size(); i++){
                    JsonObject jsonRepForm = JArray.getJsonObject(i);
                    ReparationForm rf = ReparationForm.deserialize(jsonRepForm.getJsonObject("ReparationForm_"+i));
                    lRepForm.add(rf);
                }
                break;
            case "Error" :
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        return lRepForm;
    }
    
    /**
     * This method open reparation form detail window
     * @param lRepForm list of reparation form
     * @param userLogged user
     * @param row integer
     */
    public void workOnReparationForm(List<ReparationForm> lRepForm, User userLogged, int idForm) throws ParseException, IOException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, NoSuchAlgorithmException {
        int i = 0;
        while(i < lRepForm.size() && lRepForm.get(i).getId() != idForm){
            i++;
        }
        if(i < lRepForm.size()){
            if(workOnUpdate(idForm, userLogged)){
                ReparationForm repForm = lRepForm.get(i);
                repForm.setDescriptionCardState("In reparation");
                new ReparationFormDetailWindow(repForm, userLogged);
            }
        }else{
            errorMessage = "No reparation form found in the list with this id";
        }
    }
    
    private boolean workOnUpdate(int idForm, User userLogged) throws ParseException, IOException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, NoSuchAlgorithmException{
        boolean result = false;
        JsonObject inputJson = verifJson(ReparationFormDAO.workOnUpdate(classView, userLogged, idForm));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success" :
                result = true;
                break;
            case "Error" :
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        return result;
    }
    
    public void openReparationForm(List<ReparationForm> lRepForm, User userLogged, int idForm) throws ParseException, IOException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, NoSuchAlgorithmException {
        int i = 0;
        while(i < lRepForm.size() && lRepForm.get(i).getId() != idForm){
            i++;
        }
        if(i < lRepForm.size()){
                ReparationForm repForm = lRepForm.get(i);
                new ReparationFormDetailWindow(repForm, userLogged);
        }else{
            errorMessage = "No reparation form found in the list with this id";
        }
    }
}
