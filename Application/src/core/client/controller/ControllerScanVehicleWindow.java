package core.client.controller;

import core.client.model.Parking;
import core.client.model.ReparationForm;
import core.client.model.ReparationFormDAO;
import core.client.model.User;
import core.client.view.ConnectWindow;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 *
 * @author charles_santerre
 */
public class ControllerScanVehicleWindow extends Controller {

    private String classView;
    private ArrayList<Parking> listParking;

    public ControllerScanVehicleWindow(String classView) {
        this.classView = classView;
        this.listParking = new ArrayList<Parking>();
    }

    /**
     * This method create a reparationForm and update listParking
     * @param tag
     * @param userLogged
     * @param parking
     * @return
     * @throws IOException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws ParseException 
     */
    public String createReparationForm(String tag, User userLogged, Parking parking) throws IOException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException {
        String answer = "";
        ArrayList<Parking> listParkingAvailable = new ArrayList<Parking>();
        if (tag.equals("")) {
            answer = "empty";
        } else if (tag.length() != 8 && tag.length() != 12) {
            errorMessage = "Wrong size";
            answer = "wrong size";
        } else if ((tag.length() == 8 && parking.getBikePlace() == false) || (tag.length() == 12 && parking.getBikePlace() == true)){
            JsonObject inputJson = verifJson(ReparationFormDAO.createReparationForm(tag, classView, userLogged, parking.getId()));
            switch (inputJson.getString("State")) {
                case "Success":
                    JsonArray JArray = inputJson.getJsonArray("Parking_list");
                    for (int i = 0; i < JArray.size(); i++) {
                        JsonObject jsonParking = JArray.getJsonObject(i);
                        Parking park = Parking.deserialize(jsonParking.getJsonObject("Parking_" + i));
                        listParkingAvailable.add(park);
                    }
                    listParking = listParkingAvailable;
                    answer = "success";
                    break;
                case "Error":
                    answer = "wrong";
                    errorMessage = inputJson.getString("ErrorMessage");
                    break;
                case "Cancelled_Reconnection":
                    answer = "close";
                    new ConnectWindow();
                    break;
                case "Reconnection_Success":
                    // Do something with Userlogged to update the user after reconnection
                    break;
            }
        }
        return answer;
    }
    
    public ArrayList<Parking> getListParkingAvailable(){
        return listParking;
    }
    
    public ReparationForm getLastReparationForm(User userLogged) throws IOException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException{
        ReparationForm repForm = null;
        JsonObject inputJson = verifJson(ReparationFormDAO.getLastReparationForm(classView, userLogged));
        String stateCom = inputJson.getString("State");
        System.out.println(stateCom);
        switch (stateCom){
            case "Success" :
                JsonArray JArray = inputJson.getJsonArray("LastReparationForm");
                for(int i = 0; i < JArray.size(); i++){
                    JsonObject jsonRepForm = JArray.getJsonObject(i);
                    repForm = ReparationForm.deserialize(jsonRepForm.getJsonObject("ReparationForm"));
                }
                break;
            case "Error" :
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        return repForm;
    }
}
