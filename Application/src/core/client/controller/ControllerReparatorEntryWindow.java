/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.client.controller;

import core.client.model.Breakdown;
import core.client.model.BreakdownDAO;
import core.client.model.CardStateDAO;
import core.client.model.Comment;
import core.client.model.CommentDAO;
import core.client.model.Part;
import core.client.model.PartDAO;
import core.client.model.ReparationForm;
import core.client.model.ReparationFormDAO;
import core.client.model.User;
import core.client.model.VehicleDAO;
import core.client.view.ConnectWindow;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 *
 * @author pds_user
 */
public class ControllerReparatorEntryWindow extends Controller {
    private String classView = null; 
    private User userLogged = null;
    
    public ControllerReparatorEntryWindow(User userLogged, String classView){
        this.userLogged = userLogged;
        this.classView = classView;
    }
    
    /**
     * Get the list of all card state from the server and return it
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<String> getListCardState() throws ParseException, NoSuchAlgorithmException, IOException{
        List<String> result = new ArrayList<String>();
        JsonObject inputJson = verifJson(CardStateDAO.getAllCardState(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("List");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(jsonArray.getString(i));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        return result;
    }
        
   /**
     * Send the idPart and idVehicle to the server to add a part to the specified vehicle in the database
     * @param idCarModel
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<Breakdown> getListHaveBreakdownCar(int idCarModel, int idCar) throws ParseException, NoSuchAlgorithmException, IOException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        
        JsonObject inputJson = verifJson(BreakdownDAO.getListBreakdownCarModel(idCarModel, idCar, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("BreakdownList");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(Breakdown.deserialize(jsonArray.getJsonObject(i)));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
    /**
     * Get the list of breakdown that a bike can have from the server
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<Breakdown> getListHaveBreakdownBike(int idBike) throws ParseException, NoSuchAlgorithmException, IOException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        
        JsonObject inputJson = verifJson(BreakdownDAO.getListBreakdownBike(idBike, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("BreakdownList");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(Breakdown.deserialize(jsonArray.getJsonObject(i)));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
    /**
     * Send the idVehicle and the idBreakdown to the server to add the breakdown to the vehicle in the database
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<Breakdown> addBreakdown(int idVehicle, int idBreakdown, String table) throws ParseException, NoSuchAlgorithmException, IOException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        
        JsonObject inputJson = verifJson(VehicleDAO.addBreakdown(idVehicle, idBreakdown, table, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("BreakdownList");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(Breakdown.deserialize(jsonArray.getJsonObject(i)));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
    /**
     * Send the idVehicle and the idBreakdown to the server to delete the breakdown from the vehicle in the database
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @return
     * @throws ParseException
     * @throws NoSuchAlgorithmException
     * @throws IOException 
     */
    public List<Breakdown> deleteBreakdown(int idVehicle, int idBreakdown, String table) throws ParseException, NoSuchAlgorithmException, IOException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        
        JsonObject inputJson = verifJson(VehicleDAO.deleteBreakdown(idVehicle, idBreakdown, table, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("BreakdownList");
                for(int i = 0; i < jsonArray.size(); i++){
                    result.add(Breakdown.deserialize(jsonArray.getJsonObject(i)));
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
    /**
     * Send the ReparationForm to the server to update its attributes in the database
     * @param repForm
     * @return
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws UnsupportedEncodingException
     * @throws ParseException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException 
     */
    public String updateReparationFormDetails(ReparationForm repForm) throws NoSuchAlgorithmException, IOException, UnsupportedEncodingException, ParseException, IllegalAccessException, NoSuchMethodException, InvocationTargetException{
        String result = "";
        
        JsonObject inputJson = verifJson(ReparationFormDAO.updateReparatorDetails(repForm, userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                result = "success";
                break;
            case "Error":
                result = "error";
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                result = null;
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        
        return result;
    }
    
}
