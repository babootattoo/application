package core.client.controller;

import core.client.model.Bike;
import core.client.model.Breakdown;
import core.client.model.Car;
import core.client.model.CarModel;
import core.client.model.DateFormatted;
import core.client.model.NFCCase;
import core.client.model.Part;
import core.client.model.User;
import core.client.model.VehicleDAO;
import core.client.view.ConnectWindow;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonObject;

/**
 *
 * @author pierre.TokerKovacic
 */
public class ControllerVehicleDetails extends Controller {
 
    private String classView; 
    private User userLogged;
    private int idVehicle;
    private Car car;
    
    public ControllerVehicleDetails(String classView, User userLogged){
        this.classView = classView;
        this.userLogged = userLogged;
    }
    
    /**
     * This method receive and send car details
     * @param idVehicle id of the vehicle
     * @return List of reparation form
     * @throws IOException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws ParseException 
     */
    public Object getDetails(int idVehicle, String typeVehicle) throws IOException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException{
        Object vehicle = null;
        String stateCom = null, table = null;
        JsonObject inputJson = null;
        List<Part> parts = new ArrayList<Part>();
        List<Breakdown> breakdowns = new ArrayList<Breakdown>();
        switch (typeVehicle){
            case "Bike ID":
                table = "Bike";
                inputJson = verifJson(VehicleDAO.getDetailsByID(idVehicle, table, userLogged, classView));
                stateCom = inputJson.getString("State");
                switch (stateCom){
                    case "Success" :
                        vehicle = Bike.deserialize(inputJson.getJsonObject("Bike"));
                        break;
                    case "Error" :
                        errorMessage = inputJson.getString("ErrorMessage");
                        break;
                    case "Cancelled_Reconnection":
                        new ConnectWindow();
                        break;
                }
                break;
            case "Car ID":
                table = "Car";
                inputJson = verifJson(VehicleDAO.getDetailsByID(idVehicle, table, userLogged, classView));
                stateCom = inputJson.getString("State");
                switch (stateCom){
                    case "Success" :
                        vehicle = Car.deserialize(inputJson.getJsonObject("Car"));
                        break;
                    case "Error" :
                        errorMessage = inputJson.getString("ErrorMessage");
                        break;
                    case "Cancelled_Reconnection":
                        new ConnectWindow();
                        break;
                }
                break;
        }
        return vehicle;
    }
    
}
