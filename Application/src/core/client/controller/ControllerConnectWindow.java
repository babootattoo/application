package core.client.controller;

import core.client.model.User;
import core.client.model.UserDAO;
import core.client.view.AdministratorMenuWindow;
import core.client.view.ReparatorMenuWindow;
import java.io.IOException;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import javax.json.JsonObject;

/**
 *
 * @author charles.santerre
 */
public class ControllerConnectWindow extends Controller{

    public ControllerConnectWindow() {
    }

    /**
     * This method load adapted view witch depend of type user in JsonObject
     * @param login
     * @param password
     * @return String of result state in Json
     * @throws UnknownHostException
     * @throws IOException
     * @throws ParseException 
     */
    public String openMenuWindow(String login, String password, String classView) throws UnknownHostException, IOException, ParseException, NoSuchAlgorithmException {
        String answer = "";
        if (login.equals("") || password.equals("")) {
            answer = "empty";
        } else {
            String cryptedPassword = User.cryptPassword(password);
            JsonObject inputJson = UserDAO.tryLogin(login, cryptedPassword, classView);
            String stateCom = inputJson.getString("State");

            switch (stateCom) {
                case "Success":
                    answer = "success";
                    User userLogged = User.deserialize(inputJson.getJsonObject("User"));
                    switch (userLogged.getTypeUser()) {
                        case "Repairer":
                            new ReparatorMenuWindow(userLogged);
                            break;
                        case "Administrative":
                            new AdministratorMenuWindow(userLogged);
                            break;
                        case "Maintenance Technician":
                            //new technicianMenuWindow();
                            break;
                    }
                    break;
                case "Error":
                    answer = "wrong";
                    errorMessage = inputJson.getString("ErrorMessage");
                    break;
            }
        }
        return answer;
    }
}
