/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.client.controller;

import core.client.model.BreakdownDAO;
import core.client.model.ParkingDAO;
import core.client.model.PartDAO;
import core.client.model.ReparationFormDAO;
import core.client.model.User;
import core.client.model.UserDAO;
import core.client.view.StatisticMenuWindow;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import javax.json.JsonArray;

import javax.json.JsonObject;

/**
 *
 * @author Quentin Goutte
 */
public class ControllerDepotStatWindow extends Controller {
    private String classView = null; 
    private User userLogged = null;
    
    public ControllerDepotStatWindow(String classView, User userLogged){
        this.userLogged = userLogged;
        this.classView = classView;
    }
    
    public int getNumberEmployee() throws ParseException, NoSuchAlgorithmException, IOException{
        int result=0;
        JsonObject inputJson = verifJson(UserDAO.getNumberEmployee(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("NbEmployee");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNumberVehicle() throws ParseException, NoSuchAlgorithmException, IOException{
        int result=0;
        JsonObject inputJson = verifJson(UserDAO.getNumberVehicle(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("NbVehicle");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNumberVehicleInDepot() throws ParseException, NoSuchAlgorithmException, IOException{
        int result=0;
        JsonObject inputJson = verifJson(ParkingDAO.getNbVehicleInDepot(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("NbVehicleInDepot");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNumberBreakdownThisYear()throws ParseException, NoSuchAlgorithmException, IOException{
        int result=0;
        JsonObject inputJson = verifJson(BreakdownDAO.getNbBreakdownThisYear(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("NbBreakdownThisYear");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNumberBreakdownThisMonth()throws ParseException, NoSuchAlgorithmException, IOException{
        int result=0;
        JsonObject inputJson = verifJson(BreakdownDAO.getNbBreakdownThisMonth(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("NbBreakdownThisMonth");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getAvgBreakdownByMonth()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(BreakdownDAO.getAvgBreakdownByMonth(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetAvgBreakdownByMonth");
                result = jsonArray.getJsonNumber(0).doubleValue();
                
                
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getAvgBreakdownByYear()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(BreakdownDAO.getAvgBreakdownByYear(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetAvgBreakdownByYear");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNumberBuyingPiecesThisYear()throws ParseException, NoSuchAlgorithmException, IOException{
        int result=0;
        JsonObject inputJson = verifJson(PartDAO.getNumberBuyingPiecesThisYear(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetNumberBuyingPiecesThisYear");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public int getNumberBuyingPiecesThisMonth()throws ParseException, NoSuchAlgorithmException, IOException{
        int result=0;
        JsonObject inputJson = verifJson(PartDAO.getNumberBuyingPiecesThisMonth(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetNumberBuyingPiecesThisMonth");
                result = jsonArray.getInt(0);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getAvgPiecesByMonth()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getAvgPiecesByMonth(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetAvgPiecesByMonth");
                result = jsonArray.getJsonNumber(0).doubleValue();
                
                
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getAvgPiecesByYear()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getAvgPiecesByYear(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetAvgPiecesByYear");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getAvgDuringBreakdown()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(ReparationFormDAO.getAvgDuringBreakdown(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetAvgDuringBreakdown");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getPriceThisYear()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getPriceThisYear(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetPriceThisYear");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getPriceThisMonth()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getPriceThisMonth(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetPriceThisMonth");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
         
    public double getAvgPriceByMonth()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getAvgPriceByMonth(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetAvgPriceByMonth");
                result = jsonArray.getJsonNumber(0).doubleValue();
                
                
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public double getAvgPriceByYear()throws ParseException, NoSuchAlgorithmException, IOException{
        double result=0;
        JsonObject inputJson = verifJson(PartDAO.getAvgPriceByYear(userLogged, classView));
        String stateCom = inputJson.getString("State");
        switch (stateCom){
            case "Success":
                JsonArray jsonArray = inputJson.getJsonArray("GetAvgPriceByYear");
                result = jsonArray.getJsonNumber(0).doubleValue();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return result;
    }
    
    public String openStatisticMenuWindow()throws IOException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException, IllegalArgumentException, IllegalAccessException, NoSuchAlgorithmException{
        String answer = "";
        new StatisticMenuWindow(userLogged);
        return answer;
    }

}
