package core.server.network;

import core.server.model.ViewModel;
import database.connection.pool.Datasource;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * Server class contains the ServerSocket
 * It extends Thread to not freeze the main thread during a loop in the Server class
 * @author Stephane.Schenkel
 */
public class Server extends Thread{
    
    public static ConnectedUsers cListUsers;
    
    private ServerSocket sc;
    private final ServConf conf = new ServConf();
    
    /**
     * Initialize the serversocket object with the port of the configuration class
     * Initialize the connection pool
     * @throws Exception 
     */
    public Server() throws Exception{
        sc = new ServerSocket(conf.PORT);
        Datasource.initializeDataSource();
        cListUsers = new ConnectedUsers();
        ViewModel.println("Listening to port " + conf.PORT + "\n");
    }
    
    /**
     * Contains a loop true, it opens a new Socket between a client and the server
     * each time a client wants to reach the server.
     * The socket is sent to the class SocketHandler, 
     * The new object SocketHandler is a Thread to be able to have multiple client
     * to communicate with the server at the same time.
     */
    @Override
    public void run(){
        try{
            while(true){
                Socket s = sc.accept();
                
                new Thread(new SocketHandler(s)).start();
            }
        }
        catch(SocketException e){
            Thread.currentThread().interrupt();
        }
        catch(IOException e){
            ViewModel.printError(e);
        }
    }
    
    /**
     * Show the list of active users on the server
     */
    public void listActiveUsers(){
        cListUsers.showList();
    }
    
    /**
     * Closes the serversocket (it closes all opened client socket too)
     * Closes all the connection of the connection pool and purge the connection pool
     */
    public void closeServer(){
        try{
            ViewModel.println("Closing server socket...");
            sc.close();
            if(sc.isClosed()){
                ViewModel.println("Server socket is closed");
            }
            Datasource.closeConnectionPool();
        }catch(IOException e){
            ViewModel.printError(e);
        }
    }
}
