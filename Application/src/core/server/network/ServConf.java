package core.server.network;

/**
 * Configuration of the server, contains all parameters of the server
 * @author Stephane.Schenkel
 */
public class ServConf {
    
    /**
     * Port where the server is listening and waiting for data
     */
    public int PORT;

    public ServConf(){
            init();
    }

    private void init(){
        PORT = 10080;
    }
    
}
