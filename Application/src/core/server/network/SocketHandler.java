package core.server.network;

import core.server.model.ViewModel;
import data.processing.ProcessData;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import javax.json.JsonObject;

/**
 * SocketHandler class implements Runnable to be able to be a Thread
 * This class will send and receive data to the attribute socket client.
 * @author Stephane.Schenkel
 */
public class SocketHandler implements Runnable {
    
    Socket soc;
    String ipClient;
    
    /**
     * Set the socket attribute with the opened socket to the client
     * Set the ipClient attribute with the ip of the client of the opened socket
     * @param s Opened socket between a client and the server
     */
    public SocketHandler(Socket s){
        soc = s;
        ipClient = soc.getInetAddress().getHostAddress().toString();
        ViewModel.println("(" + ipClient + ") Open connection");
    }
    
    /**
     * Contains the InputStream and the OutputStream.
     * A loop while(true) where it waits to receive data from the client with (in.readLine())
     * If the input is "END" it quits the loop, if it's an other input, it sends the input string
     * To the processData class
     */
    public void run(){
        try{
            BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
            PrintWriter out = new PrintWriter(new DataOutputStream(soc.getOutputStream()), true);

            while(true){
                String inputLine = in.readLine();
                ViewModel.println("(" + ipClient + ") Receiving data : " + inputLine);
                if(!inputLine.equals("END")){
                    ProcessData pd = new ProcessData(inputLine, ipClient);
                    JsonObject outputJson = pd.getResult();
                    if(outputJson != null){
                        out.println(outputJson.toString());
                        ViewModel.println("(" + ipClient + ") Sending data : " + outputJson.toString());
                    }
                }else{
                    ViewModel.println("(" + ipClient + ") Closing connection");
                    break;
                }
            }
            
            out.close();
            in.close();
            soc.close();
        }catch(Exception e){
            ViewModel.printError(e);
        }
    }
}
