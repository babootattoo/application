package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Stephane.Schenkel
 */
public class PartDAO {
    
    /**
     * Get all the part used by the specified vehicle
     * @param idVehicle
     * @param table
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static List<Part> getUsedListByVehicleId(int idVehicle, String table) throws ClassNotFoundException, SQLException, InterruptedException{
        List<Part> result = new ArrayList<Part>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT p.id as id, stock, description, purchaseprice, installationdate, numberUse FROM part p JOIN usepart"+ table +" upc on p.id = upc.idpart WHERE id"+table+" = ?");
        stat.setInt(1, idVehicle);
        ResultSet rs = stat.executeQuery();
        while(rs.next()){
            result.add(new Part(rs.getInt("id"), rs.getInt("stock"), rs.getString("description"), rs.getDouble("purchaseprice"), rs.getInt("numberUse"), new DateFormatted(rs.getDate("installationdate"), false)));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Get all the available part for bikes
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static List<Part> getAvailablePartBike() throws SQLException, ClassNotFoundException, InterruptedException{
        List<Part> result = new ArrayList<Part>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT id, stock, description, purchaseprice FROM part WHERE bikepart = true ORDER BY id ASC");
        ResultSet rs = stat.executeQuery();
        
        Date datenull = null;
        while(rs.next()){
            result.add(new Part(rs.getInt("id"), rs.getInt("stock"), rs.getString("description"), rs.getDouble("purchaseprice"), 0, new DateFormatted(datenull, false)));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Get all the available part for the specified carmodel
     * @param idModel
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static List<Part> getAvailablePartModel(int idModel) throws SQLException, ClassNotFoundException, InterruptedException{
        List<Part> result = new ArrayList<Part>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT p.id as id, stock, description, purchaseprice FROM part p JOIN canusepartcarmodel cp ON p.id = cp.idpart WHERE idcarmodel = ? ORDER BY p.id ASC");
        stat.setInt(1, idModel);
        ResultSet rs = stat.executeQuery();
        
        Date datenull = null;
        while(rs.next()){
            result.add(new Part(rs.getInt("id"), rs.getInt("stock"), rs.getString("description"), rs.getDouble("purchaseprice"), 0, new DateFormatted(datenull, false)));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    public static int decrementStockByPart(int idPart) throws ClassNotFoundException, SQLException, InterruptedException{
        int result = 0;
        
        Connection co = Datasource.getConnection();
        PreparedStatement statUpdateStock = co.prepareStatement("UPDATE part SET stock = stock - 1 WHERE id = ?");
        statUpdateStock.setInt(1, idPart);

        result = statUpdateStock.executeUpdate();

        statUpdateStock.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    public static int incrementStockByPart(int idPart) throws SQLException, ClassNotFoundException, InterruptedException{
        int result = 0;
        
        Connection co = Datasource.getConnection();
        PreparedStatement statUpdateStock = co.prepareStatement("UPDATE part SET stock = stock + 1 WHERE id = ?");
        statUpdateStock.setInt(1, idPart);

        result = statUpdateStock.executeUpdate();

        statUpdateStock.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    public static int getNumberPiecesBuyingThisMonth() throws SQLException, ClassNotFoundException, InterruptedException{
        int result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT sum(quantity) as number FROM orderConcernedPart,orderPart WHERE orderConcernedPart.idorderpart=orderpart.id AND extract(month from orderdate) = extract(month from current_date)");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getInt("number");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    public static int getNumberPiecesBuyingThisYear() throws SQLException, ClassNotFoundException, InterruptedException{
        int result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT sum(quantity) as number FROM orderConcernedPart,orderPart WHERE orderConcernedPart.idorderpart=orderpart.id AND extract(year from orderdate) = extract(year from current_date)");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getInt("number");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    public static double getAvgPiecesByMonth() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg (cnt.number) as moyenne FROM (SELECT sum(quantity) as number FROM orderConcernedPart,orderPart WHERE orderConcernedPart.idorderpart=orderpart.id GROUP BY extract(month from orderdate)) cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();

        Datasource.returnConnection(co);
        return result; 
    }
    
    public static double getAvgPiecesByYear() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg (cnt.number) as moyenne FROM (SELECT sum(quantity) as number FROM orderConcernedPart,orderPart WHERE orderConcernedPart.idorderpart=orderpart.id GROUP BY extract(year from orderdate)) cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();

        Datasource.returnConnection(co);
        return result; 
    }
    
    public static double getPriceThisYear() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT SUM(purchaseprice) as number FROM part,usePartCar WHERE extract(year from usePartCar.installationdate) = extract(year from current_date) AND part.id=usePartCar.idPart");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("number");
        }
        rs.close();
        stat.close();
        
        PreparedStatement stat2= co.prepareStatement("SELECT SUM(purchaseprice) as number FROM part,usePartBike WHERE extract(year from usePartBike.installationdate) = extract(year from current_date) AND part.id=usePartBike.idPart");
        ResultSet rs2 = stat2.executeQuery();
        while (rs2.next()){
            result = result+rs2.getDouble("number");
        }
        rs2.close();
        stat2.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    public static double getPriceThisMonth() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT SUM(purchaseprice) as number FROM part,usePartCar WHERE extract(month from usePartCar.installationdate) = extract(month from current_date) AND part.id=usePartCar.idPart");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("number");
        }
        rs.close();
        stat.close();
        
        PreparedStatement stat2= co.prepareStatement("SELECT SUM(purchaseprice) as number FROM part,usePartBike WHERE extract(month from usePartBike.installationdate) = extract(month from current_date) AND part.id=usePartBike.idPart");
        ResultSet rs2 = stat2.executeQuery();
        while (rs2.next()){
            result = result+rs2.getDouble("number");
        }
        rs2.close();
        stat2.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    public static double getAvgPriceByYear() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg (cnt.number) as moyenne FROM (SELECT SUM(purchaseprice) as number FROM part,usePartCar WHERE part.id=usePartCar.idPart GROUP BY extract(year from installationdate)) cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();
        
        PreparedStatement stat2= co.prepareStatement("SELECT avg (cnt.number) as moyenne FROM (SELECT SUM(purchaseprice) as number FROM part,usePartBike WHERE part.id=usePartBike.idPart GROUP BY extract(year from installationdate)) cnt");
        ResultSet rs2 = stat2.executeQuery();
        while (rs2.next()){
            result = result+rs2.getDouble("moyenne");
        }
        rs2.close();
        stat2.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    public static double getAvgPriceByMonth() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg (cnt.number) as moyenne FROM (SELECT SUM(purchaseprice) as number FROM part,usePartCar WHERE part.id=usePartCar.idPart GROUP BY extract(month from installationdate)) cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();
        
        PreparedStatement stat2= co.prepareStatement("SELECT avg (cnt.number) as moyenne FROM (SELECT SUM(purchaseprice) as number FROM part,usePartBike WHERE part.id=usePartBike.idPart GROUP BY extract(month from installationdate)) cnt");
        ResultSet rs2 = stat2.executeQuery();
        while (rs2.next()){
            result = result+rs2.getDouble("moyenne");
        }
        rs2.close();
        stat2.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    public static double getPriceThisMonthBreakdownCar() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT SUM(purchaseprice) as number FROM part,usePartCar WHERE extract(month from usePartCar.installationdate) = extract(month from current_date) AND part.id=usePartCar.idPart");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("number");
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;  
    }
    
    public static double getPriceThisMonthBreakdownBike() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT SUM(purchaseprice) as number FROM part,usePartBike WHERE extract(month from usePartBike.installationdate) = extract(month from current_date) AND part.id=usePartBike.idPart");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("number");
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;  
    }
    
    public static double getPriceThisYearBreakdownCar() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT SUM(purchaseprice) as number FROM part,usePartCar WHERE extract(year from usePartCar.installationdate) = extract(year from current_date) AND part.id=usePartCar.idPart");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("number");
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;  
    }
    
    public static double getPriceThisYearBreakdownBike() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT SUM(purchaseprice) as number FROM part,usePartBike WHERE extract(year from usePartBike.installationdate) = extract(year from current_date) AND part.id=usePartBike.idPart");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("number");
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;  
    }
    
    public static double getPriceTotalBreakdownCar() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT SUM(purchaseprice) as number FROM part,usePartCar WHERE part.id=usePartCar.idPart");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("number");
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;  
    }
    
    public static double getPriceTotalBreakdownBike() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT SUM(purchaseprice) as number FROM part,usePartBike WHERE part.id=usePartBike.idPart");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("number");
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;  
    }
    
    public static double getAvgPriceYearBreakdownCar() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg (cnt.number) as moyenne FROM (SELECT SUM(purchaseprice) as number FROM part,usePartCar WHERE part.id=usePartCar.idPart GROUP BY extract(year from installationdate)) cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    public static double getAvgPriceMonthBreakdownBike() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg (cnt.number) as moyenne FROM (SELECT SUM(purchaseprice) as number FROM part,usePartBike WHERE part.id=usePartBike.idPart GROUP BY extract(month from installationdate)) cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    public static double getAvgPriceMonthBreakdownCar() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg (cnt.number) as moyenne FROM (SELECT SUM(purchaseprice) as number FROM part,usePartCar WHERE part.id=usePartCar.idPart GROUP BY extract(month from installationdate)) cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    public static double getAvgPriceYearBreakdownBike() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg (cnt.number) as moyenne FROM (SELECT SUM(purchaseprice) as number FROM part,usePartBike WHERE part.id=usePartBike.idPart GROUP BY extract(year from installationdate)) cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
}
