package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * All database methods related to the ReparationForm class
 * @author Stephane.Schenkel, Brice.Boutamdja, Charles.Santerre
 */
public class ReparationFormDAO {
    
    /**
     * Get last ReparationForm from ReparationForm table of the database It
     * creates the Car object or Bike object of the ReparationForm and the
     * @return ReparationForm
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException
     * @throws ParseException 
     */
    public static ReparationForm getLastReparationForm() throws ClassNotFoundException, SQLException, InterruptedException, ParseException {
        Connection co = Datasource.getConnection();
        ReparationForm rf = null;
        PreparedStatement stat = co.prepareStatement("SELECT rf.id as idRepForm, rf.idcar, rf.idbike,  rf.entrydate, rf.outdate, rf.diagnosis, cs.description as descriptionCardState, "
                + "pk.id as idParking, pk.placenumber as placeNumberParking, pk.occupied, pk.bikePlace"
                + " FROM reparationform rf, cardState cs, parking pk"
                + " WHERE rf.idCardState = cs.id AND rf.idparking = pk.id AND rf.id = (select max(id) from reparationform)");

        ResultSet rs = stat.executeQuery();
        Datasource.returnConnection(co);
        while (rs.next()) {
            Car car = CarDAO.getCarById(rs.getInt("idcar"));
            Bike bike = BikeDAO.getBikeById(rs.getInt("idbike"));
            Parking parking = new Parking(rs.getInt("idParking"), rs.getString("placeNumberParking"), rs.getBoolean("occupied"), rs.getBoolean("bikePlace"));
            rf = new ReparationForm(Integer.parseInt(rs.getString("idRepForm")), rs.getString("descriptionCardState"), parking, car, bike, new DateFormatted(rs.getDate("entrydate"), false), new DateFormatted(rs.getDate("outdate"), false), rs.getString("diagnosis"));
        }
        rs.close();
        stat.close();
        return rf;
    }
    
    /**
     * Get all ReparationForm from ReparationForm table of the database It
     * creates the Car object or Bike object of the ReparationForm and the
     * Parking Object
     * @return list of reparation form
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException
     * @throws ParseException 
     */
    public static List<ReparationForm> getReparationFormAll() throws ClassNotFoundException, SQLException, InterruptedException, ParseException {
        Connection co = Datasource.getConnection();
        List<ReparationForm> result = new ArrayList<ReparationForm>();
        PreparedStatement stat = co.prepareStatement("SELECT rf.id as idRepForm, rf.idcar, rf.idbike,  rf.entrydate, rf.outdate, rf.diagnosis, cs.description as descriptionCardState, "
                + "pk.id as idParking, pk.placenumber as placeNumberParking, pk.occupied, pk.bikePlace"
                + " FROM reparationform rf, cardState cs, parking pk"
                + " WHERE rf.idCardState = cs.id AND rf.idparking = pk.id AND cs.description = 'Waiting for reparation'");

        ResultSet rs = stat.executeQuery();
        Datasource.returnConnection(co);
        while (rs.next()) {
            Car car = CarDAO.getCarById(rs.getInt("idcar"));
            Bike bike = BikeDAO.getBikeById(rs.getInt("idbike"));
            Parking parking = new Parking(rs.getInt("idParking"), rs.getString("placeNumberParking"), rs.getBoolean("occupied"), rs.getBoolean("bikePlace"));
            ReparationForm rf = new ReparationForm(Integer.parseInt(rs.getString("idRepForm")), rs.getString("descriptionCardState"), parking, car, bike, new DateFormatted(rs.getDate("entrydate"), false), new DateFormatted(rs.getDate("outdate"), false), rs.getString("diagnosis"));
            rf.updatePriorityLevel();
            result.add(rf);
        }
        rs.close();
        stat.close();
        return result;
    }
    
    public static List<ReparationForm> getReparationFormAllWorkOn(int userId) throws ClassNotFoundException, SQLException, InterruptedException, ParseException {
        Connection co = Datasource.getConnection();
        List<ReparationForm> result = new ArrayList<ReparationForm>();
        PreparedStatement stat = co.prepareStatement("SELECT rf.id as idRepForm, rf.idcar, rf.idbike,  rf.entrydate, rf.outdate, rf.diagnosis, cs.description as descriptionCardState, "
                + "pk.id as idParking, pk.placenumber as placeNumberParking, pk.occupied, pk.bikePlace"
                + " FROM reparationform rf, cardState cs, parking pk, workon wo"
                + " WHERE rf.idCardState = cs.id AND rf.idparking = pk.id AND rf.id = wo.idreparationform AND wo.idemployee = ?");
        stat.setInt(1, userId);
        
        ResultSet rs = stat.executeQuery();
        Datasource.returnConnection(co);
        while (rs.next()) {
            Car car = CarDAO.getCarById(rs.getInt("idcar"));
            Bike bike = BikeDAO.getBikeById(rs.getInt("idbike"));
            Parking parking = new Parking(rs.getInt("idParking"), rs.getString("placeNumberParking"), rs.getBoolean("occupied"), rs.getBoolean("bikePlace"));
            ReparationForm rf = new ReparationForm(Integer.parseInt(rs.getString("idRepForm")), rs.getString("descriptionCardState"), parking, car, bike, new DateFormatted(rs.getDate("entrydate"), false), new DateFormatted(rs.getDate("outdate"), false), rs.getString("diagnosis"));
            rf.updatePriorityLevel();
            result.add(rf);
        }
        rs.close();
        stat.close();
        return result;
    }

    /**
     * delete a repearationForm by idCar from ReparationForm table of the
     * database
     *
     * @param car
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException
     */
    public static void deleteRepForm(Car car) throws ClassNotFoundException, SQLException, InterruptedException {
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("DELETE FROM reparationForm WHERE idCar = " + car.getId());
        stat.executeUpdate();
        stat.close();
        Datasource.returnConnection(co);
    }

    /**
     * Create ReparationForm a new reparationForm with the new car
     *
     * @param tag
     * @param idParking
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    public static int createReparationForm(String tag, int idParking) throws SQLException, ClassNotFoundException, InterruptedException {
        int answerParking = ParkingDAO.setParkingPlaceOccupiedTrue(idParking);
        int answer = 0;
        if (answerParking == 1) {
            Connection co = Datasource.getConnection();
            PreparedStatement stat = null;
            switch (tag.length()) {
                case 8:
                    stat = co.prepareStatement(
                            "INSERT INTO ReparationForm(idCardState, idParking, idCar, idBike, entryDate) VALUES"
                            + " (2,?,"
                            + " (SELECT c.id FROM car c JOIN nfcCase nfc ON nfc.id = c.idnfccase"
                            + " WHERE nfc.tagid = ?),"
                            + " null,(SELECT current_date))");
                    stat.setInt(1, idParking);
                    stat.setString(2, tag);
                    break;
                case 12:
                    stat = co.prepareStatement(
                            "INSERT INTO ReparationForm(idCardState, idParking, idCar, idBike, entryDate) VALUES"
                            + " (2,?,null,"
                            + " (SELECT b.id FROM bike b JOIN rfidChip rfid ON rfid.id = b.idrfidchip"
                            + " WHERE rfid.tagid = ?),"
                            + " (SELECT current_date))");
                    stat.setInt(1, idParking);
                    stat.setString(2, tag);
                    break;
            }
            answer = stat.executeUpdate();
            stat.close();
            Datasource.returnConnection(co);
        }
        return answer;
    }

    /**
     * Update the reparationform given in the parameter
     * @param repForm
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static int updateReparationForm(ReparationForm repForm) throws ClassNotFoundException, SQLException, InterruptedException {
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("UPDATE reparationform SET idcardstate = (SELECT id FROM cardstate WHERE description LIKE ?)"
                + ", entrydate = ?"
                + ", outdate = ?"
                + ", diagnosis = ?"
                + " WHERE id = ?");
        stat.setString(1, repForm.getDescriptionCardState());
        if (repForm.getEntryDate().toString() == null) {
            stat.setDate(2, null);
        } else {
            stat.setDate(2, java.sql.Date.valueOf(repForm.getEntryDate().toString()));
        }
        if (repForm.getOutDate().toString() == null) {
            stat.setDate(3, null);
        } else {
            stat.setDate(3, java.sql.Date.valueOf(repForm.getOutDate().toString()));
        }
        stat.setString(4, repForm.getDiagnosis());
        stat.setInt(5, repForm.getId());

        int result = stat.executeUpdate();

        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    public static int getPriorityLevelInReparationForm(ReparationForm repForm) throws ClassNotFoundException, SQLException, InterruptedException{
        List<Breakdown> lBreakdown = null;
        if(repForm.getBike() != null){
            lBreakdown = BreakdownDAO.getHaveListByVehicleId(repForm.getBike().getId(), "bike");
            
        }else if(repForm.getCar() != null){
            lBreakdown = BreakdownDAO.getHaveListByVehicleId(repForm.getCar().getId(), "car");
        }
        int priorityMax = 0;
        for (Breakdown b : lBreakdown){
            if(b.getPriorityLevel() > priorityMax){
                priorityMax = b.getPriorityLevel();
            }
        }

        Instant instant = LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
        Long milliDiff = Date.from(instant).getTime() - repForm.getEntryDate().getDate().getTime();
        long  milliPerDay = 1000 * 60 * 60 * 24;
        milliDiff = milliDiff/milliPerDay;
        int diff = milliDiff.intValue() / 7;
        
        if(priorityMax+diff > 6){
            priorityMax = 6; 
        }else{
            priorityMax = priorityMax + diff;
        }
        
        return priorityMax;
    }
    
    public static int updateWorkOn(int idForm, int idUser) throws SQLException, ClassNotFoundException, InterruptedException{
        int result = 0;
        
        Connection co = Datasource.getConnection();
        
        PreparedStatement stat = co.prepareStatement("SELECT * FROM workOn WHERE idEmployee = ? AND idReparationForm = ?");
        stat.setInt(1, idUser);
        stat.setInt(2, idForm);
        
        ResultSet rs = stat.executeQuery();
        
        boolean found = rs.next();
        
        rs.close();
        stat.close();
        
        if(!found){
            stat = co.prepareStatement("INSERT INTO workOn VALUES (?, ?)");
            stat.setInt(1, idUser);
            stat.setInt(2, idForm);

            result = stat.executeUpdate();
            stat.close();
        }else{
            result = -1;
        }

        Datasource.returnConnection(co);
        return result;
    }
    
    public static double getAvgDuringBreakdown() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg(cnt.c) as moyenne FROM(SELECT DATE_PART('day',outdate::timestamp - entrydate::timestamp) as c FROM REPARATIONFORM WHERE DATE_PART('day',outdate::timestamp - entrydate::timestamp)>0)cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result; 
    }
}
