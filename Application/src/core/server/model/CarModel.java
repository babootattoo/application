package core.server.model;

import java.util.Objects;
import javax.json.JsonObject;

/**
 *
 * @author Stephane.Schenkel
 */
public class CarModel {
    
    private int id;
    private String builder;
    private String bodyType;
    private boolean isElectric;
    private String description;

    public CarModel(int id, String builder, String bodyType, boolean isElectric, String description) {
        this.id = id;
        this.builder = builder;
        this.bodyType = bodyType;
        this.isElectric = isElectric;
        this.description = description;
    }
    
    public static CarModel deserialize(JsonObject inputJson){
        if(inputJson.getString("id").equals("-1")){
            return null;
        }else{
            return new CarModel(Integer.parseInt(inputJson.getString("id")), inputJson.getString("builder"), inputJson.getString("bodyType"), Boolean.valueOf(inputJson.getString("isElectric")), inputJson.getString("description"));
        }
    }

    public int getId() {
        return id;
    }

    public String getBuilder() {
        return builder;
    }

    public String getBodyType() {
        return bodyType;
    }

    public boolean getIsElectric() {
        return isElectric;
    }

    public String getDescription() {
        return description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBuilder(String builder) {
        this.builder = builder;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public void setIsElectric(boolean isElectric) {
        this.isElectric = isElectric;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.id;
        hash = 53 * hash + Objects.hashCode(this.builder);
        hash = 53 * hash + Objects.hashCode(this.bodyType);
        hash = 53 * hash + (this.isElectric ? 1 : 0);
        hash = 53 * hash + Objects.hashCode(this.description);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CarModel other = (CarModel) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.isElectric != other.isElectric) {
            return false;
        }
        if (!Objects.equals(this.builder, other.builder)) {
            return false;
        }
        if (!Objects.equals(this.bodyType, other.bodyType)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        return true;
    }
    
    public String getModelFullName(){
        return builder + " " + bodyType + " " + description;
    }
    
}
