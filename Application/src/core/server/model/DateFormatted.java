package core.server.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Personalized Date class, with a format of yyyy-MM-dd
 * @author Stephane.Schenkel
 */
public class DateFormatted{
    Date date;
    private boolean hour;
    
    private static DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    private static DateFormat formatHour = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a");
    
    public DateFormatted(String date, boolean timestamp) throws ParseException{
        if (date.equals("null")) {
            this.date = null;
        }else{
            if(timestamp){
                this.date = formatHour.parse(date);
            }else{
                this.date = format.parse(date);
            }
        }
        this.hour = timestamp;
    }
    
    public DateFormatted(Date date, boolean timestamp){
        this.date = date;
        this.hour = timestamp;
    }
    
    public String toString(){
        if(date != null){
            if(hour){
                return toStringHour();
            }else{
                return format.format(date);
            }
        }else{
            return null;
        }
    }
    
    public String toStringHour(){
        if(date != null){
            return formatHour.format(date);
        }else{
            return null;
        }
    }
    
    public Date getDate(){
        return this.date;
    }
    
    public void setDate(Date date, boolean timestamp){
        this.date = date;
        this.hour = timestamp;
    }
    
    public void setDate(String date, boolean timestamp) throws ParseException{
        if(timestamp){
            this.date = formatHour.parse(date);
        }else{
            this.date = format.parse(date);
        }
        this.hour = timestamp;
    }
}
