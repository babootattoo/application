package core.server.model;

/**
 *
 * @author charles.Santerre, Stéphane.Schenkel
 */
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class SerializableJson {

    public static JsonObjectBuilder serialize(Object ob) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, IllegalArgumentException {
        JsonObject json = null;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        if (ob != null) {
            Field[] fields = ob.getClass().getDeclaredFields();

            for (Field field : fields) {
                String className = field.getType().getSimpleName();

                if (className.equals("Car") || className.equals("Bike") || className.equals("Parking") || className.equals("Part") || className.equals("CarModel") || className.equals("Breakdown") || className.equals("NFCCase") || className.equals("RFIDChip")) {
                    String change = field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
                    Method meth = ob.getClass().getMethod("get" + change);
                    builder.add(field.getName(), SerializableJson.serialize(meth.invoke(ob)));

                } else if (className.equals("ArrayList") || className.equals("List")) {
                    JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
                    JsonObjectBuilder builderObjectList = Json.createObjectBuilder();

                    String change = field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
                    Method meth = ob.getClass().getMethod("get" + change);
                    
                    int i = 0;
                    String classNameObjectList = null;
                    for (Object o : SerializableJson.createArrayList(meth.invoke(ob))) {
                        if (classNameObjectList == null) {
                            classNameObjectList = o.getClass().getSimpleName();
                        }
                        builderObjectList.add(classNameObjectList + "_" + i, SerializableJson.serialize(o));
                        arrayBuilder.add(builderObjectList);
                        i++;
                    }
                    builder.add(field.getName() + "_list", arrayBuilder);
                } else {
                    String change = field.getName().substring(0,1).toUpperCase() + field.getName().substring(1);
                    Method meth = ob.getClass().getMethod("get"+change);
                    builder.add(field.getName(), (meth.invoke(ob) != null) ? (meth.invoke(ob).toString() != null) ? meth.invoke(ob).toString() : "null" : "null");
                }
            }
        } else {
            builder.add("id", "-1");
        }
        return builder;
    }

    private static ArrayList<Object> createArrayList(Object obj) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, IllegalArgumentException {
        ArrayList<Object> list = new ArrayList<Object>();
        Class c = obj.getClass();
        Method meth = c.getMethod("size");
        int size = Integer.valueOf(String.valueOf(meth.invoke(obj)));
        Method meth2 = c.getMethod("get", int.class);
        for (int i = 0; i < size; i++) {
            list.add(meth2.invoke(obj, i));
        }
        return list;
    }
}