package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Stephane.Schenkel
 */
public class CommentDAO {
    
    /**
     * Get all the comment of the specified reparation form id
     * @param id
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static List<Comment> getCommentsByFormId(int id) throws ClassNotFoundException, SQLException, InterruptedException{
        List<Comment> result = new ArrayList<Comment>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT firstname, lastname, creation_date, commentary "
                + "FROM commentaryreparationform crf JOIN employee e ON crf.idemployee = e.id "
                + "WHERE idreparationform = ?");
        stat.setInt(1, id);
        ResultSet rs = stat.executeQuery();
        
        while(rs.next()){
            result.add(new Comment(rs.getString("firstname") + " " + rs.getString("lastname"), new DateFormatted(rs.getTimestamp("creation_date"), true), rs.getString("commentary")));
        }
        
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Add a comment into the database with the specified message, idForm and idUser
     * @param idUser
     * @param idForm
     * @param message
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static int addComment(int idUser, int idForm, String message) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("INSERT INTO commentaryreparationform VALUES (?, ?, now(), ?)");
        stat.setInt(1, idForm);
        stat.setInt(2, idUser);
        stat.setString(3, message);
        
        int result = stat.executeUpdate();
        
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
}
