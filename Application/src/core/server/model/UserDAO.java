package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * All database methods related to the User class
 * @author Stephane.Schenkel
 */
public class UserDAO {
    
    /**
     * Return a User object created from the database
     * It searches the user that has the corresponding login and password
     * If this user is not found in the database, it returns a null object
     * @param login
     * @param password
     * @return a user object or a null object
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static User getUserByLoginPassword(String login, String password) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        User result = null;
        PreparedStatement stat = co.prepareStatement("SELECT employee.id, firstname, lastname, address, town, postalcode, email, hiringdate, incomePerHour, workingtypeuser "
                + "FROM employee, typeemployee "
                + "WHERE login = ? AND password = ? AND idtypeemployee = typeemployee.id");
        stat.setString(1, login);
        stat.setString(2, password);
        ResultSet rs = stat.executeQuery();
        if (rs.next()) {
            result = new User(rs.getInt("id"), rs.getString("workingtypeuser"), rs.getString("firstname"), rs.getString("lastname"), rs.getString("address"), rs.getString("town"), rs.getString("postalcode"), login, rs.getString("email"), new DateFormatted(rs.getDate("hiringdate"), false), rs.getFloat("incomePerHour"));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    public static List<User> getAllReparator() throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        List<User> result = new ArrayList<User>();
        PreparedStatement stat = co.prepareStatement("SELECT employee.id, firstname, lastname FROM employee, typeemployee WHERE idtypeemployee = typeemployee.id AND workingTypeUser='Repairer'");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            User user = new User (rs.getInt("id"), null,rs.getString("firstname"), rs.getString("lastname"),null,null,null,null,null,null,0.0);
            result.add(user);
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
                
    }
    
    
    public static Integer getNumberUser() throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection(); 
        Integer result = null;
        PreparedStatement stat = co.prepareStatement("SELECT count(employee.id) as number FROM employee");
        ResultSet rs= stat.executeQuery();
        while (rs.next()){
            result = rs.getInt("number"); 
        }
        
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result; 
        
    }
    
    public static int getNumberReparationTotal(int id) throws ClassNotFoundException, SQLException, InterruptedException{
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT count(*) as number FROM WorkOn WHERE idEmployee=?");
        stat.setInt(1,id);
        ResultSet rs=stat.executeQuery();
        int result = 0;
        while (rs.next()){
            result = rs.getInt("number");
        }
        
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    public static int getNumberReparationThisYear(int id) throws ClassNotFoundException, SQLException, InterruptedException{
        Connection co = Datasource.getConnection();
        //TODO
        int result=0;
         PreparedStatement stat = co.prepareStatement("SELECT count(*) as number FROM CommentaryReparationForm,workon WHERE CommentaryReparationForm.idReparationForm = workon.idReparationform AND CommentaryReparationForm.idEmployee = workon.idEmployee AND extract(year from creation_date) = extract(year from current_date) AND workon.idEmployee=?");
        //EndTODO
        stat.setInt(1,id);
        ResultSet rs=stat.executeQuery();
        while (rs.next()){
            result = rs.getInt("number");
        }
        
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    
    
    public static int getNumberReparationToday(int id) throws ClassNotFoundException, SQLException, InterruptedException{
        Connection co = Datasource.getConnection();
        //TODO
        int result=0;
        PreparedStatement stat = co.prepareStatement("SELECT count(*) as number FROM CommentaryReparationForm,workon WHERE CommentaryReparationForm.idReparationForm = workon.idReparationform AND CommentaryReparationForm.idEmployee = workon.idEmployee AND creation_date=current_date AND workon.idEmployee=?");
        //EndTODO
        stat.setInt(1,id);
        ResultSet rs=stat.executeQuery();
        while (rs.next()){
            result = rs.getInt("number");
        }
        
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    public static int getNumberReparationThisMonth(int id) throws ClassNotFoundException, SQLException, InterruptedException{
        Connection co = Datasource.getConnection();
        //TODO
        int result=0;
        PreparedStatement stat = co.prepareStatement("SELECT count(*) as number FROM CommentaryReparationForm,workon WHERE CommentaryReparationForm.idReparationForm = workon.idReparationform AND CommentaryReparationForm.idEmployee = workon.idEmployee AND creation_date=current_date AND workon.idEmployee=?");
        //EndTODO
        stat.setInt(1,id);
        ResultSet rs=stat.executeQuery();
        while (rs.next()){
            result = rs.getInt("number");
        }
        
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
}
