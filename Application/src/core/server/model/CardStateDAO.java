package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Stephane.Schenkel
 */
public class CardStateDAO {
    
    /**
     * Return the list of card state
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static List<String> getAllString() throws ClassNotFoundException, SQLException, InterruptedException{
        List<String> result = new ArrayList<String>();
        
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT description FROM cardstate ORDER BY id asc");
        ResultSet rs = stat.executeQuery();
        
        while(rs.next()){
            result.add(rs.getString("description"));
        }
        
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        
        return result;
    }
    
    /**
     * Return the list of count per card state
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static List<String> getCountCardState() throws ClassNotFoundException, SQLException, InterruptedException{
        List<String> result = new ArrayList<String>();
        
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("select description, count(idCardState) as number from reparationForm r, cardState c where c.id = r.idCardState group by description");
        ResultSet rs = stat.executeQuery();
        
        while(rs.next()){
            result.add(String.valueOf(rs.getString("description")));
            result.add(rs.getString("number"));
        }
        
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        
        return result;
    }
}
