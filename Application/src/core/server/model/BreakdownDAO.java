package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Stéphane.Schenkel
 */
public class BreakdownDAO {
    
    /**
     * Return the list of breakdown that the specified vehicle has.
     * @param idVehicle
     * @param table
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static List<Breakdown> getHaveListByVehicleId(int idVehicle, String table) throws ClassNotFoundException, SQLException, InterruptedException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("select b.id as id, b.description as bdesc, bc.description as bcdesc, breakdowndate, b.averageTimeMinutes as bavgtm, b.priorityLevel as bpl, resolved, resolved_date from breakdown b join havebreakdown"+ table +" hb on b.id = hb.idbreakdown"
                + " JOIN breakdowncategory bc ON b.idbreakdowncategory = bc.id WHERE id"+table+" = ?");
        stat.setInt(1, idVehicle);
        ResultSet rs = stat.executeQuery();
        while(rs.next()){
            result.add(new Breakdown(rs.getInt("id"), rs.getString("bcdesc"), rs.getString("bdesc"), new DateFormatted(rs.getDate("breakdowndate"), false), rs.getInt("bavgtm"), rs.getInt("bpl"), rs.getBoolean("resolved"), new DateFormatted(rs.getTimestamp("resolved_date"), true)));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Return the list of available breakdown by the bike.
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static List<Breakdown> getAvailableBreakdownBike(int idBike) throws SQLException, ClassNotFoundException, InterruptedException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT b.id as id, b.description as bdesc, bc.description as bcdesc, b.averageTimeMinutes as bavgtm, b.priorityLevel as bpl FROM breakdown b JOIN breakdowncategory bc ON b.idbreakdowncategory = bc.id WHERE canoccurbike = true AND b.id NOT IN (SELECT idbreakdown FROM havebreakdownbike WHERE idbike = ?)");
        stat.setInt(1, idBike);
        ResultSet rs = stat.executeQuery();
        
        Date datenull = null;
        while(rs.next()){
            result.add(new Breakdown(rs.getInt("id"), rs.getString("bcdesc"), rs.getString("bdesc"), new DateFormatted(datenull, false), rs.getInt("bavgtm"), rs.getInt("bpl"), false, new DateFormatted(datenull, false)));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Return the list of breakdown available by the specified carmodel
     * @param idModel
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static List<Breakdown> getAvailableBreakdownModel(int idModel, int idCar) throws SQLException, ClassNotFoundException, InterruptedException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT b.id as id, b.description as bdesc, bc.description as bcdesc, b.averageTimeMinutes as bavgtm, b.priorityLevel as bpl FROM breakdown b JOIN breakdowncategory bc ON b.idbreakdowncategory = bc.id JOIN canhavebreakdowncarmodel chb ON b.id = chb.idbreakdown WHERE idcarmodel = ? AND b.id NOT IN (SELECT idbreakdown FROM havebreakdowncar WHERE idcar = ?)");
        stat.setInt(1, idModel);
        stat.setInt(2, idCar);
        ResultSet rs = stat.executeQuery();
        
        Date datenull = null;
        while(rs.next()){
            result.add(new Breakdown(rs.getInt("id"), rs.getString("bcdesc"), rs.getString("bdesc"), new DateFormatted(datenull, false), rs.getInt("bavgtm"), rs.getInt("bpl"), false, new DateFormatted(datenull, false)));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    public static int getNumberBreakdownThisYear() throws SQLException, ClassNotFoundException, InterruptedException{
        int result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT count(*) as number FROM havebreakdowncar WHERE extract(year from breakdowndate) = extract(year from current_date)");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getInt("number");
        }
        rs.close();
        stat.close();
        
        PreparedStatement stat2 = co.prepareStatement("SELECT count(*) as number FROM havebreakdownbike WHERE extract(year from breakdowndate) = extract(year from current_date)");
        ResultSet rs2 = stat2.executeQuery();
        while (rs2.next()){
            result = result + rs2.getInt("number");
        }
        rs2.close();
        stat2.close();
        
        Datasource.returnConnection(co);
        return result; 
    }
    
    public static int getNumberBreakdownThisMonth() throws SQLException, ClassNotFoundException, InterruptedException{
        int result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT count(*) as number FROM havebreakdowncar WHERE extract(month from breakdowndate) = extract(month from current_date)");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getInt("number");
        }
        rs.close();
        stat.close();
        
        PreparedStatement stat2 = co.prepareStatement("SELECT count(*) as number FROM havebreakdownbike WHERE extract(month from breakdowndate) = extract(month from current_date)");
        ResultSet rs2 = stat2.executeQuery();
        while (rs2.next()){
            result = result + rs2.getInt("number");
        }
        rs2.close();
        stat2.close();
        
        Datasource.returnConnection(co);
        return result; 
    }
    
    public static double getAvgBreakdownByMonth() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg (cnt.c) as moyenne FROM (SELECT count(*) as c FROM havebreakdowncar GROUP BY extract(month from breakdowndate)) cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();
        
        PreparedStatement stat2 = co.prepareStatement("SELECT avg (cnt.c) as moyenne FROM (SELECT count(*) as c FROM havebreakdownbike GROUP BY extract(month from breakdowndate)) cnt");
        ResultSet rs2 = stat2.executeQuery();
        while (rs2.next()){
            result = result + rs2.getDouble("moyenne");
        }
        rs2.close();
        stat2.close();
        
        Datasource.returnConnection(co);
        return result; 
    }
    
    public static double getAvgBreakdownByYear() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg (cnt.c) as moyenne FROM (SELECT count(*) as c FROM havebreakdowncar GROUP BY extract(year from breakdowndate)) cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();
        
        PreparedStatement stat2 = co.prepareStatement("SELECT avg (cnt.c) as moyenne FROM (SELECT count(*) as c FROM havebreakdownbike GROUP BY extract(year from breakdowndate)) cnt");
        ResultSet rs2 = stat2.executeQuery();
        while (rs2.next()){
            result = result + rs2.getDouble("moyenne");
        }
        rs2.close();
        stat2.close();
        
        Datasource.returnConnection(co);
        return result; 
    }
    
    public static int getNbCarTotalBreakdown() throws SQLException, ClassNotFoundException, InterruptedException{
        int result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT count(*) as number FROM haveBreakdownCar ");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getInt("number");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result; 
    }
    
    public static int getNbBikeTotalBreakdown() throws SQLException, ClassNotFoundException, InterruptedException{
        int result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT count(*) as number FROM haveBreakdownBike ");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getInt("number");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result; 
    }
    
    public static int getNbCarBreakdownThisMonth() throws SQLException, ClassNotFoundException, InterruptedException{
        int result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT count(*) as number FROM haveBreakdownCar WHERE extract(month from haveBreakdownCar.breakdowndate) = extract(month from current_date)");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getInt("number");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    public static int getNbBikeBreakdownThisMonth() throws SQLException, ClassNotFoundException, InterruptedException{
        int result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT count(*) as number FROM haveBreakdownBike WHERE extract(month from haveBreakdownBike.breakdowndate) = extract(month from current_date)");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getInt("number");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    public static int getNbCarBreakdownThisYear() throws SQLException, ClassNotFoundException, InterruptedException{
        int result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT count(*) as number FROM haveBreakdownCar WHERE extract(year from haveBreakdownCar.breakdowndate) = extract(year from current_date)");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getInt("number");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    public static int getNbBikeBreakdownThisYear() throws SQLException, ClassNotFoundException, InterruptedException{
        int result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT count(*) as number FROM haveBreakdownBike WHERE extract(year from haveBreakdownBike.breakdowndate) = extract(year from current_date)");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getInt("number");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    public static double getAvgCarBreakdownByYear() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg (cnt.c) as moyenne FROM (SELECT count(*) as c FROM havebreakdowncar GROUP BY extract(year from breakdowndate)) cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result; 
    }
    
    public static double getAvgCarBreakdownByMonth() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg (cnt.c) as moyenne FROM (SELECT count(*) as c FROM havebreakdowncar GROUP BY extract(year from breakdowndate)) cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result; 
    }
    
    public static double getAvgBikeBreakdownByYear() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg (cnt.c) as moyenne FROM (SELECT count(*) as c FROM havebreakdownbike GROUP BY extract(year from breakdowndate)) cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result; 
    }
    
    public static double getAvgBikeBreakdownByMonth() throws SQLException, ClassNotFoundException, InterruptedException{
        double result = 0;
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT avg (cnt.c) as moyenne FROM (SELECT count(*) as c FROM havebreakdownbike GROUP BY extract(month from breakdowndate)) cnt");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            result = rs.getDouble("moyenne");
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result; 
    }
    
    public static List<String[]> getListBreakdownCar() throws SQLException, ClassNotFoundException, InterruptedException{
        List<String[]> result = new ArrayList<String[]>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT count(havebreakdowncar.idbreakdown) as number, description from havebreakdowncar,breakdown where havebreakdowncar.idbreakdown=breakdown.id GROUP BY description ORDER BY number");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            String[] get= {rs.getString("number"), rs.getString("description")};
            result.add(get);
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    public static List<String[]> getListBreakdownBike() throws SQLException, ClassNotFoundException, InterruptedException{
        List<String[]> result = new ArrayList<String[]>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT count(havebreakdownbike.idbreakdown) as number, description from havebreakdownbike,breakdown where havebreakdownbike.idbreakdown=breakdown.id GROUP BY description ORDER BY number");
        ResultSet rs = stat.executeQuery();
        while (rs.next()){
            String[] get= {rs.getString("number"), rs.getString("description")};
            result.add(get);
        }
        rs.close();
        stat.close();
        
        Datasource.returnConnection(co);
        return result;   
    }
    
    /**
     * Return the list of maximum priority level per car
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static List<String> getMaxPriorityLevelPerCar() throws ClassNotFoundException, SQLException, InterruptedException{
        List<String> result = new ArrayList<String>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("select c.id as id, matriculation, max(prioritylevel) as priorityLevel from car c, havebreakdowncar h, breakdown b where c.id = h.idcar and h.idbreakdown = b.id group by matriculation, c.id");
        ResultSet rs = stat.executeQuery();
        while(rs.next()){
            result.add(rs.getString("id"));
            result.add(rs.getString("matriculation"));
            result.add(String.valueOf(rs.getInt("priorityLevel")));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
        /**
     * Return the list of maximum priority level per bike
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static List<String> getMaxPriorityLevelPerBike() throws ClassNotFoundException, SQLException, InterruptedException{
        List<String> result = new ArrayList<String>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("select bi.id as id, idrfidchip, max(prioritylevel) as priorityLevel from bike bi, havebreakdownbike h, breakdown b where bi.id = h.idbike and h.idbreakdown = b.id group by idrfidchip, bi.id");
        ResultSet rs = stat.executeQuery();
        while(rs.next()){
            result.add(rs.getString("id") + "_" + rs.getString("idrfidchip") + "_" + String.valueOf(rs.getInt("priorityLevel")));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
}
