package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Stephane.Schenkel
 */
public class RFIDChipDAO {
    
    /**
     * Get the RFIDChip with the specified id
     * @param idChip
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static RFIDChip getChipById(int idChip) throws ClassNotFoundException, SQLException, InterruptedException{
        Connection co = Datasource.getConnection();
        RFIDChip result = null;
        PreparedStatement stat = co.prepareStatement("SELECT * FROM rfidchip WHERE id = ?");
        stat.setInt(1, idChip);
        ResultSet rs = stat.executeQuery();
        if (rs.next()) {
            result = new RFIDChip(rs.getInt("id"), rs.getString("tagid"), new DateFormatted(rs.getDate("installationdate"), false));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Get the RFIDChip with the specified tag
     * @param idChip
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static RFIDChip getChipByTag(String tag) throws ClassNotFoundException, SQLException, InterruptedException{
        Connection co = Datasource.getConnection();
        RFIDChip result = null;
        PreparedStatement stat = co.prepareStatement("SELECT * FROM rfidchip WHERE tagid = ?");
        stat.setString(1, tag);
        ResultSet rs = stat.executeQuery();
        if (rs.next()) {
            result = new RFIDChip(rs.getInt("id"), rs.getString("tagid"), new DateFormatted(rs.getDate("installationdate"), false));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
}
