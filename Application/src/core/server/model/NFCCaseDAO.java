package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Stephane.Schenkel
 */
public class NFCCaseDAO {
    
    /**
     * Get the NFCCase with the specified id
     * @param idCase
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static NFCCase getCaseById(int idCase) throws ClassNotFoundException, SQLException, InterruptedException{
        Connection co = Datasource.getConnection();
        NFCCase result = null;
        PreparedStatement stat = co.prepareStatement("SELECT * FROM nfccase WHERE id = ?");
        stat.setInt(1, idCase);
        ResultSet rs = stat.executeQuery();
        if (rs.next()) {
            result = new NFCCase(rs.getInt("id"), rs.getString("tagid"), new DateFormatted(rs.getDate("installationdate"), false));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Get the NFCCase with the specified tag
     * @param idCase
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static NFCCase getCaseByTag(String tag) throws ClassNotFoundException, SQLException, InterruptedException{
        Connection co = Datasource.getConnection();
        NFCCase result = null;
        PreparedStatement stat = co.prepareStatement("SELECT * FROM nfccase WHERE tagid = ?");
        stat.setString(1, tag);
        ResultSet rs = stat.executeQuery();
        if (rs.next()) {
            result = new NFCCase(rs.getInt("id"), rs.getString("tagid"), new DateFormatted(rs.getDate("installationdate"), false));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
}
