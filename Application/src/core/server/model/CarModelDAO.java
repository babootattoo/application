package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Stephane.Schenkel
 */
public class CarModelDAO {
    
    /**
     * Return the carModel found in the database with the specified id
     * @param idCase
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static CarModel getModelById(int idCase) throws ClassNotFoundException, SQLException, InterruptedException{
        Connection co = Datasource.getConnection();
        CarModel result = null;
        PreparedStatement stat = co.prepareStatement("SELECT * FROM carmodel WHERE id = ?");
        stat.setInt(1, idCase);
        ResultSet rs = stat.executeQuery();
        if (rs.next()) {
            result = new CarModel(rs.getInt("id"), rs.getString("builder"), rs.getString("bodytype"), rs.getBoolean("electric"), rs.getString("descriptioncarmodel"));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
}
