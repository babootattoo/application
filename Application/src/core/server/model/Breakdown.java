package core.server.model;

import java.text.ParseException;
import javax.json.JsonObject;

/**
 *
 * @author Stephane.Schenkel
 */
public class Breakdown {
    
    private int id;
    private String category;
    private String description;
    private int averageTimeMinutes;
    private int priorityLevel;
    private boolean resolved;
    private DateFormatted resolved_date;
    
    private DateFormatted occuredDate;

    public Breakdown(int id, String category, String description, DateFormatted occuredDate, int averageTimeMinutes, int priorityLevel, boolean resolved, DateFormatted resolved_date) {
        this.id = id;
        this.category = category;
        this.description = description;
        this.occuredDate = occuredDate;
        this.averageTimeMinutes = averageTimeMinutes;
        this.priorityLevel = priorityLevel;
        this.resolved = resolved;
        this.resolved_date = resolved_date;
    }

    public int getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public DateFormatted getOccuredDate() {
        return occuredDate;
    }
    
    public int getAverageTimeMinutes() {
        return averageTimeMinutes;
    }
    
    public int getPriorityLevel() {
        return priorityLevel;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setOccuredDate(DateFormatted occuredDate) {
        this.occuredDate = occuredDate;
    }
    
    public void setAverageTimeMinutes(int averageTimeMinutes) {
        this.averageTimeMinutes = averageTimeMinutes;
    }
    
    public void setPriorityLevel(int priorityLevel) {
        this.priorityLevel = priorityLevel;
    }
    
    public static Breakdown deserialize(JsonObject inputJson) throws ParseException{
        if(inputJson.getString("id").equals("-1")){
            return null;
        }else{
            return new Breakdown(Integer.parseInt(inputJson.getString("id")), inputJson.getString("category"), inputJson.getString("description"), new DateFormatted(inputJson.getString("occuredDate"), false), Integer.parseInt(inputJson.getString("averageTimeMinutes")), Integer.parseInt(inputJson.getString("priorityLevel")), Boolean.parseBoolean(inputJson.getString("resolved")), new DateFormatted(inputJson.getString("resolved_date"), true));
        }
    }

    public boolean getResolved() {
        return resolved;
    }

    public DateFormatted getResolved_date() {
        return resolved_date;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    public void setResolved_date(DateFormatted resolved_date) {
        this.resolved_date = resolved_date;
    }
    
}
