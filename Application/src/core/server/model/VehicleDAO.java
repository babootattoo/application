package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Stephane.Schenkel
 */
public class VehicleDAO {
    
    /**
     * Delete the part for the specified vehicle
     * @param idVehicle
     * @param idPart
     * @param table
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static int deletePart(int idVehicle, int idPart, String table) throws SQLException, ClassNotFoundException, InterruptedException{
        int result = 0;
        
        Connection co = Datasource.getConnection();
        
        PreparedStatement stat = co.prepareStatement("SELECT numberUse FROM  usepart"+table+" WHERE id"+table+" = ? AND idPart = ?");
        stat.setInt(1, idVehicle);
        stat.setInt(2, idPart);
        
        ResultSet rs = stat.executeQuery();
        
        rs.next();
        
        if(rs.getInt("numberUse") != 1){
            PreparedStatement statUpdate = co.prepareStatement("UPDATE usepart"+table+" SET numberUse = numberUse - 1 WHERE id"+table+" = ? AND idPart = ?");
            statUpdate.setInt(1, idVehicle);
            statUpdate.setInt(2, idPart);

            result = statUpdate.executeUpdate();

            statUpdate.close();   
        }else{
            PreparedStatement statInsert = co.prepareStatement("DELETE FROM usepart"+table+" WHERE id"+table+" = ? AND idpart = ?");
            statInsert.setInt(1, idVehicle);
            statInsert.setInt(2, idPart);

            result = statInsert.executeUpdate();

            statInsert.close();   
        }
        
        rs.close();
        stat.close(); 
        
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Add the part for the specified vehicle
     * @param idVehicle
     * @param idPart
     * @param table
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static int addPart(int idVehicle, int idPart, String table) throws SQLException, ClassNotFoundException, InterruptedException{
        int result = 0;
        
        Connection co = Datasource.getConnection();
        
        PreparedStatement stat = co.prepareStatement("SELECT * FROM  usepart"+table+" WHERE id"+table+" = ? AND idPart = ?");
        stat.setInt(1, idVehicle);
        stat.setInt(2, idPart);
        
        ResultSet rs = stat.executeQuery();
        
        if(rs.next()){
            PreparedStatement statUpdate = co.prepareStatement("UPDATE usepart"+table+" SET numberUse = numberUse + 1 WHERE id"+table+" = ? AND idPart = ?");
            statUpdate.setInt(1, idVehicle);
            statUpdate.setInt(2, idPart);

            result = statUpdate.executeUpdate();

            statUpdate.close();   
        }else{
            PreparedStatement statInsert = co.prepareStatement("INSERT INTO usepart"+table+" VALUES (?, ?, 1, now())");
            statInsert.setInt(1, idVehicle);
            statInsert.setInt(2, idPart);

            result = statInsert.executeUpdate();

            statInsert.close();   
        }
        
        rs.close();
        stat.close(); 
        
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Delete the breakdown for the specified vehicle
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static int deleteBreakdown(int idVehicle, int idBreakdown, String table) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("DELETE FROM havebreakdown"+table+" WHERE id"+table+" = ? AND idbreakdown = ?");
        stat.setInt(1, idVehicle);
        stat.setInt(2, idBreakdown);
        
        int result = stat.executeUpdate();
        
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Add the breakdown for the specified vehicle
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static int addBreakdown(int idVehicle, int idBreakdown, String table) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("INSERT INTO havebreakdown"+table+" VALUES (?, ?, now())");
        stat.setInt(1, idVehicle);
        stat.setInt(2, idBreakdown);
        
        int result = stat.executeUpdate();
        
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Update the resolved column of breakdown for a specified vehicle
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static int updateResolvedBreakdown(int idVehicle, int idBreakdown, String table) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("UPDATE havebreakdown"+table+" SET resolved = true, resolved_date = now() WHERE id"+table+" = ? AND idbreakdown = ?");
        stat.setInt(1, idVehicle);
        stat.setInt(2, idBreakdown);
        
        int result = stat.executeUpdate();
        
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    public static int getNumberVehicle () throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT count(*) as number FROM Car");
        ResultSet rs= stat.executeQuery();
        int result = 0;
        while (rs.next()){
            result = rs.getInt("number");
        }
        
        rs.close();
        stat.close();
     
        PreparedStatement stat2 = co.prepareStatement("SELECT count(*) as number FROM Bike");
        ResultSet rs2 = stat2.executeQuery();
        int result2 = 0 ;
        while (rs2.next()){
            result2= rs2.getInt("number");
        }
                
        rs2.close();
        stat2.close();
        Datasource.returnConnection(co);
        result = result+result2;
        return result;
    }
    
}
