package core.server.view;

import java.lang.Thread.State;
import java.util.Scanner;
import core.server.network.Server;
import core.server.model.ViewModel;

/**
 * Main method, create a Server class (which is also a Thread) and start it
 * Then there is a loop where the user can input a string
 * If it is "close" it stops the server and exit the loop.
 * @author Stephane.Schenkel, Brice.Boutamdja
 */
public class main {
    
    public static void main (String[] args) {
        try{
            boolean exit = false;
            Server serv = new Server();
            serv.start();
            do{
                Scanner sc = new Scanner(System.in);
                String input = sc.nextLine();
                switch(input){
                    case "close":
                        serv.closeServer();
                        serv.interrupt();
                        serv.join();
                        if(serv.getState().equals(State.TERMINATED)){
                            exit = true;
                        }
                        break;
                    case "list_user":
                        serv.listActiveUsers();
                        break;
                }
            }while(!exit);
        }catch(Exception e){
            ViewModel.printError(e);
        }
    }
    
}
